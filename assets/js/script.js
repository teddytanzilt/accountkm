function formatCurrency(num)
{
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
    {
        num = "0";
    }

    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();

    if (cents < 10)
    {
        cents = "0" + cents;
    }
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
    {
        num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
    }

    //return (((sign) ? '' : '-') + '$' + num + '.' + cents);
    return ((sign) ? '' : '-') + num;
}

function unformatCurrency(num)
{
    num = num.toString().replace(/\$|\,/g, '');
    return num;
}

function calculateValue(value, perc)
{   
    var return_value = (parseInt(perc)/100) * parseInt(value);
    return return_value;
}

/*function calculateTotal(value, disc_perc, tax_perc)
{   
    var disc_value = calculateValue(value, disc_perc);
    var tax_value = calculateValue(value, tax_perc);
    var total = parseInt(value) - parseInt(disc_value) + parseInt(tax_value);
    return total;
}*/

function calculateTotal(value, disc_value, tax_perc)
{   
    var tax_value = calculateValue(value, tax_perc);
    var total = parseInt(value) - parseInt(disc_value) + parseInt(tax_value);
    return total;
}

