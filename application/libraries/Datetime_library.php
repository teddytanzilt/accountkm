<?php
class Datetime_library
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();		
	}
	
	/*input_date format Y-m-d H:i:s*/
	function date_format($input_date, $format = 'Y-m-d H:i:s')
	{
		try {
			$date = new DateTime($input_date);
			return $date->format($format);
		} catch (Exception $e) {
			return "";
		}
		
	}
	
	function date_format_from_to($input_date, $from = 'dmY', $to = 'Ymd')
	{
		try {
			return date_format(date_create_from_format($from, $input_date), $to);
		} catch (Exception $e) {
			return "";
		}
	}
	
	function date_format_add($input_date, $interval, $format = 'Y-m-d H:i:s')
	{
		/*$diff1Day = new DateInterval('P1D');
		$diff24Hours = new DateInterval('PT24H');
		$diff1440Minutes = new DateInterval('PT1440M');*/
		try {
			$date = new DateTime($input_date);
			$diff1Day = new DateInterval('P'.$interval.'D');
			$date->add($diff1Day);
			return $date->format($format);
		} catch (Exception $e) {
			return "";
		}
	}
	
	/*input_date format Y-m-d*/
	function date_diff($start_date, $end_date)
	{
		$start_date = $start_date." 00:00:00";
		$end_date = $end_date." 00:00:00";
		$date1Timestamp = strtotime($start_date);
		$date2Timestamp = strtotime($end_date);
		$difference = $date2Timestamp - $date1Timestamp;
		return intval($difference) / 24 / 3600;
	}

	function indonesian_date ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB') {
		try {
			if (trim ($timestamp) == '')
			{
					$timestamp = time ();
			}
			elseif (!ctype_digit ($timestamp))
			{
				$timestamp = strtotime ($timestamp);
			}
			# remove S (st,nd,rd,th) there are no such things in indonesia :p
			$date_format = preg_replace ("/S/", "", $date_format);
			$pattern = array (
				'/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
				'/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
				'/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
				'/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
				'/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
				'/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
				'/April/','/June/','/July/','/August/','/September/','/October/',
				'/November/','/December/',
			);
			$replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
				'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
				'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
				'Januari','Februari','Maret','April','Juni','Juli','Agustus','September',
				'Oktober','November','Desember',
			);
			$date = date ($date_format, $timestamp);
			$date = preg_replace ($pattern, $replace, $date);
			//$date = "{$date} {$suffix}";
			$date = ($suffix)?"{$date} {$suffix}":"{$date}";
			return $date;
		} catch (Exception $e) {
			return "";
		}
	} 
	
	public function time_diff($firstTime,$lastTime)
	{
		$firstTime=strtotime($firstTime);
		$lastTime=strtotime($lastTime);
		$timeDiff=$lastTime-$firstTime;

		$hour = floor ($timeDiff/3600);
		$min = floor(($timeDiff%3600)/60);
		$sec = ($timeDiff%60);
		
		if($min == 0){
			return $hour." Jam";
		}
		return $hour." Jam ".$min." Min";
	}
}
?>