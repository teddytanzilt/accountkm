<?php
class Custom_upload_library
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();	
	}
	
	function custom_upload_file($width, $height, $image_name, &$validation, &$error_message, &$new_image_name, $image_name_prefix = '')
	{  		
		if($_FILES[$image_name]['name'] != ""){
			$new_image_path = "";
			$config['upload_path'] = UPLOADPATH;
			$config['allowed_types'] = "gif|jpg|png|jpeg";
			$config['max_size']	 = "2000";
			$config['encrypt_name'] = TRUE;			
			$this->CI->load->library('upload',$config );
			$this->CI->load->library('image_resize_library');
			try {	
				if($this->CI->upload->do_upload($image_name)){
					$filedata = $this->CI->upload->data();				
					$image_new_width = $width;
					$image_new_height = $height;
					$new_image_name = $image_name_prefix.$filedata['file_name'];
					$new_image_path = $config['upload_path'].'/'.$new_image_name;					
					$this->CI->image_resize_library->resize($config['upload_path']."/".$filedata['file_name']);
					$this->CI->image_resize_library->resizeImage($image_new_width, $image_new_height, "crop"); /*default 'exact', (options: exact, portrait, landscape, auto, crop) */
					$this->CI->image_resize_library->saveImage($new_image_path, 100);
					unlink($config['upload_path']."/".$filedata['file_name']);
				} else {
					$validation = false;
					$error_message = $this->CI->upload->display_errors();
				}
				if($validation){
					if($new_image_path == ""){
						$validation = false;
						$error_message = "Image Process Somehow Failed.";
					}
				}
			} catch (Exception $e) {
				$validation = false;
				$error_message = $e->getMessage();				
			}
		}
	}

	
	
}
?>