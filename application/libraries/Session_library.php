<?php
class Session_library
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();		
	}
	
	function check_session()
	{
		if($this->CI->session->userdata(SESSION_CHECK)){
			return true;
		} else {
			return false;
		}
	}
	
	public function check_session_auth()
	{
		if($this->check_session()){
			redirect(base_url('home/dashboard'));
			exit;
		} else {
			redirect(base_url('home/login'));
			exit;
		}
	}

	public function check_session_auth_exist($exist = true)
	{
		if($exist){
			if($this->check_session()){
				redirect(base_url('home/dashboard'));
				exit;
			}
		} else {
			if(!$this->check_session()){
				redirect(base_url('home/login'));
				exit;
			}
		}
	}
	
}
?>