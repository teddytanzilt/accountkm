<?php
class Common_library
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();		
	}
	
	public function getData($data = null)
	{
		try{
			foreach($_POST as $arr_name => $arr_value){
				if (is_array($arr_value)) {
					//$data[$arr_name] = $arr_value;
 					$data[$arr_name] = $this->subData($arr_name, $arr_value);	
				} elseif($arr_name == "content" || $arr_name == "description" || $arr_name == "product_content" || $arr_name == "social_media_link" || $arr_name == "tanggal"){
					$data[$arr_name] = $arr_value;
				} elseif((strpos($arr_name, "jumlah") !== false) || (strpos($arr_name, "harga") !== false)){
					$data[$arr_name] = clean_value_harga($arr_value);
				} else {
					$data[$arr_name] = clean_value($arr_value);
				}
			}	
		} catch (Exception $e) {

		}
		return $data;
	}

	public function splitArray($arr, $splitter = ",") {
		$return_val = "";
		foreach($arr as $val){
			if($return_val == ""){
				$return_val = $val;
			} else {
				$return_val = $return_val.$splitter.$val;
			}
		}
		return $return_val;
	}

	public function subData($dataName, $data = null)
	{
		try{
			for($x = 0 ; $x < count($data) ; $x++){
				if($dataName == "content" || $dataName == "description" || $dataName == "product_content" || $dataName == "social_media_link"){
					$data[$x] = $data[$x];
				} elseif((strpos($dataName, "jumlah") !== false) || (strpos($dataName, "harga") !== false)){
					$data[$x] = clean_value_harga($data[$x]);
				} else {
					$data[$x] = clean_value($data[$x]);
				}
			}
		} catch (Exception $e) {

		}
		return $data;
	}
}
?>