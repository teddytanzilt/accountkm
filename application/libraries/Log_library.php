<?php
class Log_library
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();		
	}
	
	public function writeLog($result = null)
	{
		try {
			$core_app_log_value = $this->CI->core_app_log_model->getValueList();
			$core_app_log_value["ip"] = 	$this->CI->input->ip_address();		
			$core_app_log_value["forwarded_ip"] = $this->CI->input->ip_address();
			$core_app_log_value["user_agent"] = $this->CI->agent->agent_string();
			$core_app_log_value["device_id"] = $this->CI->device_id;
			$core_app_log_value["event"] = $this->CI->router->fetch_method();
			$core_app_log_value["message"] = ($result['validation']?"Success" : "Failed - ").$result['message'];
			$core_app_log_value["module"] = $this->CI->router->fetch_class();
			$core_app_log_value["action"] = $this->CI->router->fetch_method();
			$core_app_log_value["user_id"] = $this->CI->session_user_id;
			$core_app_log_value["link"] = current_url();
			$core_app_log_value["input_data"] = json_encode($_POST);
			$core_app_log_value["json_return"] = json_encode($result['data_json']);
			$core_app_log_value["created_on"] = date('Y-m-d H:i:s');			
			$this->CI->core_app_log_model->setValueList($core_app_log_value);
			$this->CI->core_app_log_model->insertHeader();
		} catch (Exception $e) {
			
		}
	}

	public function writeLogEmail($mail = null)
	{
		try {
			$core_email_log_value = $this->CI->core_email_log_model->getValueList();
			$core_email_log_value["mail_from"] = $mail->From;		
			$core_email_log_value["mail_from_name"] = $mail->FromName;		
			$core_email_log_value["mail_to"] = json_encode($mail->to);		
			$core_email_log_value["mail_cc"] = json_encode($mail->cc);		
			$core_email_log_value["mail_bcc"] = json_encode($mail->bcc);		
			$core_email_log_value["mail_subject"] = $mail->Subject;
			$core_email_log_value["mail_body"] = $mail->Body;		
			$core_email_log_value["mail_error"] = json_encode($mail->smtp->error);		
			$core_email_log_value["mail_error_info"] = $mail->ErrorInfo;		
			$core_email_log_value["created_on"] = date('Y-m-d H:i:s');			
			$this->CI->core_email_log_model->setValueList($core_email_log_value);
			$this->CI->core_email_log_model->insertHeader();
		} catch (Exception $e) {
			
		}
	}	
	
}
?>