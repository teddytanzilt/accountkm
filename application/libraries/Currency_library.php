
<?php
class Currency_library
{
	var $CI;

  	function __construct()
	{
		$this->CI =& get_instance();		
	}
	
	function to_currency($number, $decimals = 2, $currency_symbol = "Rp. ")
	{
		$CI =& get_instance();
		if($number >= 0)
		{
			$ret = $currency_symbol.number_format($number, $decimals, ',', '.');
		}
		else
		{
			$ret = '&#8209;'.$currency_symbol.number_format(abs($number), $decimals, ',', '.');
		}

		return preg_replace('/(?<=\d{2})0+$/', '', $ret);
	}

	function price_unformat($input_price)
	{
		$removed_symbol = array(",", ".");
		$unformatted_price = str_replace($removed_symbol, "", $input_price);
		return $unformatted_price;
	}
	
	function round_percentage($partial, $full)
	{
		$partial = intval($partial);
		$full = intval($full);
		$percentage = ($partial / $full) * 100;
		return floor($percentage);
	}
	
}
?>