<?php
class Master_user_model extends Core_master_model{	    	
	public $table_name = "master_user";
	var $default_value_list = Array(
		'id' => NULL ,
		'full_name' => '' ,
		'user_name' => '' ,
		'password' => '' ,
		'role' => NULL,
		'ip' => '' ,
		'token_cookie' => '' ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'user_status' => STATUS_ACTIVE,
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'master_user.id' ,
		1 => 'master_user.full_name',
		2 => 'master_user.user_name' ,
		3 => 'master_role.nama AS nama_role' ,
	);
	var $datatable_show_value_list = Array(
		0 => 'id' ,
		1 => 'full_name' ,
		2 => 'user_name' ,
		3 => 'nama_role' ,
	);
	var $datatable_search_value_list = Array(
		0 => 'master_user.id' ,
		1 => 'master_user.full_name',
		2 => 'master_user.user_name' ,
		3 => 'master_role.nama'
	);
	var $fillable_value_list = array(
		'full_name',
		'user_name',
		'password',
		'role',
	);

	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

	function getDataTableCountActive($datatable_request){								
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM ".$this->table_name." ".
			"LEFT JOIN master_role ON (master_role.id = ".$this->table_name.".role) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." ".
			"LEFT JOIN master_role ON (master_role.id = ".$this->table_name.".role) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
}

?>