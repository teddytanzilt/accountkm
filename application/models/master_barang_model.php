<?php
class Master_barang_model extends Core_master_model{	    	
	public $table_name = "master_barang";
	var $default_value_list = Array(
		'id' => NULL ,
		'kode' => '' ,
		'nama' => '' ,
		'satuan' => NULL ,
		'harga_modal' => '' ,
		'harga_jual' => '' ,
		'berat_per_satuan' => '' ,
		'satuan_berat' => NULL ,
		'barang_expor' => 0,
		'import_flag' => '' ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'master_barang.id' ,
		1 => 'master_barang.kode' ,
		2 => 'master_barang.nama' ,
		3 => 'master_barang.berat_per_satuan' ,
		4 => 'master_satuan.kode AS satuan_berat'
	);
	var $datatable_show_value_list = Array(
		0 => 'id' ,
		1 => 'kode' ,
		2 => 'nama' ,
		3 => 'berat_per_satuan' ,
		4 => 'satuan_berat'
	);
	var $datatable_search_value_list = Array(
		0 => 'master_barang.id' ,
		1 => 'master_barang.kode' ,
		2 => 'master_barang.nama' ,
		3 => 'master_barang.berat_per_satuan' ,
		4 => 'master_satuan.kode'
	);
	var $fillable_value_list = array(
		'kode',
		'nama',
		'satuan',
		'harga_modal',
		'harga_jual',
		'berat_per_satuan',
		'satuan_berat',
		'barang_expor'
	);

	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

    function getDataTableCountActive($datatable_request){								
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM ".$this->table_name." ".
			"LEFT JOIN master_satuan ON (master_satuan.id = ".$this->table_name.".satuan_berat) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." ".
			"LEFT JOIN master_satuan ON (master_satuan.id = ".$this->table_name.".satuan_berat) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
}

?>