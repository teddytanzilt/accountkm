<?php
class Trans_sales_invoice_payment_model extends Core_master_model{	    	
	public $table_name = "trans_sales_invoice_payment";
	var $default_value_list = Array(
		'id' => NULL ,
		'header_id' => NULL ,
		'bank' => NULL ,
		'jumlah_bayar' => 0 ,
		'tanggal_bayar' => NULL,
		'note' => '' ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'detail_status' => STATUS_ACTIVE
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $fillable_value_list = array(
		'bank',
		'jumlah_bayar',
		'tanggal_bayar',
		'note'
	);

	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

    function getPaymentComposite($header_id){
    	$sql = "SELECT trans_sales_invoice_payment.*, master_rekening_bank.bank AS nama_bank ". 
				"from trans_sales_invoice_payment ".
				"LEFT JOIN master_rekening_bank ON (trans_sales_invoice_payment.bank = master_rekening_bank.id) ".
				"WHERE trans_sales_invoice_payment.record_status = '".STATUS_ACTIVE."' AND ".
				"trans_sales_invoice_payment.detail_status = '".STATUS_ACTIVE."' AND ".
				"trans_sales_invoice_payment.header_id = '".$header_id."' ";
		$query = $this->db->query($sql);
		$res = $query->result();
		return $res;
	}
	
     function cancelPayment($header_id){
		$sql = "UPDATE trans_sales_invoice_payment SET detail_status = '".STATUS_CANCEL."' ".
				"WHERE header_id = ".$header_id." ";
		$query = $this->db->query($sql);
		return $query;
	}

}

?>