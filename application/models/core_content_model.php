<?php
class Core_content_model extends Core_master_model{	    	
	public $table_name = "core_content";
	var $default_value_list = array(
		'id' => NULL ,
		'title' => NULL ,
		'record_type' => NULL ,
		'text_01' => NULL ,
		'text_02' => NULL ,
		'text_03' => NULL ,
		'text_04' => NULL ,
		'text_05' => NULL ,
		'text_06' => NULL ,
		'text_07' => NULL ,
		'text_08' => NULL ,
		'text_09' => NULL ,
		'int_01' => NULL ,
		'int_02' => NULL ,
		'int_03' => NULL ,
		'int_04' => NULL ,
		'int_05' => NULL ,
		'int_06' => NULL ,
		'int_07' => NULL ,
		'int_08' => NULL ,
		'int_09' => NULL ,
		'note_01' => NULL ,
		'note_02' => NULL ,
		'note_03' => NULL ,
		'note_04' => NULL ,
		'note_05' => NULL ,
		'note_06' => NULL ,
		'note_07' => NULL ,
		'note_08' => NULL ,
		'note_09' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE
	);
	var $fillable_value_list = array(
		'title'
	);
	var $value_list  = array();
	var $array_condition  = array();
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }
	
}

?>