<?php
class Trans_sales_invoice_detail_model extends Core_master_model{	    	
	public $table_name = "trans_sales_invoice_detail";
	var $default_value_list = Array(
		'id' => NULL ,
		'header_id' => NULL ,
		'barang' => NULL ,
		'note' => '' ,
		'quantity' => 0,
		'harga_modal_unit' => 0 ,
		'harga_unit' => 0 ,
		'discount' => 0 ,
		'tax' => 0 ,
		'jumlah' => 0 ,
		'serial_no' => '' ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'detail_status' => STATUS_PENDING
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $fillable_value_list = array(
		'barang',
		//'note',
		'quantity',
		//'harga_modal_unit',
		'harga_unit',
		'discount',
		'tax',
		'jumlah',
		'serial_no'
	);

	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

	function getTransDetailCompositeByHeader($header_id){								
		$sql = "SELECT trans_sales_invoice_detail.*, ".
				"master_barang.kode AS text_kode_barang, ".
				"master_barang.nama AS text_nama_barang ".
				"from trans_sales_invoice_detail " .
				"LEFT JOIN master_barang ON (master_barang.id = trans_sales_invoice_detail.barang) ".
				"WHERE trans_sales_invoice_detail.record_status = '".STATUS_ACTIVE."' AND header_id = '".$header_id."' ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}   

	function getSearchExporTransDetailCompositeByHeaderBarang($q){								
		$sql = "SELECT trans_sales_invoice_detail.*, ".
				"master_barang.kode AS text_kode_barang, ".
				"master_barang.nama AS text_nama_barang, ".
				"trans_sales_invoice_header.no_transaksi ".
				"from trans_sales_invoice_detail " .
				"LEFT JOIN master_barang ON (master_barang.id = trans_sales_invoice_detail.barang) ".
				"LEFT JOIN trans_sales_invoice_header ON (trans_sales_invoice_header.id = trans_sales_invoice_detail.header_id) ".
				"WHERE trans_sales_invoice_detail.record_status = '".STATUS_ACTIVE."' ".
				"AND trans_sales_invoice_detail.detail_status = '".STATUS_PENDING."' ".
				"AND trans_sales_invoice_header.expor = '1' ".
				"AND (trans_sales_invoice_header.no_transaksi LIKE '%".$q."%' ".
				"OR master_barang.nama LIKE '%".$q."%' ) ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}    

	function getExporTransDetailComposite($id){								
		$sql = "SELECT trans_sales_invoice_detail.*, ".
				"master_barang.kode AS text_kode_barang, ".
				"master_barang.nama AS text_nama_barang, ".
				"trans_sales_invoice_header.no_transaksi ".
				"from trans_sales_invoice_detail " .
				"LEFT JOIN master_barang ON (master_barang.id = trans_sales_invoice_detail.barang) ".
				"LEFT JOIN trans_sales_invoice_header ON (trans_sales_invoice_header.id = trans_sales_invoice_detail.header_id) ".
				"WHERE trans_sales_invoice_detail.record_status = '".STATUS_ACTIVE."' ".
				"AND trans_sales_invoice_detail.detail_status = '".STATUS_PENDING."' ".
				//"AND trans_sales_invoice_header.expor = '1' ".
				"AND trans_sales_invoice_detail.id = '".$id."' ";
		$query = $this->db->query($sql);
		$res = $query->row_array(0);
		return $res;
	}  
}

?>