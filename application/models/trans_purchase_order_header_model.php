<?php
class Trans_purchase_order_header_model extends Core_master_model{	    	
	public $table_name = "trans_purchase_order_header";
	var $default_value_list = Array(
		'id' => NULL ,
		'no_transaksi' => '' ,
		'tanggal' => '' ,
		'supplier' => NULL ,
		'ship_to' => '' ,
		'terms' => '' ,
		'fob' => '' ,
		'ship_via' => '' ,
		'expected_date' => NULL ,
		'taxable_vendor' => 0 ,
		'rate' => '' ,
		'faktur_pajak' => '',
		'expor' => 0,
		'note' => '' ,
		'subtotal' => 0 ,
		'discount_value' => 0 ,
		'ppn_rate' => 0 ,
		'ppn_value' => 0 ,
		'freight_value' => 0 ,
		'total' => 0 ,
		'sisa' => 0,
		'prepared_by' => NULL ,
		'prepared_date' => NULL ,
		'approved_by' => NULL ,
		'approved_date' => NULL ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'trans_status' => STATUS_PENDING,
		'pembayaran_status' => STATUS_PENDING
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'trans_purchase_order_header.id' ,
		1 => 'trans_purchase_order_header.no_transaksi',
		2 => 'trans_purchase_order_header.tanggal' ,
		3 => 'master_supplier.nama AS nama_supplier' ,
		4 => 'trans_purchase_order_header.faktur_pajak' ,
		5 => 'trans_purchase_order_header.trans_status' ,
		6 => 'trans_purchase_order_header.pembayaran_status'
	);
	var $datatable_show_value_list = Array(
		0 => 'id' ,
		1 => 'no_transaksi',
		2 => 'tanggal' ,
		3 => 'nama_supplier' ,
		4 => 'faktur_pajak' ,
		5 => 'trans_status' ,
		6 => 'pembayaran_status'
	);
	var $datatable_search_value_list = Array(
		0 => 'trans_purchase_order_header.id' ,
		1 => 'trans_purchase_order_header.no_transaksi',
		2 => 'trans_purchase_order_header.tanggal' ,
		3 => 'master_supplier.nama' ,
		4 => 'trans_purchase_order_header.faktur_pajak' ,
		5 => 'trans_purchase_order_header.trans_status' ,
		6 => 'trans_purchase_order_header.pembayaran_status'
	);
	var $fillable_value_list = array(
		'supplier',
		'ship_to',
		'terms',
		'fob',
		'ship_via',
		'expected_date',
		'taxable_vendor',
		'rate',
		'faktur_pajak',
		'expor',
		'note',
		'subtotal',
		'discount_value',
		/*'ppn_rate',
		'ppn_value',*/
		'freight_value',
		'total'/*,
		'prepared_by',
		'prepared_date',
		'approved_by',
		'approved_date'*/
	);

	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

	function getDataTableCountActive($datatable_request){								
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM ".$this->table_name." ".
			"LEFT JOIN master_supplier ON (master_supplier.id = ".$this->table_name.".supplier) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." ".
			"LEFT JOIN master_supplier ON (master_supplier.id = ".$this->table_name.".supplier) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getListPembelianPerSupplier($tanggal_dari, $tanggal_ke){ 							
		$sql = "SELECT trans_purchase_order_header.*, master_supplier.nama AS nama_supplier ". 
				"from trans_purchase_order_header ".
				"LEFT JOIN master_supplier ON (trans_purchase_order_header.supplier = master_supplier.id) ".
				"WHERE trans_purchase_order_header.tanggal >= '".$tanggal_dari." 00:00:00' and trans_purchase_order_header.tanggal <= '".$tanggal_ke." 23:59:59' AND ".
				"trans_purchase_order_header.record_status = '".STATUS_ACTIVE."' AND ".
				"trans_purchase_order_header.trans_status = '".STATUS_COMPLETE."' ".
				"ORDER BY master_supplier.nama";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getListPembelianWithItemPerSupplier($tanggal_dari, $tanggal_ke){ 							
		$sql = "SELECT trans_purchase_order_header.*, master_supplier.nama AS nama_supplier, item.item ". 
				"from trans_purchase_order_header ".
				"LEFT JOIN master_supplier ON (trans_purchase_order_header.supplier = master_supplier.id) ".
				"LEFT JOIN (select header_id, group_concat(master_barang.kode) AS item 
					from trans_purchase_order_detail
					LEFT JOIN master_barang ON (master_barang.id = trans_purchase_order_detail.barang)
					group by header_id) item on (item.header_id = trans_purchase_order_header.id) ".
				"WHERE trans_purchase_order_header.tanggal >= '".$tanggal_dari." 00:00:00' and trans_purchase_order_header.tanggal <= '".$tanggal_ke." 23:59:59' AND ".
				"trans_purchase_order_header.record_status = '".STATUS_ACTIVE."' AND ".
				"trans_purchase_order_header.trans_status = '".STATUS_COMPLETE."' ".
				"ORDER BY master_supplier.nama";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
}

?>