<?php
class Trans_cash_bank_model extends Core_master_model{	    	
	public $table_name = "trans_cash_bank";
	var $default_value_list = Array(
		'id' => NULL ,
		'no_transaksi' => '' ,
		'tanggal' => '' ,
		'bank' => NULL ,
		'type' => NULL ,
		'kredit' => 0 ,
		'debit' => 0 ,
		'saldo_awal' => 0 ,
		'saldo_akhir' => 0 ,
		'saldo_bank' => NULL ,
		'note' => '' ,
		'cek_giro' => '',
		'reff_id' => NULL,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'trans_status' => STATUS_ACTIVE
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'id' ,
		1 => 'no_transaksi',
		2 => 'tanggal' ,
		3 => 'format(saldo_awal, 0) AS saldo_awal' ,
		4 => 'format(debit, 0) AS debit' ,
		5 => 'format(kredit, 0) AS kredit' ,
		6 => 'format(saldo_akhir, 0) AS saldo_akhir' ,
		//7 => 'note' 
	);
	var $datatable_show_value_list = Array(
		0 => 'id' ,
		1 => 'no_transaksi',
		2 => 'tanggal' ,
		3 => 'saldo_awal',
		4 => 'debit' ,
		5 => 'kredit' ,
		6 => 'saldo_akhir' ,
		//7 => 'note' 
	);
	var $datatable_search_value_list = Array(
		0 => 'id' ,
		1 => 'no_transaksi',
		2 => 'tanggal' ,
		/*2 => 'format(kredit, 0)' ,
		3 => 'format(debit, 0)' ,
		4 => 'format(saldo_akhir, 0)' ,*/
		3 => 'saldo_awal' ,
		4 => 'debit' ,
		5 => 'kredit' ,
		6 => 'saldo_akhir' ,
		//7 => 'note'  
	);
	var $fillable_value_list = array(
		'bank',
		'type',
		'kredit',
		'debit',
		'saldo_awal',
		'saldo_akhir',
		'saldo_bank',
		'note',
		'cek_giro'
	);
	var $prefix = "KAS";

	var $query_buku_kas = "SELECT ". 
						"s.id, s.no_transaksi, s.bank, ".
					    "s.tanggal, ".
					    "s.debit, s.kredit, ".
					    "@b as saldo_awal, ".
					    "@b := @b + s.debit - s.kredit AS saldo_akhir, ".
					    "s.note, s.record_status, s.trans_status ".
						"FROM ".
					    "(SELECT @b := 0) AS dummy ".
					  	"CROSS JOIN trans_cash_bank AS s ".
						"ORDER BY tanggal";

	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

    function getPrefix(){
    	return $this->prefix;
    }

    function getDataTableCountActive($datatable_request){								
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM ".$this->table_name." ".
			//"LEFT JOIN master_type_cash_bank ON (master_type_cash_bank.id = ".$this->table_name.".type) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' AND ".$this->table_name.".trans_status = '".STATUS_COMPLETE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}

		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		//$sql.=" FROM ".$this->table_name." ".
		$sql.=" FROM (".$this->query_buku_kas.") trans_cash_bank ".
			//"LEFT JOIN master_type_cash_bank ON (master_type_cash_bank.id = ".$this->table_name.".type) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' AND ".$this->table_name.".trans_status = '".STATUS_COMPLETE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";

		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

    function getSaldo(){
		$sql = "SELECT saldo_akhir ".
				//"FROM ".$this->table_name." ".
				"FROM (".$this->query_buku_kas.") trans_cash_bank ".
				"ORDER BY id DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['saldo_akhir'];
		}
	}

	function getHistoryPerBank($bank){ 							
		/*$sql = "SELECT trans_cash_bank.* ".
				"from trans_cash_bank " .
				"LEFT JOIN master_rekening_bank ON (master_rekening_bank.id = trans_cash_bank.bank) ".
				"WHERE true AND ". 
				"trans_cash_bank.record_status = '".STATUS_ACTIVE."' AND trans_cash_bank.bank = '".$bank."' ".
				"ORDER BY tanggal ";*/
		$sql = "SELECT trans_cash_bank.* ".
				"from " .
				"(SELECT ". 
				"s.id, s.no_transaksi, s.bank, ".
			    "s.tanggal, ".
			    "s.debit, s.kredit, ".
			    "@b as saldo_awal, ".
			    "@b := @b + s.debit - s.kredit AS saldo_bank, ".
			    "s.note, s.record_status, s.trans_status ".
				"FROM ".
			    "(SELECT @b := 0) AS dummy ".
			  	"CROSS JOIN trans_cash_bank AS s ".
			  	"WHERE s.bank = '".$bank."' ".
				"ORDER BY tanggal) trans_cash_bank ".
				"LEFT JOIN master_rekening_bank ON (master_rekening_bank.id = trans_cash_bank.bank) ".
				"WHERE true AND ". 
				"trans_cash_bank.record_status = '".STATUS_ACTIVE."' AND trans_cash_bank.trans_status = '".STATUS_COMPLETE."' ".
				"ORDER BY tanggal ";
				
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getHistoryPerBankRange($bank, $tanggal_dari, $tanggal_ke){ 							
		/*$sql = "SELECT trans_cash_bank.* ".
				"from trans_cash_bank " .
				"LEFT JOIN master_rekening_bank ON (master_rekening_bank.id = trans_cash_bank.bank) ".
				"WHERE true AND ". 
				"trans_cash_bank.record_status = '".STATUS_ACTIVE."' AND trans_cash_bank.bank = '".$bank."' ".
				"ORDER BY tanggal ";*/
		$sql = "SELECT trans_cash_bank.* ".
				"from " .
				"(SELECT ". 
				"s.id, s.no_transaksi, s.bank, ".
			    "s.tanggal, ".
			    "s.debit, s.kredit, ".
			    "@b as saldo_awal, ".
			    "@b := @b + s.debit - s.kredit AS saldo_bank, ".
			    "s.note, s.record_status, s.trans_status ".
				"FROM ".
			    "(SELECT @b := 0) AS dummy ".
			  	"CROSS JOIN trans_cash_bank AS s ".
			  	"WHERE s.bank = '".$bank."' ".
				"ORDER BY tanggal) trans_cash_bank ".
				"LEFT JOIN master_rekening_bank ON (master_rekening_bank.id = trans_cash_bank.bank) ".
				"WHERE true AND ". 
				"trans_cash_bank.tanggal >= '".$tanggal_dari." 00:00:00' and trans_cash_bank.tanggal <= '".$tanggal_ke." 23:59:59' AND ".
				"trans_cash_bank.record_status = '".STATUS_ACTIVE."' AND trans_cash_bank.trans_status = '".STATUS_COMPLETE."' ".
				"ORDER BY tanggal ";
				
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getSaldoAwal($bulan, $tahun){
		$sql = "SELECT saldo_awal ".
				//"FROM ".$this->table_name." ".
				"FROM (".$this->query_buku_kas.") trans_cash_bank ".
				"WHERE MONTH(tanggal) = '".$bulan."' AND YEAR(tanggal) = '".$tahun."' ".
				"AND record_status = '".STATUS_ACTIVE."' AND trans_status = '".STATUS_COMPLETE."'".
				"ORDER BY id ASC LIMIT 1 ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['saldo_awal'];
		}
	}

	function getSaldoAkhir($bulan, $tahun){
		$sql = "SELECT saldo_akhir ".
				//"FROM ".$this->table_name." ".
				"FROM (".$this->query_buku_kas.") trans_cash_bank ".
				"WHERE MONTH(tanggal) = '".$bulan."' AND YEAR(tanggal) = '".$tahun."' ".
				"AND record_status = '".STATUS_ACTIVE."' AND trans_status = '".STATUS_COMPLETE."'".
				"ORDER BY id DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['saldo_akhir'];
		}
	}

	function getTotalDebit($bulan, $tahun){
		$sql = "SELECT SUM(debit) AS total_debit, SUM(kredit) AS total_kredit ".
				"FROM ".$this->table_name." ".
				"WHERE MONTH(tanggal) = '".$bulan."' AND YEAR(tanggal) = '".$tahun."' ".
				"AND record_status = '".STATUS_ACTIVE."' AND trans_status = '".STATUS_COMPLETE."' ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['total_debit'];
		}
	}

	function getTotalKredit($bulan, $tahun){
		$sql = "SELECT SUM(debit) AS total_debit, SUM(kredit) AS total_kredit ".
				"FROM ".$this->table_name." ".
				"WHERE MONTH(tanggal) = '".$bulan."' AND YEAR(tanggal) = '".$tahun."' ".
				"AND record_status = '".STATUS_ACTIVE."' AND trans_status = '".STATUS_COMPLETE."' ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['total_kredit'];
		}
	}

	function getDataDebitPerType($bulan, $tahun){ 	
		$sql = "SELECT trans_cash_bank.type, master_type_cash_bank.kode AS kode_cash_bank, master_type_cash_bank.nama AS type_cash_bank, ".
				"SUM(debit) AS total_debit ".
				"from trans_cash_bank " .
				"LEFT JOIN master_type_cash_bank ON (trans_cash_bank.type = master_type_cash_bank.id) ".
				"WHERE debit != 0 ".
				//"AND MONTH(trans_cash_bank.tanggal) = '".$bulan."' AND YEAR(trans_cash_bank.tanggal) = '".$tahun."' ".
				"AND trans_cash_bank.record_status = '".STATUS_ACTIVE."' AND trans_cash_bank.trans_status = '".STATUS_COMPLETE."' ".
				"GROUP BY trans_cash_bank.type, master_type_cash_bank.kode, master_type_cash_bank.nama ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getDataDebitDetailPerType($bulan, $tahun){ 	
		$sql = "SELECT trans_cash_bank.type, master_type_cash_bank.kode AS kode_cash_bank, master_type_cash_bank.nama AS type_cash_bank, ".
				"master_rekening_bank.bank, master_rekening_bank.no_rekening, master_rekening_bank.nama_rekening,  ".
				"tanggal, debit AS jumlah, note  ".
				"from trans_cash_bank   ".
				"LEFT JOIN master_type_cash_bank ON (trans_cash_bank.type = master_type_cash_bank.id)  ".
				"LEFT JOIN master_rekening_bank ON (trans_cash_bank.bank = master_rekening_bank.id)  ".
				"WHERE debit != 0 ".
				"AND MONTH(trans_cash_bank.tanggal) = '".$bulan."' AND YEAR(trans_cash_bank.tanggal) = '".$tahun."' ".
				"AND trans_cash_bank.record_status = '".STATUS_ACTIVE."' AND trans_cash_bank.trans_status = '".STATUS_COMPLETE."' ".
				"ORDER BY trans_cash_bank.tanggal ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getDataKreditPerType($bulan, $tahun){ 	
		$sql = "SELECT trans_cash_bank.type, master_type_cash_bank.kode AS kode_cash_bank, master_type_cash_bank.nama AS type_cash_bank, ".
				"SUM(kredit) AS total_kredit ".
				"from trans_cash_bank " .
				"LEFT JOIN master_type_cash_bank ON (trans_cash_bank.type = master_type_cash_bank.id) ".
				"WHERE kredit != 0 ".
				//"AND MONTH(trans_cash_bank.tanggal) = '".$bulan."' AND YEAR(trans_cash_bank.tanggal) = '".$tahun."' ".
				"AND trans_cash_bank.record_status = '".STATUS_ACTIVE."' AND trans_cash_bank.trans_status = '".STATUS_COMPLETE."' ".
				"GROUP BY trans_cash_bank.type, master_type_cash_bank.kode, master_type_cash_bank.nama ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getDataKreditDetailPerType($bulan, $tahun){ 	
		$sql = "SELECT trans_cash_bank.type, master_type_cash_bank.kode AS kode_cash_bank, master_type_cash_bank.nama AS type_cash_bank, ".
				"master_rekening_bank.bank, master_rekening_bank.no_rekening, master_rekening_bank.nama_rekening,  ".
				"tanggal, kredit AS jumlah, note  ".
				"from trans_cash_bank   ".
				"LEFT JOIN master_type_cash_bank ON (trans_cash_bank.type = master_type_cash_bank.id)  ".
				"LEFT JOIN master_rekening_bank ON (trans_cash_bank.bank = master_rekening_bank.id)  ".
				"WHERE kredit != 0 ".
				"AND MONTH(trans_cash_bank.tanggal) = '".$bulan."' AND YEAR(trans_cash_bank.tanggal) = '".$tahun."' ".
				"AND trans_cash_bank.record_status = '".STATUS_ACTIVE."' AND trans_cash_bank.trans_status = '".STATUS_COMPLETE."' ".
				"ORDER BY trans_cash_bank.tanggal ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getBukuBesar($tanggal_dari, $tanggal_ke){ 	
		/*$sql = "SELECT trans_cash_bank.*, ".
				"master_rekening_bank.bank, master_rekening_bank.no_rekening, master_rekening_bank.nama_rekening  ".
				"FROM trans_cash_bank ".
				"LEFT JOIN master_rekening_bank ON (trans_cash_bank.bank = master_rekening_bank.id)  ".
				"WHERE trans_cash_bank.tanggal >= '".$tanggal_dari." 00:00:00' and trans_cash_bank.tanggal <= '".$tanggal_ke." 23:59:59' AND ".
				"trans_cash_bank.record_status = '".STATUS_ACTIVE."'";*/

		$sql = "SELECT trans_cash_bank.*, ".
				"master_rekening_bank.bank, master_rekening_bank.no_rekening, master_rekening_bank.nama_rekening  ".
				"FROM (".$this->query_buku_kas.") trans_cash_bank ".
				"LEFT JOIN master_rekening_bank ON (trans_cash_bank.bank = master_rekening_bank.id)  ".
				"WHERE trans_cash_bank.tanggal >= '".$tanggal_dari." 00:00:00' and trans_cash_bank.tanggal <= '".$tanggal_ke." 23:59:59' AND ".
				"trans_cash_bank.record_status = '".STATUS_ACTIVE."' AND trans_cash_bank.trans_status = '".STATUS_COMPLETE."'";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getTotalPenjualan($tanggal_dari, $tanggal_ke, $expor = 0){
		$sql = "SELECT SUM(total) as total ".
				"FROM trans_sales_invoice_header ".
				"WHERE trans_sales_invoice_header.tanggal >= '".$tanggal_dari." 00:00:00' and trans_sales_invoice_header.tanggal <= '".$tanggal_ke." 23:59:59' ".
				"AND expor = '".$expor."' ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['total'];
		}
	}

	function getTotalPembelian($tanggal_dari, $tanggal_ke, $expor = 0){
		$sql = "SELECT SUM(total) as total ".
				"FROM trans_purchase_order_header ".
				"WHERE trans_purchase_order_header.tanggal >= '".$tanggal_dari." 00:00:00' and trans_purchase_order_header.tanggal <= '".$tanggal_ke." 23:59:59' ".
				"AND expor = '".$expor."' ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['total'];
		}
	}


	function getSaldoAwalRange($tanggal_dari, $tanggal_ke){
		$sql = "SELECT saldo_awal ".
				//"FROM ".$this->table_name." ".
				"FROM (".$this->query_buku_kas.") trans_cash_bank ".
				"WHERE trans_cash_bank.tanggal >= '".$tanggal_dari." 00:00:00' and trans_cash_bank.tanggal <= '".$tanggal_ke." 23:59:59' ".
				"AND record_status = '".STATUS_ACTIVE."' AND trans_status = '".STATUS_COMPLETE."' ".
				"ORDER BY id ASC LIMIT 1 ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['saldo_awal'];
		}
	}

	function getSaldoAkhirRange($tanggal_dari, $tanggal_ke){
		$sql = "SELECT saldo_akhir ".
				//"FROM ".$this->table_name." ".
				"FROM (".$this->query_buku_kas.") trans_cash_bank ".
				"WHERE trans_cash_bank.tanggal >= '".$tanggal_dari." 00:00:00' and trans_cash_bank.tanggal <= '".$tanggal_ke." 23:59:59' ".
				"AND record_status = '".STATUS_ACTIVE."' AND trans_status = '".STATUS_COMPLETE."' ".
				"ORDER BY id DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['saldo_akhir'];
		}
	}

	/*belong to ancient method*/
	function getSaldoPerTanggal($tanggal){
		$sql = "SELECT saldo_akhir ".
				"FROM ".$this->table_name." ".
				"WHERE tanggal < '".$tanggal."' ".
				"ORDER BY tanggal DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['saldo_akhir'];
		}
	}

	/*belong to ancient method*/
	function updateCashBank($jumlah, $tanggal, $exclude_id){
		$sql = "UPDATE trans_cash_bank SET saldo_awal = (saldo_awal+".intval($jumlah)."), saldo_akhir = (saldo_akhir+".intval($jumlah).") ".
				"WHERE tanggal >= '".$tanggal."' AND id != ".$exclude_id." ";
		$query = $this->db->query($sql);
		return $query;
	}

	/*belong to ancient method*/
	function getSaldoBankPerTanggal($tanggal, $bank){
		$sql = "SELECT saldo_bank ".
				"FROM ".$this->table_name." ".
				"WHERE tanggal < '".$tanggal."' AND bank = '".$bank."' ".
				"ORDER BY tanggal DESC LIMIT 1 ";
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		if($row == null){
			return '0';
		} else {
			return $row['saldo_bank'];
		}
	}

	/*belong to ancient method*/
	function updateBankCashBank($jumlah, $tanggal, $exclude_id, $bank){
		$sql = "UPDATE ".$this->table_name." SET saldo_bank = (saldo_bank+".intval($jumlah).") ".
				"WHERE tanggal >= '".$tanggal."' AND id != ".$exclude_id." AND bank = ".$bank." ";

		$query = $this->db->query($sql);
		return $query;
	}

	/*Per bank*/
	function getCountDataBankActive($bank){		
		$this->array_condition = array('record_status' => STATUS_ACTIVE, 'trans_status' => STATUS_COMPLETE, 'bank' => $bank);
		$this->db->where($this->array_condition);
		$this->db->from($this->table_name);
		$counted = $this->db->count_all_results();
		return $counted;
	}
	
	function getDataTableCountBankActive($bank, $datatable_request){	
		$query_buku_bank = $this->getQueryBukuBank($bank);
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM (".$query_buku_bank.") ".$this->table_name." ".
			//"LEFT JOIN master_type_cash_bank ON (master_type_cash_bank.id = ".$this->table_name.".type) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' AND ".$this->table_name.".trans_status = '".STATUS_COMPLETE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}

		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataBankActive($bank, $datatable_request){								
		$query_buku_bank = $this->getQueryBukuBank($bank);

		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		//$sql.=" FROM ".$this->table_name." ".
		$sql.=" FROM (".$query_buku_bank.") trans_cash_bank ".
			//"LEFT JOIN master_type_cash_bank ON (master_type_cash_bank.id = ".$this->table_name.".type) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' AND ".$this->table_name.".trans_status = '".STATUS_COMPLETE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";

		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}



	/*****/
	function getQueryBukuBank($bank){
		$query = "SELECT ". 
				"s.id, s.no_transaksi, s.bank, ".
			    "s.tanggal, ".
			    "s.debit, s.kredit, ".
			    "@b as saldo_awal, ".
			    "@b := @b + s.debit - s.kredit AS saldo_akhir, ".
			    "s.note, s.record_status, s.trans_status ".
				"FROM ".
			    "(SELECT @b := 0) AS dummy ".
			  	"CROSS JOIN trans_cash_bank AS s ".
			  	"WHERE s.bank = '".$bank."' ".
				"ORDER BY tanggal";

		return $query;
	}
}

?>