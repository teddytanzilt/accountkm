<?php
class Trans_sales_invoice_header_model extends Core_master_model{	    	
	public $table_name = "trans_sales_invoice_header";
	var $default_value_list = Array(
		'id' => NULL ,
		'no_transaksi' => '' ,
		'tanggal' => '' ,
		'customer' => NULL ,
		'do' => NULL,
		'ship_to' => '' ,
		'terms' => '' ,
		'fob' => '' ,
		'ship_via' => '' ,
		'ship_date' => NULL ,
		'po_no' => '' ,
		'currency' => '' ,
		'faktur_pajak' => '',
		'expor' => 0,
		'note' => '' ,
		'subtotal' => 0 ,
		'discount_value' => 0 ,
		'ppn_rate' => 0 ,
		'ppn_value' => 0 ,
		'freight_value' => 0 ,
		'total' => 0 ,
		'sisa' => 0,
		'prepared_by' => NULL ,
		'prepared_date' => NULL ,
		'approved_by' => NULL ,
		'approved_date' => NULL ,
		'shipped_by' => NULL ,
		'shipped_date' => NULL ,
		'received_by' => NULL ,
		'received_date' => NULL ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'trans_status' => STATUS_PENDING,
		'pembayaran_status' => STATUS_PENDING,
		'delivery_status' => STATUS_PENDING
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'trans_sales_invoice_header.id' ,
		1 => 'trans_sales_invoice_header.no_transaksi',
		2 => 'trans_sales_invoice_header.tanggal' ,
		3 => 'master_customer.nama AS nama_customer' ,
		4 => 'trans_sales_invoice_header.faktur_pajak' ,
		5 => 'trans_sales_invoice_header.po_no' ,
		6 => 'trans_sales_invoice_header.trans_status' ,
		//7 => 'trans_sales_invoice_header.delivery_status',
		7 => 'trans_sales_invoice_header.pembayaran_status'
	);
	var $datatable_show_value_list = Array(
		0 => 'id' ,
		1 => 'no_transaksi',
		2 => 'tanggal' ,
		3 => 'nama_customer' ,
		4 => 'faktur_pajak' ,
		5 => 'po_no' ,
		6 => 'trans_status' ,
		//7 => 'delivery_status',
		7 => 'pembayaran_status'
	);
	var $datatable_search_value_list = Array(
		0 => 'trans_sales_invoice_header.id' ,
		1 => 'trans_sales_invoice_header.no_transaksi',
		2 => 'trans_sales_invoice_header.tanggal' ,
		3 => 'master_customer.nama' ,
		4 => 'trans_sales_invoice_header.faktur_pajak' ,
		5 => 'trans_sales_invoice_header.po_no' ,
		6 => 'trans_sales_invoice_header.trans_status' ,
		//7 => 'trans_sales_invoice_header.delivery_status',
		7 => 'trans_sales_invoice_header.pembayaran_status'
	);

	var $datatable_do_value_list = Array(
		0 => 'trans_sales_invoice_header.id' ,
		1 => 'trans_sales_invoice_header.no_transaksi',
		2 => 'trans_sales_invoice_header.tanggal' ,
		3 => 'master_customer.nama AS nama_customer' ,
		4 => 'trans_sales_invoice_header.faktur_pajak' ,
		5 => 'trans_sales_invoice_header.delivery_status' 
	);
	var $datatable_do_show_value_list = Array(
		0 => 'id' ,
		1 => 'no_transaksi',
		2 => 'tanggal' ,
		3 => 'nama_customer' ,
		4 => 'faktur_pajak' ,
		5 => 'delivery_status',
	);
	var $datatable_do_search_value_list = Array(
		0 => 'trans_sales_invoice_header.id' ,
		1 => 'trans_sales_invoice_header.no_transaksi',
		2 => 'trans_sales_invoice_header.tanggal' ,
		3 => 'master_customer.nama' ,
		4 => 'trans_sales_invoice_header.faktur_pajak', 
		5 => 'trans_sales_invoice_header.delivery_status' 
	);
	var $fillable_value_list = array(
		'customer',
		'do',
		'ship_to',
		'terms',
		'fob',
		'ship_via',
		'ship_date',
		'po_no',
		'currency',
		'faktur_pajak',
		'expor',
		'note',
		'subtotal',
		'discount_value',
		/*'ppn_rate',
		'ppn_value',*/
		'freight_value',
		'total'/*,
		'prepared_by',
		'prepared_date',
		'approved_by',
		'approved_date',
		'shipped_by',
		'shipped_date',
		'received_by',
	'received_date'*/
	);

	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

	function getDataTableCountActive($datatable_request){								
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM ".$this->table_name." ".
			"LEFT JOIN master_customer ON (master_customer.id = ".$this->table_name.".customer) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." ".
			"LEFT JOIN master_customer ON (master_customer.id = ".$this->table_name.".customer) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getDatatableDoShowValueList() {
	   return $this->datatable_do_show_value_list;
	}

	function getDataTableDoData($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_do_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." WHERE 1=1 ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_do_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_do_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getCountDoDataActive(){		
		$this->array_condition = array('record_status' => STATUS_ACTIVE, 'trans_status' => STATUS_COMPLETE);
		$this->db->where($this->array_condition);
		$this->db->from($this->table_name);
		$counted = $this->db->count_all_results();
		return $counted;
	}

	function getDataTableDoCountActive($datatable_request){								
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM ".$this->table_name." ".
			"LEFT JOIN master_customer ON (master_customer.id = ".$this->table_name.".customer) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' AND  ".$this->table_name.".trans_status = '".STATUS_COMPLETE."'";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_do_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDoDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_do_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." ".
			"LEFT JOIN master_customer ON (master_customer.id = ".$this->table_name.".customer) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' AND ".$this->table_name.".trans_status = '".STATUS_COMPLETE."'";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_do_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_do_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
	
	function getListPenjualanPerCustomer($tanggal_dari, $tanggal_ke){ 							
		$sql = "SELECT trans_sales_invoice_header.*, master_customer.nama AS nama_customer ". 
				"from trans_sales_invoice_header ".
				"LEFT JOIN master_customer ON (trans_sales_invoice_header.customer = master_customer.id) ".
				"WHERE trans_sales_invoice_header.tanggal >= '".$tanggal_dari." 00:00:00' and trans_sales_invoice_header.tanggal <= '".$tanggal_ke." 23:59:59' AND ".
				"trans_sales_invoice_header.record_status = '".STATUS_ACTIVE."' AND ".
				"trans_sales_invoice_header.trans_status = '".STATUS_COMPLETE."' ".
				"ORDER BY master_customer.nama";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getListPenjualanWithItemPerCustomer($tanggal_dari, $tanggal_ke){ 							
		$sql = "SELECT trans_sales_invoice_header.*, master_customer.nama AS nama_customer, item.item ". 
				"from trans_sales_invoice_header ".
				"LEFT JOIN master_customer ON (trans_sales_invoice_header.customer = master_customer.id) ".
				"LEFT JOIN (select header_id, group_concat(master_barang.kode) AS item 
					from trans_sales_invoice_detail
					LEFT JOIN master_barang ON (master_barang.id = trans_sales_invoice_detail.barang)
					group by header_id) item on (item.header_id = trans_sales_invoice_header.id) ".
				"WHERE trans_sales_invoice_header.tanggal >= '".$tanggal_dari." 00:00:00' and trans_sales_invoice_header.tanggal <= '".$tanggal_ke." 23:59:59' AND ".
				"trans_sales_invoice_header.record_status = '".STATUS_ACTIVE."' AND ".
				"trans_sales_invoice_header.trans_status = '".STATUS_COMPLETE."' ".
				"ORDER BY master_customer.nama";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}

	function getListInvoiceNotDelivered(){ 							
		$sql = "SELECT trans_sales_invoice_header.* ". 
				"from trans_sales_invoice_header " .
				"WHERE trans_sales_invoice_header.record_status = '".STATUS_ACTIVE."' AND trans_sales_invoice_header.delivery_status = '".STATUS_PENDING."' AND trans_sales_invoice_header.trans_status = '".STATUS_COMPLETE."' ";
		$query = $this->db->query($sql);
		$res = $query->result();
		return $res;
	}
}

?>