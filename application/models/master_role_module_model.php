<?php
class Master_role_module_model extends Core_master_model{	    	
	public $table_name = "master_role_module";
	var $default_value_list = Array(
		'id_role' => NULL ,
		'id_module' => NULL 
	);
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

    function getRoleModuleNameListByRole($role_id){								
		$sql = "SELECT master_role_module.*, ".
				"master_module.kode AS kode_module, ".
				"master_module.nama AS nama_module, ".
				"master_module.link AS link_module ".
				"from master_role_module " .
				"LEFT JOIN master_module ON (master_module.id = master_role_module.id_module) ".
				"WHERE id_role = '".$role_id."' ";
		$query = $this->db->query($sql);
		$res = $query->result();
		return $res;
	}
}

?>