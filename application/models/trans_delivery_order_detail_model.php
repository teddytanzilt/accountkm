<?php
class Trans_delivery_order_detail_model extends Core_master_model{	    	
	public $table_name = "trans_delivery_order_detail";
	var $default_value_list = Array(
		'id' => NULL ,
		'header_id' => NULL ,
		'barang' => NULL ,
		'note' => '' ,
		'quantity' => 0,
		'serial_no' => '' ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'detail_status' => STATUS_ACTIVE
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $fillable_value_list = array(
		'barang',
		'note',
		'quantity'//,
		//'serial_no'
	);

	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

	function getTransDetailCompositeByHeader($header_id){								
		$sql = "SELECT trans_delivery_order_detail.*, ".
				"master_barang.kode AS text_kode_barang, ".
				"master_barang.nama AS text_nama_barang ".
				"from trans_delivery_order_detail " .
				"LEFT JOIN master_barang ON (master_barang.id = trans_delivery_order_detail.barang) ".
				"WHERE trans_delivery_order_detail.record_status = '".STATUS_ACTIVE."' AND header_id = '".$header_id."' ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}    

}

?>