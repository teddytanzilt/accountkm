<?php
class Core_app_config_model extends Core_master_model{	    	
	public $table_name = "core_app_config";
	var $default_value_list = Array(
		'id' => NULL ,
		'config_key' => '' ,
		'config_value' => ''
	);
	var $value_list  = array();
	var $array_condition  = array();
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }
	
	
	function exists($key)
	{
		$this->db->from($this->table_name);	
		$this->db->where('config_key',$key);
		$query = $this->db->get();
		return ($query->num_rows()==1);
	}

	function get_all()
	{
		$this->db->from($this->table_name);
		$this->db->order_by("config_key", "asc");
		return $this->db->get();		
	}

	function get($key)
	{
		return $this->config->item($key);
	}

	function save($key,$value)
	{
		$config_data=array(
			'config_key'=>$key,
			'config_value'=>$value
		);

		if (!$this->exists($key))
		{
			return $this->db->insert($this->table_name,$config_data);
		}

		$this->db->where('config_key', $key);
		return $this->db->update($this->table_name,$config_data);		
	}

	function get_raw_language_value()
	{
		$this->db->from($this->table_name);
		$this->db->where("config_key", "language");
		$row = $this->db->get()->row_array();
		if (!empty($row))
		{
			return $row['config_value'];
		}
		return '';	
	}

	function batch_save($data)
	{
		$success=true;
		$this->db->trans_start();
		foreach($data as $key=>$value)
		{
			if(!$this->save($key, $value))
			{
				$success=false;
				break;
			}
		}
		$this->db->trans_complete();		
		return $success;
	}

	function get_logo_image()
	{
		if ($this->config->item('company_logo'))
		{
			return site_url('app_files/view/'.$this->get('company_logo'));
		}
		return  base_url().'img/header_logo.png';
	}

	function get_additional_payment_types()
	{
		$return = array();
		$payment_types = $this->get('additional_payment_types');
		if ($payment_types)
		{
			$return = array_map('trim', explode(',',$payment_types));
		}
		return $return;
	}
}

?>