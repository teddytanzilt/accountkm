<?php
class Core_app_log_model extends Core_master_model{	    	
	public $table_name = "core_app_log";
	var $default_value_list = Array(
		'id' => NULL ,
		'ip' => '' ,
		'forwarded_ip' => '' ,
		'user_agent' => '' ,
		'accept_language' => '' ,
		'device_id' => '' ,
		'user_id' => NULL ,
		'event' => '' ,
		'message' => '' ,
		'module' => NULL ,
		'action' => '' ,
		'link' => '' ,
		'created_on' => NULL,
		'input_data' => NULL,
		'json_return' => NULL
	);
	var $value_list  = array();
	var $array_condition  = array();
	
	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }
}

?>