<?php
class Trans_delivery_order_header_model extends Core_master_model{	    	
	public $table_name = "trans_delivery_order_header";
	var $default_value_list = Array(
		'id' => NULL ,
		'no_transaksi' => '' ,
		'tanggal' => '' ,
		'customer' => NULL ,
		'invoice' => NULL ,
		'ship_to' => '' ,
		'ship_via' => '' ,
		'po_no' => '' ,
		'note' => '' ,
		'prepared_by' => NULL ,
		'prepared_date' => NULL ,
		'approved_by' => NULL ,
		'approved_date' => NULL ,
		'shipped_by' => NULL ,
		'shipped_date' => NULL ,
		'received_by' => NULL ,
		'received_date' => NULL ,
		'created_by' => NULL ,
		'created_on' => NULL ,
		'modified_by' => NULL ,
		'modified_on' => NULL ,
		'record_status' => STATUS_ACTIVE,
		'trans_status' => STATUS_PENDING,
		'pembayaran_status' => STATUS_PENDING
	);
	var $value_list  = array();
	var $array_condition  = array();
	var $datatable_value_list = Array(
		0 => 'trans_delivery_order_header.id' ,
		1 => 'trans_delivery_order_header.no_transaksi',
		2 => 'trans_delivery_order_header.tanggal' ,
		3 => 'master_customer.nama AS nama_customer' ,
		4 => 'trans_delivery_order_header.trans_status' 
	);
	var $datatable_show_value_list = Array(
		0 => 'id' ,
		1 => 'no_transaksi',
		2 => 'tanggal' ,
		3 => 'nama_customer' ,
		4 => 'trans_status',
	);
	var $datatable_search_value_list = Array(
		0 => 'trans_delivery_order_header.id' ,
		1 => 'trans_delivery_order_header.no_transaksi',
		2 => 'trans_delivery_order_header.tanggal' ,
		3 => 'master_customer.nama' ,
		4 => 'trans_delivery_order_header.trans_status' 
	);
	var $fillable_value_list = array(
		'customer',
		//'invoice',
		'ship_to',
		'ship_via',
		'po_no'/*,
		//'note',
		'prepared_by',
		'prepared_date',
		'approved_by',
		'approved_date',
		'shipped_by',
		'shipped_date',
		'received_by',
		'received_date'*/
	);

	function __construct()
    {        
        parent::__construct();	
		$this->value_list = $this->default_value_list;
    }

    function getDataTableCountActive($datatable_request){								
		$sql = "SELECT COUNT(".$this->table_name.".id) AS field_name ".
			"FROM ".$this->table_name." ".
			"LEFT JOIN master_customer ON (master_customer.id = ".$this->table_name.".customer) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";


		}
		log_message("INFO", $sql);
		$query = $this->db->query($sql);
		$row = $query->row_array(0);
		return $row['field_name'];
	}
	
	function getDataTableDataActive($datatable_request){								
		$sql = "SELECT ";
		foreach($this->datatable_value_list as $dt_id => $dt_val){
			$sql.=($dt_id==0)?$dt_val:",".$dt_val;
		}
		$sql.=" FROM ".$this->table_name." ".
			"LEFT JOIN master_customer ON (master_customer.id = ".$this->table_name.".customer) ".
			"WHERE ".$this->table_name.".record_status = '".STATUS_ACTIVE."' ";
		if( !empty($datatable_request['search']['value']) ) { 
			$sql.="AND (";
			foreach($this->datatable_search_value_list as $dt_id => $dt_val){
				$sql.=($dt_id==0)?$dt_val:"OR ".$dt_val;
				$sql.=" LIKE '%".$datatable_request['search']['value']."%' ";
			}
			$sql.=") ";
		}
		$sql.=" ORDER BY ". $this->datatable_search_value_list[$datatable_request['order'][0]['column']]."   ".$datatable_request['order'][0]['dir'];
		$sql.=" LIMIT ".$datatable_request['start']." ,".$datatable_request['length']."   ";
		$query = $this->db->query($sql);
		$res = $query->result_array();
		return $res;
	}
}

?>