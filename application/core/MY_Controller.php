<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    public $module_name = "";
	public $module_title = "";
	public $module_subtitle = "";
	public $report_title = "";
	public $table_name = "";
	public $model_object = null;
	public $model_detail_object = null;
	public $web_name = "";
	public $session_user_id = null;
	public $session_user_name = "";
	public $device_id = "";
	public $data_json = null;
	
	public $link_login = "";
	public $link_back = "";
	public $link_add = "";
	public $link_add_submit = "";
	public $link_edit = "";
	public $link_edit_submit = "";
	public $link_view = "";
	public $link_delete = "";

	public $view_list = "";
	public $view_edit = "";
	public $view_add = "";
	public $view_view = "";
	public $view_report = "";

	public $msg_add_success = "";
	public $msg_edit_success = "";
	public $msg_delete_success = "";

	public $multi_table_name = array();
	public $multi_model_object = array();
	
	public $validation_object = array();

	public $trans_prefix = "";
	
	public function __construct() {
        parent::__construct();
    }
}

class Crud_Controller extends MY_Controller {
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('session_user_id') != null){
			$this->session_user_id = $this->session->userdata('session_user_id');
			$this->session_user_name = $this->session->userdata('session_user_name');
			$this->device_id = "";
		} 
		
		$this->module_name = "content";
		$this->table_name = "tb_content";
		$this->model_object = $this->core_content_model;
		
		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";

		$this->msg_add_success = "Create Content Success.";
		$this->msg_edit_success = "Edit Content Success.";
		$this->msg_delete_success = "Delete Content Success.";
		
		$core_app_config_cond = array('config_key' => 'WEB_NAME');
		$this->core_app_config_model->setCond($core_app_config_cond);
		$this->web_name =  $this->core_app_config_model->getHeaderField("config_value");

		$this->config->load('custom');
    }
	
	public function index($data = null)
	{
		$this->module_subtitle = "List";
		$content_cond = array($this->table_name.'.record_status' => STATUS_ACTIVE);
		$this->model_object->setCond($content_cond);
		$data[$this->module_name.'_list'] = $this->model_object->getListArray();
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->view_list;		
		$this->load->view('parts/template',$data);
	}
	
	public function create($data = null)
	{
		$this->module_subtitle = "Create";
		$data = $this->create_load_data($data);
		$data['title'] = $this->web_name.' | '.$this->module_title;		
		$data['content'] = $this->view_add;		
		$this->load->view('parts/template',$data);
	}

	public function create_load_data($data = null) {
		return $data;
	}

	public function create_validation_data($result, $data) {
		return $result;
	}
	
	public function create_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						//$result['message'] = "Column Exist : ".$vo;
						$result['message'] = "Data Exist : ".$data[$vo];
					}
				}
			}
		}
		if($result['validation']) {
			$result = $this->create_validation_data($result, $data);
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$fillable_value = $this->model_object->getFillableValueList();
					$content_value = $this->model_object->getValueList();
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["created_by"] = $this->session_user_id;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->insertHeader();
					$content_id = $this->db->insert_id();
					if ($this->db->trans_status() === FALSE){ break; }

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_add_success);
			redirect(base_url().$this->link_back);
		} else {
			$this->create($data);
		}
	}
	
	public function edit($id, $data = null)
	{
		$this->module_subtitle = "Edit";
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		$content_detail = $this->model_object->getHeaderArray();	
		foreach($content_detail as $arr_name => $arr_value){
			$data[$arr_name] = $arr_value;
		}
		$data = $this->edit_load_data($data);
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->view_edit;		
		$this->load->view('parts/template',$data);
	}
	
	public function edit_load_data($data = null) {
		return $data;
	}

	public function edit_validation_data($result, $data) {
		return $result;
	}

	public function edit_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo], 'id !=' => $data['id']);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						//$result['message'] = "Column Exist : ".$vo;
						$result['message'] = "Data Exist : ".$data[$vo];
					}
				}
			}
		}	
		if($result['validation']) {
			$result = $this->edit_validation_data($result, $data);
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$fillable_value = $this->model_object->getFillableValueList();
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
					$this->model_object->setCond($content_cond);	
					$content_value = $this->model_object->getHeaderArray();
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["modified_by"] = $this->session_user_id;
					$content_value["modified_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_edit_success);
			redirect(base_url().$this->link_back);
		} else {
			$this->edit($data['id'], $data);
		}
	}
	
	public function delete_process($id)
	{
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		if(!$this->model_object->checkExist()){
			$result['validation'] = false;
			$result['message'] = "Invalid Content Id.";
		}
		if($result['validation']){
			$content_cond = array('record_status' => STATUS_DELETE, 'id' => $id);
			$this->model_object->setCond($content_cond);
			if($this->model_object->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Content has been deleted previously.";
			}
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/	
		if($result['validation']){
			try {
				$this->db->trans_begin();
				while(true) {
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
					$this->model_object->setCond($content_cond);	
					$content_value = $this->model_object->getHeaderArray();
					$content_value["record_status"] = STATUS_DELETE;
					$content_value["modified_by"] = $this->session_user_id;
					$content_value["modified_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		/*********Process ends here ***********/	
		if($result['validation']){
			$this->session->set_flashdata("success_message", $this->msg_delete_success);
			redirect(base_url().$this->link_back);
		} else {
			$this->index($result);
		}
	}

	public function view_load_data($data = null) {
		return $data;
	}

	public function view($id, $data = null)
	{
		$this->module_subtitle = "View";
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		$content_detail = $this->model_object->getHeaderArray();	
		foreach($content_detail as $arr_name => $arr_value){
			$data[$arr_name] = $arr_value;
		}
		$data = $this->view_load_data($data);
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->view_view;		
		$this->load->view('parts/template',$data);
	}
	
	public function get_data_list_json(){	
		$request_data= $_REQUEST;
		$column = $this->model_object->getDatatableValueList();
		$total_data = $this->model_object->getCountData();
		$total_filtered = $this->model_object->getDataTableCount($request_data);
		$data_filtered = $this->model_object->getDataTableData($request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}
	
	public function get_data_list_active_json(){	
		$request_data= $_REQUEST;
		$column = $this->model_object->getDatatableValueList();
		$total_data = $this->model_object->getCountDataActive();
		$total_filtered = $this->model_object->getDataTableCountActive($request_data);
		$data_filtered = $this->model_object->getDataTableDataActive($request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}
}

class Common_Controller extends MY_Controller {
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('session_user_id') != null){
			$this->session_user_id = $this->session->userdata('session_user_id');
			$this->session_user_name = $this->session->userdata('session_user_name');
			$this->device_id = "";
		} 

		$core_app_config_cond = array('config_key' => 'WEB_NAME');
		$this->core_app_config_model->setCond($core_app_config_cond);
		$this->web_name =  $this->core_app_config_model->getHeaderField("config_value");

		$this->config->load('custom');
    }
	
}

class Multi_Crud_Controller extends MY_Controller {
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('session_user_id') != null) {
			$this->session_user_id = $this->session->userdata('session_user_id');
			$this->session_user_name = $this->session->userdata('session_user_name');
			$this->device_id = "";
		} 
		
		$this->module_name = "content";

		$this->multi_table_name = array("tb_content");
		$this->multi_model_object = array($this->core_content_model);
		
		$this->link_back = ADMIN_FOLDER."/".$this->module_name;
		$this->link_add = ADMIN_FOLDER."/".$this->module_name."/create";
		$this->link_add_submit = ADMIN_FOLDER."/".$this->module_name."/create_process";
		$this->link_edit = ADMIN_FOLDER."/".$this->module_name."/edit/";
		$this->link_edit_submit = ADMIN_FOLDER."/".$this->module_name."/edit_process";
		$this->link_view = ADMIN_FOLDER."/".$this->module_name."/view/";
		$this->link_delete = ADMIN_FOLDER."/".$this->module_name."/delete_process/";
		
		$this->view_list = ADMIN_FOLDER."/".$this->module_name."/list";
		$this->view_edit = ADMIN_FOLDER."/".$this->module_name."/edit";
		$this->view_add = ADMIN_FOLDER."/".$this->module_name."/add";
		$this->view_view = ADMIN_FOLDER."/".$this->module_name."/view";

		$this->msg_add_success = "Create ".$this->module_name." success.";
		$this->msg_edit_success = "Edit ".$this->module_name." success.";
		$this->msg_delete_success = "Delete ".$this->module_name." success.";

		$this->config->load('custom');
    }
	
	public function index($data = null) {
		$this->module_subtitle = "List";
		$content_cond = array($this->multi_table_name[0].'.record_status' => STATUS_ACTIVE);
		$this->multi_model_object[0]->setCond($content_cond);
		$data[$this->module_name.'_list'] = $this->multi_model_object[0]->getListArray();
		$this->load->view($this->view_list, $data);
	}
	
	public function create($data = null) {
		$this->module_subtitle = "Create";
		$data = $this->create_load_data($data);
		$this->load->view($this->view_add, $data);
	}

	public function create_load_data($data = null) {
		return $data;
	}

	public function create_validation_data($result, $data) {
		return $result;
	}
	
	public function create_process() {
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/
		if($result['validation']){
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						//$result['message'] = "Column Exist : ".$vo;
						$result['message'] = "Data Exist : ".$data[$vo];
					}
				}
			}
		}
		if($result['validation']){
			$result = $this->create_validation_data($result, $data);
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				foreach($this->multi_model_object as $mmo){
					if($result['validation']){
						$fillable_value = $mmo->getFillableValueList();
						$content_value = $mmo->getValueList();
						foreach($fillable_value as $fv){
							$content_value[$fv] = $data[$fv];
						}
						$content_value["created_by"] = ($this->session_user_id!=NULL)?$this->session_user_id:0;
						$content_value["created_on"] = date('Y-m-d H:i:s');						
						$mmo->setValueList($content_value);	
						$mmo->insertHeader();
						$content_id = $this->db->insert_id();
						if ($this->db->trans_status() === FALSE){ $result['validation'] = false; }
						array_push($result["data_json"], $content_value);
					}
				}
				if ($this->db->trans_status() === FALSE){	
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
			$this->log_library->writeLog($result);
		}
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_add_success);
			redirect(base_url().$this->link_back);
		} else {
			foreach($_POST as $arr_name => $arr_value){
				$data[$arr_name] = $arr_value;
			}
			$data['error_message'] = $result['message'];
			$this->create($data);
		}
	}
	
	public function edit($id, $data = null) {
		$this->module_subtitle = "Edit";
		foreach($this->multi_model_object as $mmo){
			$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
			$mmo->setCond($content_cond);
			$content_detail = $mmo->getHeaderArray();	
			foreach($content_detail as $arr_name => $arr_value){
				$data[$arr_name] = $arr_value;
			}
		}
		$data = $this->edit_load_data($data);
		$this->load->view($this->view_edit, $data);
	}

	public function edit_load_data($data = null) {
		return $data;
	}
	
	public function edit_validation_data($result, $data) {
		return $result;
	}

	public function edit_process() {
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']){
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => ${$vo}, 'id != ' => $id);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$validation = false;
						//$data['error_message'] = "Column Exist : ".$vo;
						$result['message'] = "Data Exist : ".$data[$vo];
					}
				}
			}
		}	
		if($result['validation']){
			$result = $this->edit_validation_data($result, $data);
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']){
			try {
				$this->db->trans_begin();
				foreach($this->multi_model_object as $mmo){
					if($result['validation']){
						$fillable_value = $mmo->getFillableValueList();
						$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
						$mmo->setCond($content_cond);	
						$content_value = $mmo->getHeaderArray();
						foreach($fillable_value as $fv){
							$content_value[$fv] = $data[$fv];
						}
						$content_value["modified_by"] = $this->session_user_id;
						$content_value["modified_on"] = date('Y-m-d H:i:s');
						$mmo->setValueList($content_value);		
						$mmo->updateHeader();
						if ($this->db->trans_status() === FALSE){ $result['validation'] = false; }
						array_push($result["data_json"], $content_value);
					}
				}
				if ($this->db->trans_status() === FALSE){	
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
			$this->log_library->writeLog($result);
		}
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_edit_success);
			redirect(base_url().$this->link_back);
		} else {
			foreach($_POST as $arr_name => $arr_value){
				$data[$arr_name] = $arr_value;
			}
			$data['error_message'] = $result['message'];
			$this->edit($data['id'], $data);
		}
	}
	
	public function delete_process($id)
	{
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']){
			$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
			$this->multi_model_object[0]->setCond($content_cond);
			if(!$this->multi_model_object[0]->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Invalid Content Id.";
			}
		}
		if($result['validation']){
			$content_cond = array('record_status' => STATUS_DELETE, 'id' => $id);
			$this->multi_model_object[0]->setCond($content_cond);
			if($this->multi_model_object[0]->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Content has been deleted previously.";
			}
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/	
		if($result['validation']){
			try {
				$this->db->trans_begin();
				if($result['validation']){
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
					$this->multi_model_object[0]->setCond($content_cond);	
					$content_value = $this->multi_model_object[0]->getHeaderArray();
					$content_value["record_status"] = STATUS_DELETE;
					$content_value["modified_by"] = $this->session_user_id;
					$content_value["modified_on"] = date('Y-m-d H:i:s');
					$this->multi_model_object[0]->setValueList($content_value);		
					$this->multi_model_object[0]->updateHeader();
					if ($this->db->trans_status() === FALSE){ $result['validation'] = false; }
					array_push($result["data_json"], $content_value);
				}
				if ($this->db->trans_status() === FALSE){	
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		/*********Process ends here ***********/
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_delete_success);
			redirect(base_url().$this->link_back);
		} else {
			$data['error_message'] = $result['message'];
			$this->index($data);
		}
	}

	public function view_load_data($data = null) {
		return $data;
	}

	public function view($id, $data = null)
	{
		$this->module_subtitle = "View";
		foreach($this->multi_model_object as $mmo){
			$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
			$mmo->setCond($content_cond);
			$content_detail = $mmo->getHeaderArray();	
			foreach($content_detail as $arr_name => $arr_value){
				$data[$arr_name] = $arr_value;
			}
		}
		$data = $this->view_load_data($data);
		$this->load->view($this->view_view, $data);
	}
	
	public function get_data_list_json(){	
		$request_data= $_REQUEST;
		$column = $this->multi_model_object[0]->getDatatableValueList();
		$total_data = $this->multi_model_object[0]->getCountData();
		$total_filtered = $this->multi_model_object[0]->getDataTableCount($request_data);
		$data_filtered = $this->multi_model_object[0]->getDataTableData($request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}
	
	public function get_data_list_active_json(){   
		$request_data= $_REQUEST; 
		$column = $this->multi_model_object[0]->getDatatableValueList(); 
		$total_data = $this->multi_model_object[0]->getCountDataActive(); 
		$total_filtered = $this->multi_model_object[0]->getDataTableCountActive($request_data); 
		$data_filtered = $this->multi_model_object[0]->getDataTableDataActive($request_data); 


		$data = array(); 
		foreach($data_filtered as $row_id => $row_content){ 
			$nested_data=array();  
			foreach($column as $dt_id => $dt_val){ 
				$nested_data[] = $row_content[$dt_val]; 
			} 
			$data[] = $nested_data; 
		} 
		$json_data = array( 
			"draw"            => intval( $request_data['draw'] ), 
			"recordsTotal"    => intval( $total_data ),   
			"recordsFiltered" => intval( $total_filtered ),  
			"data"            => $data   
		); 
		echo json_encode($json_data); 
	} 
}

class Report_Controller extends MY_Controller {
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('session_user_id') != null){
			$this->session_user_id = $this->session->userdata('session_user_id');
			$this->session_user_name = $this->session->userdata('session_user_name');
			$this->device_id = "";
		} 
		
		$this->module_name = "content";
		$this->report_title = "Content Report";
		
		$this->module_title = "Content Title";
		
		$this->link_back = $this->module_name;
		
		$this->view_report = $this->module_name."/report";
		
		$core_app_config_cond = array('config_key' => 'WEB_NAME');
		$this->core_app_config_model->setCond($core_app_config_cond);
		$this->web_name =  $this->core_app_config_model->getHeaderField("config_value");

		$this->config->load('custom');
    }
	
	public function index($data = null)
	{
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->view_report;		
		$this->load->view('parts/template',$data);
	}
	
	public function generate_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
				
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			
		} else {
			$this->index($data);
		}
	}
}


class Header_Detail_Controller extends Crud_Controller {
	public function __construct() {
        parent::__construct();
		if($this->session->userdata('session_user_id') != null){
			$this->session_user_id = $this->session->userdata('session_user_id');
			$this->session_user_name = $this->session->userdata('session_user_name');
			$this->device_id = "";
		} 
		
		$this->module_name = "content";
		$this->table_name = "tb_content";
		$this->model_object = $this->core_content_model;
		$this->model_detail_object = $this->core_content_model;
		
		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";

		$this->msg_add_success = "Create Content Success.";
		$this->msg_edit_success = "Edit Content Success.";
		$this->msg_delete_success = "Delete Content Success.";
		
		$core_app_config_cond = array('config_key' => 'WEB_NAME');
		$this->core_app_config_model->setCond($core_app_config_cond);
		$this->web_name =  $this->core_app_config_model->getHeaderField("config_value");

		$this->config->load('custom');
    }

    public function edit($id, $data = null)
	{
		$this->module_subtitle = "Edit";
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		$content_detail = $this->model_object->getHeaderArray();	
		foreach($content_detail as $arr_name => $arr_value){
			$data[$arr_name] = $arr_value;
		}
		$content_detail_cond = array('record_status' => STATUS_ACTIVE, 'header_id' => $id);
		$this->model_detail_object->setCond($content_detail_cond);
		$content_detail_list = $this->model_detail_object->getListArray();	
		$detail_show_value = $this->model_detail_object->getDetailShowValueList();
		foreach($content_detail_list as $d){
			foreach($detail_show_value as $fv){
				$data[$fv][] = $d[$fv];
			}
		}
		$data = $this->edit_load_data($data);
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->view_edit;		
		$this->load->view('parts/template',$data);
	}

	public function view($id, $data = null)
	{
		$this->module_subtitle = "View";
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		$content_detail = $this->model_object->getHeaderArray();	
		foreach($content_detail as $arr_name => $arr_value){
			$data[$arr_name] = $arr_value;
		}
		$content_detail_cond = array('record_status' => STATUS_ACTIVE, 'header_id' => $id);
		$this->model_detail_object->setCond($content_detail_cond);
		$content_detail_list = $this->model_detail_object->getListArray();	
		$detail_show_value = $this->model_detail_object->getDetailShowValueList();
		foreach($content_detail_list as $d){
			foreach($detail_show_value as $fv){
				$data[$fv][] = $d[$fv];
			}
		}
		$data = $this->view_load_data($data);
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->view_view;		
		$this->load->view('parts/template',$data);
	}

}