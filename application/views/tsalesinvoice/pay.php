<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/select2/dist/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/jquery-minicolors/jquery.minicolors.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/quill/dist/quill.snow.css">
<style>
table{
  font-size:10px;
}
tbody > tr, tbody > td {
   white-space: nowrap; overflow: hidden; text-overflow:ellipsis;
}
#text_subtotal, #text_total, #text_sisa{
  font-size:14px; 
}
</style>
<body>
    <?PHP $this->load->view('parts/preloader'); ?>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?PHP $this->load->view('parts/header_nav'); ?>
        <?PHP $this->load->view('parts/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <?PHP $this->load->view('parts/breadcrumb'); ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <form class="form-horizontal" id="content_form" method="post" action="<?PHP echo $this->module_name;?>/pay_process">
                                <input type = "hidden" name="id" value="<?PHP echo isset($id)?$id:''; ?>">
                                <div class="card-body">
                                    <h4 class="card-title">Order Info</h4>                
                  
                  <?PHP if($this->session->flashdata('success_message')!=""): ?>
                  <div class="alert alert-success">
                    <strong>Success!</strong> <?PHP echo $this->session->flashdata('success_message'); ?>
                  </div>
                  <?PHP endif; ?>
                  <?PHP if(isset($validation) && isset($message)): ?>
                    <?PHP if(!$validation && $message != ""): ?>
                    <div class="alert alert-block alert-danger">
                      <strong>Error!</strong> <?PHP echo $message; ?>
                    </div>
                    <?PHP endif; ?>
                  <?PHP endif; ?> 
                  
                  <div class="row">
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="do" class="col-sm-3 text-right control-label col-form-label">No DO </label>
                                            <div class="col-sm-9">
                                              <input class=" form-control" id="no_do" name="no_do" type="text" value = "<?PHP echo isset($no_do)?$no_do:''; ?>" readonly disabled="true" />
                                            </div>
                                      </div>
                                      </div>
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="expor" class="col-sm-3 text-right control-label col-form-label">Invoice Expor </label>
                                            <div class="col-sm-9">
                                                <select class="select2 form-control custom-select" name = "expor" style="width: 100%; height:36px;" readonly disabled="true">
                                                    <option value="0" <?PHP echo isset($expor)?(($expor=='0')?'selected':''):''; ?>>No</option>
                                                    <option value="1" <?PHP echo isset($expor)?(($expor=='1')?'selected':''):''; ?>>Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                    
                  <div class="row">
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="no_transaksi" class="col-sm-3 text-right control-label col-form-label">No Transaksi *</label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="no_transaksi" name="no_transaksi" type="text" value = "<?PHP echo isset($no_transaksi)?$no_transaksi:''; ?>" readonly disabled="true" />
                                            </div>
                                      </div>
                                      </div>
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                        <label for="tanggal" class="col-sm-3 text-right control-label col-form-label">Tanggal *</label>
                        <div class="input-group col-sm-9">
                          <input type="text" class="form-control datepicker" placeholder="yyyy-mm-dd" id="tanggal" name="tanggal" value = "<?PHP echo isset($tanggal)?$tanggal:''; ?>" readonly disabled="true">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="customer" class="col-sm-3 text-right control-label col-form-label">Customer *</label>
                                            <div class="col-sm-9">
                          <select class="select2 form-control custom-select" name = "customer" style="width: 100%; height:36px;" readonly disabled="true">
                            <option value = '0'>Select</option>
                            <?PHP foreach($customer_list as $d): ?>
                              <option value="<?PHP echo $d->id; ?>" <?PHP echo isset($customer)?(($customer==$d->id)?'selected':''):''; ?>><?PHP echo $d->nama; ?></option>
                            <?PHP endforeach; ?>
                          </select>
                                            </div>
                                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="ship_to" class="col-sm-3 text-right control-label col-form-label">Ship To *</label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="ship_to" name="ship_to" type="text" value = "<?PHP echo isset($ship_to)?$ship_to:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="terms" class="col-sm-3 text-right control-label col-form-label">Terms </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="terms" name="terms" type="text" value = "<?PHP echo isset($terms)?$terms:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="fob" class="col-sm-3 text-right control-label col-form-label">FOB </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="fob" name="fob" type="text" value = "<?PHP echo isset($fob)?$fob:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="ship_via" class="col-sm-3 text-right control-label col-form-label">Ship Via </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="ship_via" name="ship_via" type="text" value = "<?PHP echo isset($ship_via)?$ship_via:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="ship_date" class="col-sm-3 text-right control-label col-form-label">Ship Date </label>
                                            <div class="col-sm-9">
                          <input class=" form-control datepicker" id="ship_date" name="ship_date" type="text" value = "<?PHP echo isset($ship_date)?$ship_date:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="po_no" class="col-sm-3 text-right control-label col-form-label">PO No </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="po_no" name="po_no" type="text" value = "<?PHP echo isset($po_no)?$po_no:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                    <!--<div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="currency" class="col-sm-3 text-right control-label col-form-label">Currency </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="currency" name="currency" type="text" value = "<?PHP echo isset($currency)?$currency:''; ?>" />
                                            </div>
                                        </div>
                    </div>-->
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="faktur_pajak" class="col-sm-3 text-right control-label col-form-label">Faktur Pajak </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="faktur_pajak" name="faktur_pajak" type="text" value = "<?PHP echo isset($faktur_pajak)?$faktur_pajak:''; ?>" readonly />
                          <input type = "hidden" name = "currency" id = "currency" value = "<?PHP echo isset($currency)?$currency:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                  </div>
         
                  <div class="row">
                    <div class="col-12">
                      <div class="card">
                        <div class="card-body">
                          <h5 class="card-title m-b-0">List Detail</h5>
                        </div>
                                        <div class="table-responsive">
                          <table class="table table-striped table-bordered" id = "detail-table">
                              <thead>
                              <tr>
                                <th scope="col">Item</th>
                                <th scope="col">Description</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Unit Price</th>
                                <th scope="col">Disc %</th>
                                <th scope="col">Tax %</th>
                                <th scope="col">Amount</th>
                              </tr>
                              </thead>
                              <tbody>
                              <?PHP if(isset($barang)): ?>
                                <?PHP for($x = 0 ; $x < count($barang) ; $x++): ?>

                                  <tr class = "detailRow">
                                    <th scope="row"><input type = "hidden" name = "text_kode_barang[]" value = "<?PHP echo $text_kode_barang[$x]; ?>"><span><?PHP echo $text_kode_barang[$x]; ?></span></th>
                                    <td><input type = "hidden" name = "text_nama_barang[]" value = "<?PHP echo $text_nama_barang[$x]; ?>"><span><?PHP echo $text_nama_barang[$x]; ?></span></td>
                                    <td><input type = "hidden" name = "quantity[]" value = "<?PHP echo $quantity[$x]; ?>"><span><?PHP echo $quantity[$x]; ?></span></td>
                                    <td><input type = "hidden" name = "harga_unit[]" value = "<?PHP echo $harga_unit[$x]; ?>"><span><?PHP echo number_format($harga_unit[$x],0,'.',','); ?></span></td>
                                    <td><input type = "hidden" name = "discount[]" value = "<?PHP echo $discount[$x]; ?>"><span><?PHP echo number_format($discount[$x],0,'.',','); ?></span></td>
                                    <td><input type = "hidden" name = "tax[]" value = "<?PHP echo $tax[$x]; ?>"><span><?PHP echo $tax[$x]; ?></span></td>
                                    <td><input type = "hidden" name = "jumlah[]" value = "<?PHP echo $jumlah[$x]; ?>"><span><?PHP echo number_format($jumlah[$x],0,'.',','); ?></span></td>

                                    <input type = "hidden" name = "barang[]" value = "<?PHP echo $barang[$x]; ?>">                        
                                            </tr>


                                <?PHP endfor; ?>
                              <?PHP endif; ?>
                              </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                    <div class="col-6">
                      <div class="form-group row">
                                            <label for="note" class="col-sm-3 text-right control-label col-form-label">Note </label>
                                            <div class="col-sm-12">
                                              <textarea class=" form-control" id="note" name="note" rows = 8>
                                                <?PHP echo isset($note)?$note:''; ?>
                                              </textarea>
                                            </div>
                                        </div>
                    </div>

                    <div class="col-6">
                      <table class="table table-striped table-bordered" id = "summary-table">
                          <tbody>
                            <tr>
                              <td>Subtotal</td>
                              <td style = "text-align: right;">
                                <input id="subtotal" name="subtotal" type="hidden" value = "<?PHP echo isset($subtotal)?$subtotal:'0'; ?>" />
                                <span id = "text_subtotal"><?PHP echo isset($subtotal)?number_format($subtotal,0,'.',','):'0'; ?></span>
                              </td>
                            </tr>
                            <tr>
                              <td>Discount</td>
                              <td style = "text-align: right;">
                                <input class=" form-control mask-money" style = "text-align: right;" id="discount_value" name="discount_value" type="text" value = "<?PHP echo isset($discount_value)?number_format($discount_value,0,'.',','):'0'; ?>" readonly />
                              </td>
                            </tr>
                            <tr>
                              <td>Freight Value</td>
                              <td style = "text-align: right;">
                                <input class=" form-control mask-money" style = "text-align: right;" id="freight_value" name="freight_value" type="text" value = "<?PHP echo isset($freight_value)?number_format($freight_value,0,'.',','):'0'; ?>" readonly />
                              </td>
                            </tr>
                            <tr>
                              <td>Total</td>
                              <td style = "text-align: right;">
                                <input id="total" name="total" type="hidden" value = "<?PHP echo isset($total)?$total:'0'; ?>" />
                                <span id = "text_total"><?PHP echo isset($total)?number_format($total,0,'.',','):'0'; ?></span>
                              </td>
                            </tr>
                            <tr>
                              <td>Sisa Pembayaran</td>
                              <td style = "text-align: right;">
                                <input id="sisa" name="sisa" type="hidden" value = "<?PHP echo isset($sisa)?$sisa:'0'; ?>" />
                                <span id = "text_sisa"><?PHP echo isset($sisa)?number_format($sisa,0,'.',','):'0'; ?></span>
                              </td>
                            </tr>
                          </tbody>
                      </table>
                    </div>            
                  </div>

                  

                
                      
                                    <!--<div class="form-group row">
                                        <label for="prepared_by" class="col-sm-3 text-right control-label col-form-label">Prepared By </label>
                                        <div class="col-sm-9">
                      <input class=" form-control" id="prepared_by" name="prepared_by" type="text" value = "<?PHP echo isset($prepared_by)?$prepared_by:''; ?>" readonly />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="prepared_date" class="col-sm-3 text-right control-label col-form-label">Prepared Date </label>
                                        <div class="col-sm-9">
                      <input class=" form-control" id="prepared_date" name="prepared_date" type="text" value = "<?PHP echo isset($prepared_date)?$prepared_date:''; ?>" readonly />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="approved_by" class="col-sm-3 text-right control-label col-form-label">Approved By </label>
                                        <div class="col-sm-9">
                      <input class=" form-control" id="approved_by" name="approved_by" type="text" value = "<?PHP echo isset($approved_by)?$approved_by:''; ?>" readonly />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="approved_date" class="col-sm-3 text-right control-label col-form-label">Approved Date </label>
                                        <div class="col-sm-9">
                      <input class=" form-control" id="approved_date" name="approved_date" type="text" value = "<?PHP echo isset($approved_date)?$approved_date:''; ?>" readonly />
                                        </div>
                                    </div>-->

                                    <div class="row">
                                      <div class="col-12">
                                        <div class="card">
                                          <div class="card-body">
                                            <h5 class="card-title m-b-0">Payment List</h5>
                                          </div>
                                          <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id = "detail-table">
                                                <thead>
                                                <tr>
                                                  <th scope="col">Tanggal</th>
                                                  <th scope="col">Jumlah Bayar</th>
                                                  <th scope="col">Bank</th>
                                                  <th scope="col">No Giro</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?PHP if(isset($payment_list)): ?>
                                                  <?PHP foreach($payment_list as $p): ?>
                                                    <tr class = "detailRow">
                                                      <th scope="row"><span><?PHP echo $p->tanggal_bayar; ?></span></th>
                                                      <td><span><?PHP echo number_format($p->jumlah_bayar,0,'.',','); ?></span></td>
                                                      <td><span><?PHP echo $p->nama_bank; ?></span></td>
                                                      <td><span><?PHP echo $p->note; ?></span></td>
                                                    </tr>
                                                  <?PHP endforeach; ?>
                                                <?PHP endif; ?>
                                                </tbody>
                                            </table>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="tanggal_bayar" class="col-sm-3 text-right control-label col-form-label">Tanggal Bayar *</label>
                                        <div class="col-sm-9">
                                            <input class=" form-control datepicker" id="tanggal_bayar" name="tanggal_bayar" type="text" value = "<?PHP echo isset($tanggal_bayar)?$tanggal_bayar:''; ?>" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="rekening_bank" class="col-sm-3 text-right control-label col-form-label">Jumlah Bayar *</label>
                                        <div class="col-sm-9">
                                            <input class="mask-money form-control" id="jumlah_bayar" name="jumlah_bayar" type="text" value = "<?PHP echo isset($jumlah_bayar)?$jumlah_bayar:''; ?>" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="note" class="col-sm-3 text-right control-label col-form-label">No Giro</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" id="note" name="note" type="text" value = "" />
                                        </div>
                                    </div>

                                     <div class="form-group row">
                                        <label for="rekening_bank" class="col-sm-3 text-right control-label col-form-label">Rekening Bank *</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" name = "rekening_bank" style="width: 100%; height:36px;">
                                                <option value = '0'>Select</option>
                                                <?PHP foreach($bank_list as $d): ?>
                                                <option value="<?PHP echo $d->id; ?>" <?PHP echo isset($rekening_bank)?(($rekening_bank==$d->id)?'selected':''):''; ?>>
                                                    <?PHP echo $d->bank; ?> - <?PHP echo $d->no_rekening; ?> / <?PHP echo $d->nama_rekening; ?>
                                                </option>
                                                <?PHP endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                      <input type = "submit" class="btn btn-primary" name = "action" value = "Pay">
                                      <a href="<?PHP echo $this->module_name; ?>" class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                        

                    </div>
                   
                </div>
                
    
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
    <?PHP $this->load->view('parts/inner_footer'); ?>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?PHP echo VENDORPATH; ?>/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?PHP echo JSPATH; ?>/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?PHP echo JSPATH; ?>/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?PHP echo JSPATH; ?>/custom.min.js"></script>
  
    <script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.full.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  
    <script src="<?PHP echo JSPATH; ?>/jquery.validate.min.js"></script>
    <script src="<?PHP echo JSPATH; ?>/jquery.maskMoney.js"></script>
    <script src="<?PHP echo JSPATH; ?>/script.js"></script>
    <script>
    jQuery.validator.addMethod("notEqual", function(value, element, param) {
      return this.optional(element) || value != param;
    }, "Please specify a value");

    $().ready(function() {
      $(".select2").select2();
      $(".mask-money").maskMoney({precision:0,thousands:',', allowZero: true});
      $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
      });
      $("#content_form").validate({
        rules: {
          rekening_bank: {
            notEqual: '0'
          },
          jumlah_bayar: {
            required: true
          }
        }/*,
        messages: {
          role: {
            notEqual: "Wajib diisi."
          }
        }*/
      });
    });
  </script>
</body>