<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/multicheck/multicheck.css">
<link href="<?PHP echo VENDORPATH; ?>/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
<body>
    <?PHP $this->load->view('parts/preloader'); ?>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?PHP $this->load->view('parts/header_nav'); ?>
        <?PHP $this->load->view('parts/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
			<?PHP $this->load->view('parts/breadcrumb'); ?>
			<!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
			 <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
							<div class="card-body">
								<h5 class="card-title">Active List</h5>
								<a href = "<?PHP echo $this->module_name; ?>/create" class="btn btn-primary btn-sm">Create New</a>
							</div>
                            <div class="card-body">
                                <?PHP if($this->session->flashdata('success_message')!=""): ?>
                                <div class="alert alert-success">
                                    <strong>Success!</strong> <?PHP echo $this->session->flashdata('success_message'); ?>
                                </div>
                                <?PHP endif; ?>
                                <?PHP if(isset($validation) && isset($message)): ?>
                                    <?PHP if(!$validation && $message != ""): ?>
                                    <div class="alert alert-block alert-danger">
                                        <strong>Error!</strong> <?PHP echo $message; ?>
                                    </div>
                                    <?PHP endif; ?>
                                <?PHP endif; ?>
                                <div class="table-responsive">
                                    <table id="datatable-content" class="table table-striped table-bordered" style = "width:100%">
                                        <thead>
                                            <tr>
												<th>Id</th>
												<th>Full Name</th>
                                                <th>User Name</th>
                                                <th>Role</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
				
				<!-- Modal -->
				<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true ">
					<div class="modal-dialog" role="document ">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true ">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								Body goes here...
							</div>
							<div class="modal-footer">
							<button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
							<button class="btn btn-warning" type="button" id = "btn-confirm" data-id = ""> Confirm</button>
							</div>
						</div>
					</div>
				</div>
				<!-- modal -->
				
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
		<?PHP $this->load->view('parts/inner_footer'); ?>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?PHP echo VENDORPATH; ?>/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?PHP echo JSPATH; ?>/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?PHP echo JSPATH; ?>/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?PHP echo JSPATH; ?>/custom.min.js"></script>
	

    <!-- this page js -->
    <script src="<?PHP echo VENDORPATH; ?>/multicheck/datatable-checkbox-init.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/multicheck/jquery.multicheck.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/DataTables/datatables.min.js"></script>
    <script>
		var handleDataTableButtons = function() {
		  if ($("#datatable-content").length) {
			  $("#datatable-content").DataTable({
				processing: true,
				serverSide: true,
				deferRender: true,
				keys: true,
				order: [[ 0, 'asc' ]],
				/*columnDefs: [
				  { orderable: false, targets: [0] }
				],*/
				"columnDefs": [ {
					"targets": -1,
					"data": null,
					/*"defaultContent": "<a href=''>View</a>"*/
					"render": function ( data, type, row ) {
						return "<a href='<?PHP echo $this->module_name; ?>/edit/" + row[0] + "'>Edit</a>&nbsp;&nbsp;&nbsp;<a href = '#' data-id = '" + row[0] + "' data-name = '" + row[1] + "' class = 'dt_remove'>Delete</a>";
					},
				},
				{ "orderable": false, "targets": -1 },
				{
					"targets": [ 0 ],
					"visible": false
                }
				],
				ajax : ( {
				  url: "<?PHP echo $this->module_name; ?>/get_data_list_active_json",
				  type: "POST",
				}),

				responsive: true,
			  });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
          init: function() {
            handleDataTableButtons();
          }
          };
        }();
        TableManageButtons.init();
		
		$('#datatable-content').on('click', 'a.dt_remove', function (e) {
			e.preventDefault();
			$("#confirmModal .modal-body").html("Are you sure you want to delete " + $(this).attr("data-name") + "?");
			$("#confirmModal #btn-confirm").attr('data-id', $(this).attr("data-id"));
			$('#confirmModal').modal({
				show: true
			});
		} );
		
		$('#confirmModal').on('click', '#btn-confirm', function (e) {
			var id = $(this).attr("data-id");
			window.location = "<?PHP echo base_url(); ?><?PHP echo $this->module_name; ?>/delete_process/" + id;
		});
    </script>
</body>