<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/select2/dist/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/jquery-minicolors/jquery.minicolors.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/quill/dist/quill.snow.css">
<body>
    <?PHP $this->load->view('parts/preloader'); ?>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?PHP $this->load->view('parts/header_nav'); ?>
        <?PHP $this->load->view('parts/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <?PHP $this->load->view('parts/breadcrumb'); ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <form class="form-horizontal" id="content_form" method="post" action="<?PHP echo $this->module_name;?>/create_process">
                                <div class="card-body">
                                    <h4 class="card-title">User Info</h4>								
									
									<?PHP if($this->session->flashdata('success_message')!=""): ?>
									<div class="alert alert-success">
										<strong>Success!</strong> <?PHP echo $this->session->flashdata('success_message'); ?>
									</div>
									<?PHP endif; ?>
									<?PHP if(isset($validation) && isset($message)): ?>
										<?PHP if(!$validation && $message != ""): ?>
										<div class="alert alert-block alert-danger">
											<strong>Error!</strong> <?PHP echo $message; ?>
										</div>
										<?PHP endif; ?>
									<?PHP endif; ?>	
									
									
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Full Name *</label>
                                        <div class="col-sm-9">
											<input class=" form-control" id="full_name" name="full_name" type="text" value = "<?PHP echo isset($full_name)?$full_name:''; ?>" />
                                        </div>
                                    </div>
									
									<div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">User Name *</label>
                                        <div class="col-sm-9">
											<input class=" form-control" id="user_name" name="user_name" type="text" value = "<?PHP echo isset($user_name)?$user_name:''; ?>" />
                                        </div>
                                    </div>
									
									<div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Role *</label>
                                        <div class="col-sm-9">
											<select class="select2 form-control custom-select" name = "role" style="width: 100%; height:36px;">
												<option value = '0'>Select</option>
												<?PHP foreach($role_list as $d): ?>
													<option value="<?PHP echo $d->id; ?>" <?PHP echo isset($role)?(($role==$d->id)?'selected':''):''; ?>><?PHP echo $d->nama; ?></option>
												<?PHP endforeach; ?>
											</select>
                                        </div>
                                    </div>
									
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Password *</label>
                                        <div class="col-sm-9">
											<input class=" form-control" id="password" name="password" type="password" value = "<?PHP echo isset($password)?$password:''; ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Confirm Password *</label>
                                        <div class="col-sm-9">
                                            <input class=" form-control" id="confirm_password" name="confirm_password" type="password" value = "<?PHP echo isset($confirm_password)?$confirm_password:''; ?>" />
                                        </div>
                                    </div>
									
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <input type = "submit" class="btn btn-primary" value = "Save">
										<a href="<?PHP echo $this->module_name; ?>" class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        

                    </div>
                   
                </div>
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
		<?PHP $this->load->view('parts/inner_footer'); ?>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?PHP echo VENDORPATH; ?>/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?PHP echo JSPATH; ?>/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?PHP echo JSPATH; ?>/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?PHP echo JSPATH; ?>/custom.min.js"></script>
	
	<script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.full.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.min.js"></script>
	<script src="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	
	<script src="<?PHP echo JSPATH; ?>/jquery.validate.min.js"></script>
    <script>
	jQuery.validator.addMethod("notEqual", function(value, element, param) {
	  return this.optional(element) || value != param;
	}, "Please specify a value");

	$().ready(function() {
		$(".select2").select2();
		$("#content_form").validate({
			rules: {
				full_name: {
					required: true
				},
				user_name: {
					required: true,
					minlength: 4
				},
				role: {
					notEqual: '0'
				},
				password: {
					required: true,
					minlength: 6
				},
				confirm_password: {
					required: true,
					minlength: 6,
					equalTo: "#password"
				}
			}/*,
			messages: {
				role: {
					notEqual: "Wajib diisi."
				}
			}*/
		});
	});
	</script>
</body>