<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/select2/dist/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/jquery-minicolors/jquery.minicolors.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/quill/dist/quill.snow.css">
<style>
table{
	font-size:10px;
}
tbody > tr, tbody > td {
	 white-space: nowrap; overflow: hidden; text-overflow:ellipsis;
}
</style>
<body>
    <?PHP $this->load->view('parts/preloader'); ?>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?PHP $this->load->view('parts/header_nav'); ?>
        <?PHP $this->load->view('parts/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <?PHP $this->load->view('parts/breadcrumb'); ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <form class="form-horizontal" id="content_form" method="post" action="<?PHP echo $this->module_name;?>/create_process">
                                <div class="card-body">
                                    <h4 class="card-title">Invoice Info</h4>								
									
									<?PHP if($this->session->flashdata('success_message')!=""): ?>
									<div class="alert alert-success">
										<strong>Success!</strong> <?PHP echo $this->session->flashdata('success_message'); ?>
									</div>
									<?PHP endif; ?>
									<?PHP if(isset($validation) && isset($message)): ?>
										<?PHP if(!$validation && $message != ""): ?>
										<div class="alert alert-block alert-danger">
											<strong>Error!</strong> <?PHP echo $message; ?>
										</div>
										<?PHP endif; ?>
									<?PHP endif; ?>	
									
                                    <!--<div class="form-group row">
                                        <label for="no_transaksi" class="col-sm-3 text-right control-label col-form-label">No Transaksi *</label>
                                        <div class="col-sm-9">
											<input class=" form-control" id="no_transaksi" name="no_transaksi" type="text" value = "<?PHP echo isset($no_transaksi)?$no_transaksi:''; ?>" />
                                        </div>
                                    </div>-->
									
									<div class="form-group row">
										<label for="tanggal" class="col-sm-3 text-right control-label col-form-label">Tanggal *</label>
										<div class="input-group col-sm-9">
											<input type="text" class="form-control datepicker" placeholder="yyyy-mm-dd" id="tanggal" name="tanggal" value = "<?PHP echo isset($tanggal)?$tanggal:''; ?>">
											<div class="input-group-append">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</div>
                                    
									<div class="form-group row">
                                        <label for="bank" class="col-sm-3 text-right control-label col-form-label">Bank *</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" name = "bank" style="width: 100%; height:36px;">
                                                <option value = '0'>Select</option>
                                                <?PHP foreach($bank_list as $d): ?>
                                                <option value="<?PHP echo $d->id; ?>" <?PHP echo isset($bank)?(($bank==$d->id)?'selected':''):''; ?>><?PHP echo $d->bank.' - '.$d->no_rekening; ?></option>
                                                <?PHP endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="jenis" class="col-sm-3 text-right control-label col-form-label">Kredit/Debit *</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" name = "jenis" style="width: 100%; height:36px;">
                                                <option value = 'kredit'>Kredit</option>
                                                <option value = 'debit'>Debit</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="type" class="col-sm-3 text-right control-label col-form-label">Type *</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" name = "type" style="width: 100%; height:36px;">
                                                <option value = '0'>Select</option>
                                                <?PHP foreach($type_cash_bank_list as $d): ?>
                                                <option value="<?PHP echo $d->id; ?>" <?PHP echo isset($type)?(($type==$d->id)?'selected':''):''; ?>><?PHP echo $d->nama; ?></option>
                                                <?PHP endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="jumlah" class="col-sm-3 text-right control-label col-form-label">Jumlah *</label>
                                        <div class="col-sm-9">
                                            <input class=" form-control mask-money" id="jumlah" name="jumlah" type="text" value = "<?PHP echo isset($jumlah)?$jumlah:'0'; ?>" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="note" class="col-sm-3 text-right control-label col-form-label">Note *</label>
                                        <div class="col-sm-9">
                                            <input class=" form-control" id="note" name="note" type="text" value = "<?PHP echo isset($note)?$note:''; ?>" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="cek_giro" class="col-sm-3 text-right control-label col-form-label">Cek / Giro *</label>
                                        <div class="col-sm-9">
                                            <input class=" form-control" id="cek_giro" name="cek_giro" type="text" value = "<?PHP echo isset($cek_giro)?$cek_giro:''; ?>" />
                                        </div>
                                    </div>

                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <!--<input type = "submit" class="btn btn-primary" name = "action" value = "Save">-->
                                        <input type = "submit" class="btn btn-success" name = "action" value = "Finish">
										<a href="<?PHP echo $this->module_name; ?>" class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>   
                </div>
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
		<?PHP $this->load->view('parts/inner_footer'); ?>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?PHP echo VENDORPATH; ?>/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?PHP echo JSPATH; ?>/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?PHP echo JSPATH; ?>/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?PHP echo JSPATH; ?>/custom.min.js"></script>
	
	<script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.full.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.min.js"></script>
	<script src="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	
	<script src="<?PHP echo JSPATH; ?>/jquery.validate.min.js"></script>
    <script src="<?PHP echo JSPATH; ?>/jquery.maskMoney.js"></script>
    <script>
    var currentRow = null;

	jQuery.validator.addMethod("notEqual", function(value, element, param) {
	  return this.optional(element) || value != param;
	}, "Please specify a value");

	$().ready(function() {
		$(".select2").select2();
        $(".mask-money").maskMoney({precision:0,thousands:',', allowZero: true});
		$('.datepicker').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
            todayHighlight: true
		});
		$("#content_form").validate({
			rules: {
				bank: {
					notEqual: '0'
				},
                jenis: {
                    notEqual: '0'
                },
                type: {
                    notEqual: '0'
                },
				jumlah: {
					required: true
				},
				cek_giro: {
					required: true
				}
			}/*,
			messages: {
				role: {
					notEqual: "Wajib diisi."
				}
			}*/
		});
       	
	});
	</script>
</body>