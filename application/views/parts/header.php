<!DOCTYPE html>
<html dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="<?PHP echo $title; ?>">
		<meta name="author" content="Teddy">
		<meta name="keyword" content="<?PHP echo $title; ?>">
		<title><?PHP echo $title; ?></title>		
		<base href="<?PHP echo base_url(); ?>" />
		<link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo IMAGEPATH; ?>/favicon.png">
		<link href="<?PHP echo CSSPATH; ?>/style.min.css" rel="stylesheet">
		<link href="<?PHP echo CSSPATH; ?>/custom.css" rel="stylesheet">
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style>
			.page-wrapper { min-height:730px; }
		</style>
	</head>
	