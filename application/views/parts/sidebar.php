		<!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">						
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="home/dashboard" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
						
                        <?PHP $master_module1_array = array("ROLE", "USER", "SATUAN", "CUSTOMER", "BARANG"); ?>
                        <?PHP if(count(array_intersect($master_module1_array, $this->session->userdata('session_user_module'))) != 0): ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-database"></i><span class="hide-menu">Master Data</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <?PHP if(in_array("ROLE", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="mrole" class="sidebar-link"><i class="mdi mdi-ornament"></i><span class="hide-menu"> Role </span></a></li>
                                <?PHP endif; ?>
                                <?PHP if(in_array("USER", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="muser" class="sidebar-link"><i class="mdi mdi-emoticon-cool"></i><span class="hide-menu"> User </span></a></li>
                                <?PHP endif; ?>
                                <?PHP if(in_array("SATUAN", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="msatuan" class="sidebar-link"><i class="mdi mdi-anchor"></i><span class="hide-menu"> Satuan </span></a></li>
                                <?PHP endif; ?>
                                <?PHP if(in_array("BARANG", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="mbarang" class="sidebar-link"><i class="mdi mdi-anchor"></i><span class="hide-menu"> Barang </span></a></li>
                                <?PHP endif; ?>
                                <?PHP if(in_array("CUSTOMER", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="mcustomer" class="sidebar-link"><i class="mdi mdi-emoticon-happy"></i><span class="hide-menu"> Customer </span></a></li>
								<?PHP endif; ?>
                                <?PHP if(in_array("SUPPLIER", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="msupplier" class="sidebar-link"><i class="mdi mdi-anchor"></i><span class="hide-menu"> Supplier </span></a></li>
                                <?PHP endif; ?>
                                <?PHP if(in_array("BANK", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="mrbank" class="sidebar-link"><i class="mdi mdi-anchor"></i><span class="hide-menu"> Bank </span></a></li>
                                <?PHP endif; ?>
                                <?PHP if(in_array("TKASBON", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="mtkasbon" class="sidebar-link"><i class="mdi mdi-anchor"></i><span class="hide-menu"> Type Kas Bon </span></a></li>
                                <?PHP endif; ?>
                            </ul>
                        </li>
                        <?PHP endif; ?>

                        <?PHP $master_module2_array = array("SETTING"); ?>
                        <?PHP if(count(array_intersect($master_module2_array, $this->session->userdata('session_user_module'))) != 0): ?>
						<li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-settings-box"></i><span class="hide-menu">Master Setting</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <?PHP if(in_array("SETTING", $this->session->userdata('session_user_module'))): ?>
								<li class="sidebar-item"><a href="msetting" class="sidebar-link"><i class="mdi mdi-settings"></i><span class="hide-menu"> Setting </span></a></li>
                                <?PHP endif; ?>
                            </ul>
                        </li>
                        <?PHP endif; ?>

                        <?PHP if(in_array("PURCHASEORDER", $this->session->userdata('session_user_module'))): ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="tpurchaseorder" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Pembelian</span></a></li>
                        <?PHP endif; ?>

                        <?PHP if(in_array("SALESINVOICE", $this->session->userdata('session_user_module'))): ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="tsalesinvoice" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Penjualan</span></a></li>
                        <?PHP endif; ?>

                        <?PHP if(in_array("DELIVERYORDER", $this->session->userdata('session_user_module'))): ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="tdeliveryorder" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Delivery Order</span></a></li>
                        <?PHP endif; ?>

                        <?PHP if(in_array("KASBON", $this->session->userdata('session_user_module'))): ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="tkasbon" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Cash Bank</span></a></li>
                        <?PHP endif; ?>

                        <?PHP if(in_array("KASBONDTL", $this->session->userdata('session_user_module'))): ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="tkasbondtl/bank" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Cash Bank Detail</span></a></li>
                        <?PHP endif; ?>


                         <?PHP $report_array = array("RPEMBELIAN", "RHUTANG", "RPENJUALAN", "RPIUTANG", "RBUKUBESAR", "RBUKUBANK", "RNERACA", "RLABARUGI", "RPERSEDIAAN"); ?>
                        <?PHP if(count(array_intersect($report_array, $this->session->userdata('session_user_module'))) != 0): ?>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-buffer"></i><span class="hide-menu">Report</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">


                                <?PHP if(in_array("RPEMBELIAN", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="rpembelian" class="sidebar-link"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Pembelian</span></a></li>
                                <?PHP endif; ?>
                                <?PHP if(in_array("RHUTANG", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="rhutang" class="sidebar-link"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Hutang</span></a></li>
                                <?PHP endif; ?>


                                <?PHP if(in_array("RPENJUALAN", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="rpenjualan" class="sidebar-link"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Penjualan</span></a></li>
                                <?PHP endif; ?>
                                <?PHP if(in_array("RPIUTANG", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="rpiutang" class="sidebar-link"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Piutang</span></a></li>
                                <?PHP endif; ?>

                                
                                <?PHP if(in_array("RBUKUBESAR", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="rbukubesar" class="sidebar-link"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Buku Besar</span></a></li>
                                <?PHP endif; ?>

                                <?PHP if(in_array("RBUKUBANK", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="rbukubank" class="sidebar-link"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Buku Bank</span></a></li>
                                <?PHP endif; ?>


                                <?PHP if(in_array("RNERACA", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="rneraca" class="sidebar-link"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Neraca</span></a></li>
                                <?PHP endif; ?>

                                <?PHP if(in_array("RLABARUGI", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="rlabarugi" class="sidebar-link"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Laba Rugi</span></a></li>
                                <?PHP endif; ?>

                                <?PHP if(in_array("RPERSEDIAAN", $this->session->userdata('session_user_module'))): ?>
                                <li class="sidebar-item"><a href="rpersediaan" class="sidebar-link"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Persediaan</span></a></li>
                                <?PHP endif; ?>
                                

                            </ul>
                        </li>
                        <?PHP endif; ?>


                        <?PHP if(in_array("LMIELIDI", $this->session->userdata('session_user_module'))): ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?PHP echo $this->config->item('url_persediaan_mie'); ?>" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Mie Lidi</span></a></li>
                        <?PHP endif; ?>

                        <?PHP if(in_array("LTEPUNG", $this->session->userdata('session_user_module'))): ?>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?PHP echo $this->config->item('url_persediaan_tepung'); ?>" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Tepung</span></a></li>
                        <?PHP endif; ?>



                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->