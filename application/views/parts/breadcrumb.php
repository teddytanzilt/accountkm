			<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title"><?PHP echo $this->module_title; ?></h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home/dashboard">Home</a></li>
									<?PHP if($this->module_subtitle != ""): ?>
									<li class="breadcrumb-item"><a href= "<?PHP echo $this->module_name; ?>"><?PHP echo $this->module_title; ?></a></li>
									<li class="breadcrumb-item active" aria-current="page"><?PHP echo $this->module_subtitle; ?></li>
									<?PHP else: ?>
                                    <li class="breadcrumb-item active" aria-current="page"><?PHP echo $this->module_title; ?></li>
									<?PHP endif; ?>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->