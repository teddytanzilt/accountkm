<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/select2/dist/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/jquery-minicolors/jquery.minicolors.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/quill/dist/quill.snow.css">
<style>
table{
	font-size:10px;
}
tbody > tr, tbody > td {
	 white-space: nowrap; overflow: hidden; text-overflow:ellipsis;
}
</style>
<body>
    <?PHP $this->load->view('parts/preloader'); ?>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?PHP $this->load->view('parts/header_nav'); ?>
        <?PHP $this->load->view('parts/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <?PHP $this->load->view('parts/breadcrumb'); ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            	<input type = "hidden" name="id" value="<?PHP echo isset($id)?$id:''; ?>">
                                <div class="card-body">
                                    <h4 class="card-title">Invoice Info</h4>								
									
									<?PHP if($this->session->flashdata('success_message')!=""): ?>
									<div class="alert alert-success">
										<strong>Success!</strong> <?PHP echo $this->session->flashdata('success_message'); ?>
									</div>
									<?PHP endif; ?>
									<?PHP if(isset($validation) && isset($message)): ?>
										<?PHP if(!$validation && $message != ""): ?>
										<div class="alert alert-block alert-danger">
											<strong>Error!</strong> <?PHP echo $message; ?>
										</div>
										<?PHP endif; ?>
									<?PHP endif; ?>	
									
                                    <!--<div class="form-group row">
                                        <label for="no_transaksi" class="col-sm-3 text-right control-label col-form-label">No Transaksi *</label>
                                        <div class="col-sm-9">
											<input class=" form-control" id="no_transaksi" name="no_transaksi" type="text" value = "<?PHP echo isset($no_transaksi)?$no_transaksi:''; ?>" />
                                        </div>
                                    </div>
									
									<div class="form-group row">
										<label for="tanggal" class="col-sm-3 text-right control-label col-form-label">Tanggal *</label>
										<div class="input-group col-sm-9">
											<input type="text" class="form-control datepicker" placeholder="yyyy-mm-dd" id="tanggal" name="tanggal" value = "<?PHP echo isset($tanggal)?$tanggal:''; ?>">
											<div class="input-group-append">
												<span class="input-group-text"><i class="fa fa-calendar"></i></span>
											</div>
										</div>
									</div>-->

									<div class="form-group row">
                                        <label for="bank" class="col-sm-3 text-right control-label col-form-label">Bank *</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" name = "bank" style="width: 100%; height:36px;" readonly disabled = true>
                                                <option value = '0'>Select</option>
                                                <?PHP foreach($bank_list as $d): ?>
                                                <option value="<?PHP echo $d->id; ?>" <?PHP echo isset($bank)?(($bank==$d->id)?'selected':''):''; ?>><?PHP echo $d->bank.' - '.$d->no_rekening; ?></option>
                                                <?PHP endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="jenis" class="col-sm-3 text-right control-label col-form-label">Kredit/Debit *</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" name = "jenis" style="width: 100%; height:36px;" readonly disabled = true>
                                                <option value = 'kredit' <?PHP echo ($debit==0)?'selected':''; ?>>Kredit</option>
                                                <option value = 'debit' <?PHP echo ($kredit==0)?'selected':''; ?>>Debit</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="type" class="col-sm-3 text-right control-label col-form-label">Type *</label>
                                        <div class="col-sm-9">
                                            <select class="select2 form-control custom-select" name = "type" style="width: 100%; height:36px;" readonly disabled = true >
                                                <option value = '0'>Select</option>
                                                <?PHP foreach($type_cash_bank_list as $d): ?>
                                                <option value="<?PHP echo $d->id; ?>" <?PHP echo isset($type)?(($type==$d->id)?'selected':''):''; ?>><?PHP echo $d->nama; ?></option>
                                                <?PHP endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="jumlah" class="col-sm-3 text-right control-label col-form-label">Jumlah *</label>
                                        <div class="col-sm-9">
                                            <input class=" form-control mask-money" id="jumlah" name="jumlah" type="text" value = "<?PHP echo ($debit==0)?$kredit:$debit; ?>" readonly disabled = true />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="note" class="col-sm-3 text-right control-label col-form-label">Note *</label>
                                        <div class="col-sm-9">
                                            <input class=" form-control" id="note" name="note" type="text" value = "<?PHP echo isset($note)?$note:''; ?>" readonly disabled = true />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="cek_giro" class="col-sm-3 text-right control-label col-form-label">Cek / Giro *</label>
                                        <div class="col-sm-9">
                                            <input class=" form-control" id="cek_giro" name="cek_giro" type="text" value = "<?PHP echo isset($cek_giro)?$cek_giro:''; ?>" readonly disabled = true />
                                        </div>
                                    </div>

                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <a href="<?PHP echo $this->module_name; ?>" class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                        </div>
                        

                    </div>
                   
                </div>
                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
		<?PHP $this->load->view('parts/inner_footer'); ?>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?PHP echo VENDORPATH; ?>/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?PHP echo JSPATH; ?>/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?PHP echo JSPATH; ?>/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?PHP echo JSPATH; ?>/custom.min.js"></script>
	
	<script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.full.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.min.js"></script>
	<script src="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	
	<script src="<?PHP echo JSPATH; ?>/jquery.validate.min.js"></script>
    <script>
    var currentRow = null;

	jQuery.validator.addMethod("notEqual", function(value, element, param) {
	  return this.optional(element) || value != param;
	}, "Please specify a value");

	function getOrderListInfo(order,customer){
        $.ajax({
              method: "POST",
              url: "tinvoice/ajax_order_list_info",
              data: { 
                order: order,
                customer: customer
              },
              dataType: 'json',
              success: function(json) {
                var order_detail_list = json['order_detail'];
                $.each(order_detail_list, function( index, value ) {
                	var order_detail = '';
                	var no_container = '';
                	var jumlah = '0';
                	$.each(value, function( index2, value2 ) {
                		if(index2 == 'id'){
                			order_detail = value2;
                		}
                		if(index2 == 'no_container'){
                			no_container = value2;
                		}
                		if(index2 == 'harga'){
                			jumlah = value2;
                		}
                	});

					var append = "<tr class = 'detailRow'>" +
						  "<th scope='row'><input type = 'hidden' name = 'order_detail[]' value = '" + order_detail + "'><span>" + no_container + "</span></th>" +
						  "<td><input type = 'hidden' name = 'jumlah[]' value = '" + jumlah + "'><span>" + jumlah + "</span></td>" +
						  "<td><a href = '#' class = 'dt_remove'>Delete</a></td>" +
	                      "<input type = 'hidden' name = 'no_container[]' value = '" + no_container + "'>" + 
						"</tr>";
					
					append = append + "<script>" + 
						"$( '.detailRow > th' ).click(function() { " +
							"populateDetailModal(this); $('#detailModal').modal('toggle'); " +
						"});" + 
						"$( '.detailRow > td:not(:last)' ).click(function() { " +
							"populateDetailModal(this); $('#detailModal').modal('toggle'); " + 
						"}); " + 
						"$( '.dt_remove' ).click(function(e) { " +
	                      "e.preventDefault(); " +
	                      "currentRow = this; " +
	                        "$('#confirmModal .modal-body').html('Are you sure you want to delete this row ?'); " +
	                        "$('#confirmModal').modal({ " +
	                          "show: true " +
	                        "}); " +
	                    "}); " +
						"<\/script>";
					$( "#detail-table > tbody" ).append(append);	
				});
                calculateJumlahTotal();
            }
        });    
    }

	function getOrderDetailInfo(order_detail){
        $.ajax({
              method: "POST",
              url: "tinvoice/ajax_order_detail_info",
              data: { 
                order_detail: order_detail
              },
              dataType: 'json',
              success: function(json) {
                var kode = json['kode'];
                var nama = json['nama'];
                var jumlah = json['jumlah'];
                //$("input[name='kode']").val(kode);
                //$("input[name='nama']").val(nama);
                $("input[name='detail_jumlah']").val(jumlah);
            }
        });    
    }
	function calculateJumlahTotal() {
		var jumlah_total = 0;
		$("#detail-table > tbody tr").each(function () {
	    	var jumlah = $(this).find("input[name='jumlah[]']").val();
	    	jumlah_total = parseInt(jumlah_total) + parseInt(jumlah);		    	
	  	});
	  	$("input[name='jumlah_total']").val(jumlah_total);
    }
	

	$().ready(function() {
		calculateJumlahTotal();
		$("#content_form").validate({
			rules: {
				type_tanda_terima: {
					notEqual: '0'
				},
				tanggal_terima: {
					required: true
				},
				batas_tanggal_bayar: {
					required: true
				},
				rekening_vendor: {
					required: true
				}
			}/*,
			messages: {
				role: {
					notEqual: "Wajib diisi."
				}
			}*/
		});
	});
	</script>
</body>