<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/select2/dist/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/jquery-minicolors/jquery.minicolors.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/quill/dist/quill.snow.css">
<style>
table{
  font-size:10px;
}
tbody > tr, tbody > td {
   white-space: nowrap; overflow: hidden; text-overflow:ellipsis;
}
#text_subtotal, #text_total{
  font-size:14px; 
}
</style>
<body>
    <?PHP $this->load->view('parts/preloader'); ?>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?PHP $this->load->view('parts/header_nav'); ?>
        <?PHP $this->load->view('parts/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <?PHP $this->load->view('parts/breadcrumb'); ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <form class="form-horizontal" id="content_form" method="post" action="<?PHP echo $this->module_name;?>/edit_process">
                                <input type = "hidden" name="id" value="<?PHP echo isset($id)?$id:''; ?>">
                                <div class="card-body">
                                    <h4 class="card-title">Order Info</h4>                
                  
                  <?PHP if($this->session->flashdata('success_message')!=""): ?>
                  <div class="alert alert-success">
                    <strong>Success!</strong> <?PHP echo $this->session->flashdata('success_message'); ?>
                  </div>
                  <?PHP endif; ?>
                  <?PHP if(isset($validation) && isset($message)): ?>
                    <?PHP if(!$validation && $message != ""): ?>
                    <div class="alert alert-block alert-danger">
                      <strong>Error!</strong> <?PHP echo $message; ?>
                    </div>
                    <?PHP endif; ?>
                  <?PHP endif; ?> 
                  
                  <div class="row">
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="no_transaksi" class="col-sm-3 text-right control-label col-form-label">No Transaksi *</label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="no_transaksi" name="no_transaksi" type="text" value = "<?PHP echo isset($no_transaksi)?$no_transaksi:''; ?>" readonly disabled="true" />
                                            </div>
                                      </div>
                                      </div>
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                        <label for="tanggal" class="col-sm-3 text-right control-label col-form-label">Tanggal *</label>
                        <div class="input-group col-sm-9">
                          <input type="text" class="form-control datepicker" placeholder="yyyy-mm-dd" id="tanggal" name="tanggal" value = "<?PHP echo isset($tanggal)?$tanggal:''; ?>">
                          <div class="input-group-append">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="supplier" class="col-sm-3 text-right control-label col-form-label">Supplier *</label>
                                            <div class="col-sm-9">
                          <select class="select2 form-control custom-select" name = "supplier" style="width: 100%; height:36px;">
                            <option value = '0'>Select</option>
                            <?PHP foreach($supplier_list as $d): ?>
                              <option value="<?PHP echo $d->id; ?>" <?PHP echo isset($supplier)?(($supplier==$d->id)?'selected':''):''; ?>><?PHP echo $d->nama; ?></option>
                            <?PHP endforeach; ?>
                          </select>
                                            </div>
                                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="ship_to" class="col-sm-3 text-right control-label col-form-label">Ship To *</label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="ship_to" name="ship_to" type="text" value = "<?PHP echo isset($ship_to)?$ship_to:''; ?>" />
                                            </div>
                                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="terms" class="col-sm-3 text-right control-label col-form-label">Terms </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="terms" name="terms" type="text" value = "<?PHP echo isset($terms)?$terms:''; ?>" />
                                            </div>
                                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="fob" class="col-sm-3 text-right control-label col-form-label">FOB </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="fob" name="fob" type="text" value = "<?PHP echo isset($fob)?$fob:''; ?>" />
                                            </div>
                                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="ship_via" class="col-sm-3 text-right control-label col-form-label">Ship Via </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="ship_via" name="ship_via" type="text" value = "<?PHP echo isset($ship_via)?$ship_via:''; ?>" />
                                            </div>
                                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="expected_date" class="col-sm-3 text-right control-label col-form-label">Expected Date </label>
                                            <div class="col-sm-9">
                          <input class=" form-control datepicker" id="expected_date" name="expected_date" type="text" value = "<?PHP echo isset($expected_date)?$expected_date:''; ?>" />
                                            </div>
                                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="taxable_vendor" class="col-sm-3 text-right control-label col-form-label">Taxable Vendor </label>
                                            <div class="col-sm-9">
                          <select class="select2 form-control custom-select" name = "taxable_vendor" style="width: 100%; height:36px;">
                            <option value = '0' <?PHP echo isset($taxable_vendor)?(($taxable_vendor=='0')?'selected':''):''; ?>>No</option>
                            <option value = '1' <?PHP echo isset($taxable_vendor)?(($taxable_vendor=='1')?'selected':''):''; ?>>Yes</option>
                          </select>
                                            </div>
                                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="rate" class="col-sm-3 text-right control-label col-form-label">Rate </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="rate" name="rate" type="text" value = "<?PHP echo isset($rate)?$rate:''; ?>" />
                                            </div>
                                        </div>
                    </div>
                  </div>

                  <div class = "row">
                    <div class="col-sm-6">
                                          <div class="form-group row">
                                                                <label for="faktur_pajak" class="col-sm-3 text-right control-label col-form-label">Faktur Pajak </label>
                                                                <div class="col-sm-9">
                                              <input class=" form-control" id="faktur_pajak" name="faktur_pajak" type="text" value = "<?PHP echo isset($faktur_pajak)?$faktur_pajak:''; ?>" />
                                                                </div>
                                                            </div>
                                        </div>
                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="expor" class="col-sm-3 text-right control-label col-form-label">PO Expor *</label>
                                            <div class="col-sm-9">
                                                <select class="select2 form-control custom-select" name = "expor" style="width: 100%; height:36px;">
                                                    <option value="0" <?PHP echo isset($expor)?(($expor=='0')?'selected':''):''; ?>>No</option>
                                                    <option value="1" <?PHP echo isset($expor)?(($expor=='1')?'selected':''):''; ?>>Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                      </div>
                  </div>

                  <div class="row">
                    <div class="col-12">
                      <button type="button" id = "btn-modal-detail" class="btn btn-info" data-toggle="modal" data-target="#detailModal">Add</button>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-12">
                      <div class="card">
                        <div class="card-body">
                          <h5 class="card-title m-b-0">List Detail</h5>
                        </div>
                                        <div class="table-responsive">
                          <table class="table table-striped table-bordered" id = "detail-table">
                              <thead>
                              <tr>
                                <th scope="col">Item</th>
                                <th scope="col">Description</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Unit Price</th>
                                <th scope="col">Discount</th>
                                <th scope="col">Tax %</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Delete</th>
                              </tr>
                              </thead>
                              <tbody>
                              <?PHP if(isset($barang)): ?>
                                <?PHP for($x = 0 ; $x < count($barang) ; $x++): ?>

                                  <tr class = "detailRow">
                                    <th scope="row"><input type = "hidden" name = "text_kode_barang[]" value = "<?PHP echo $text_kode_barang[$x]; ?>"><span><?PHP echo $text_kode_barang[$x]; ?></span></th>
                                    <td><input type = "hidden" name = "text_nama_barang[]" value = "<?PHP echo $text_nama_barang[$x]; ?>"><span><?PHP echo $text_nama_barang[$x]; ?></span></td>
                                    <td><input type = "hidden" name = "quantity[]" value = "<?PHP echo $quantity[$x]; ?>"><span><?PHP echo $quantity[$x]; ?></span></td>
                                    <td><input type = "hidden" name = "harga_unit[]" value = "<?PHP echo $harga_unit[$x]; ?>"><span><?PHP echo number_format($harga_unit[$x],0,'.',','); ?></span></td>
                                    <td><input type = "hidden" name = "discount[]" value = "<?PHP echo $discount[$x]; ?>"><span><?PHP echo number_format($discount[$x],0,'.',','); ?></span></td>
                                    <td><input type = "hidden" name = "tax[]" value = "<?PHP echo $tax[$x]; ?>"><span><?PHP echo $tax[$x]; ?></span></td>
                                    <td><input type = "hidden" name = "jumlah[]" value = "<?PHP echo $jumlah[$x]; ?>"><span><?PHP echo number_format($jumlah[$x],0,'.',','); ?></span></td>
                                    <td><a href = '#' class = 'dt_remove'>Delete</a></td>     

                                    <input type = "hidden" name = "barang[]" value = "<?PHP echo $barang[$x]; ?>">                        
                                            </tr>

                                <?PHP endfor; ?>
                              <?PHP endif; ?>
                              </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                    <div class="col-6">
                      <div class="form-group row">
                                            <label for="note" class="col-sm-3 text-right control-label col-form-label">Note </label>
                                            <div class="col-sm-12">
                                              <textarea class=" form-control" id="note" name="note" rows = 8>
                                                <?PHP echo isset($note)?$note:''; ?>
                                              </textarea>
                                            </div>
                                        </div>
                    </div>

                    <div class="col-6">
                      <table class="table table-striped table-bordered" id = "summary-table">
                          <tbody>
                            <tr>
                              <td>Subtotal</td>
                              <td style = "text-align: right;">
                                <input id="subtotal" name="subtotal" type="hidden" value = "<?PHP echo isset($subtotal)?$subtotal:'0'; ?>" />
                                <span id = "text_subtotal"><?PHP echo isset($subtotal)?number_format($subtotal,0,'.',','):'0'; ?></span>
                              </td>
                            </tr>
                            <tr>
                              <td>Discount</td>
                              <td style = "text-align: right;">
                                <input class=" form-control mask-money" style = "text-align: right;" id="discount_value" name="discount_value" type="text" value = "<?PHP echo isset($discount_value)?number_format($discount_value,0,'.',','):'0'; ?>" />
                              </td>
                            </tr>
                            <tr>
                              <td>Freight Value</td>
                              <td style = "text-align: right;">
                                <input class=" form-control mask-money" style = "text-align: right;" id="freight_value" name="freight_value" type="text" value = "<?PHP echo isset($freight_value)?number_format($freight_value,0,'.',','):'0'; ?>" />
                              </td>
                            </tr>
                            <tr>
                              <td>Total</td>
                              <td style = "text-align: right;">
                                <input id="total" name="total" type="hidden" value = "<?PHP echo isset($total)?$total:'0'; ?>" />
                                <span id = "text_total"><?PHP echo isset($total)?number_format($total,0,'.',','):'0'; ?></span>
                              </td>
                            </tr>
                          </tbody>
                      </table>
                    </div>            
                  </div>

                                    <!--<div class="row">
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                          <label for="prepared_by" class="col-sm-3 text-right control-label col-form-label">Prepared By </label>
                                          <div class="col-sm-9">
                                            <input class=" form-control" id="prepared_by" name="prepared_by" type="text" value = "<?PHP echo isset($prepared_by)?$prepared_by:''; ?>" />
                                          </div>
                                        </div>  
                                      </div>
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                          <label for="prepared_date" class="col-sm-3 text-right control-label col-form-label">Prepared Date </label>
                                          <div class="col-sm-9">
                                            <input class=" form-control datepicker" id="prepared_date" name="prepared_date" type="text" value = "<?PHP echo isset($prepared_date)?$prepared_date:''; ?>" />
                                            </div>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="row">
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                          <label for="approved_by" class="col-sm-3 text-right control-label col-form-label">Approved By </label>
                                            <div class="col-sm-9">
                                              <input class=" form-control" id="approved_by" name="approved_by" type="text" value = "<?PHP echo isset($approved_by)?$approved_by:''; ?>" />
                                            </div>
                                        </div>
                                      </div>
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                          <label for="approved_date" class="col-sm-3 text-right control-label col-form-label">Approved Date </label>
                                          <div class="col-sm-9">
                                            <input class=" form-control datepicker" id="approved_date" name="approved_date" type="text" value = "<?PHP echo isset($approved_date)?$approved_date:''; ?>" />
                                          </div>
                                        </div>
                                      </div>
                                    </div>-->

                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <input type = "submit" class="btn btn-primary" name = "action" value = "Save">
                                        <input type = "submit" class="btn btn-success" name = "action" value = "Finish">
                    <a href="<?PHP echo $this->module_name; ?>" class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        

                    </div>
                   
                </div>
                
                <!-- Modal -->
                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true ">
          <div class="modal-dialog" role="document ">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="confirmModal">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true ">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Body goes here...
              </div>
              <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-warning" type="button" id = "btn-confirm" data-id = ""> Confirm</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="detailModal" role="dialog" aria-labelledby="detailModal" aria-hidden="true ">
          <div class="modal-dialog modal-lg" role="document ">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="detailModal">Detail Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true ">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group row">
                                    <label for="detail_barang" class="col-sm-3 text-right control-label col-form-label">Barang</label>
                                    <div class="col-sm-9">
                                      <select class="select2 form-control custom-select" id = "detail_barang" name = "detail_barang" style="width: 100%; height:36px;">
                      <option value = '0'>Select</option>
                      <?PHP foreach($barang_list as $d): ?>
                        <option value="<?PHP echo $d->id; ?>"><?PHP echo $d->kode; ?> - <?PHP echo $d->nama; ?></option>
                      <?PHP endforeach; ?>
                    </select>
                    <input type = "hidden" name = "detail_kode_barang">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="detail_quantity" class="col-sm-3 text-right control-label col-form-label">Harga</label>
                                    <div class="col-sm-9">
                    <input class="mask-money form-control" name="detail_harga_unit" type="text" />
                                    </div>
                                </div>
                
                                <div class="form-group row">
                                    <label for="detail_quantity" class="col-sm-3 text-right control-label col-form-label">Quantity</label>
                                    <div class="col-sm-9">
                    <input class=" form-control" name="detail_quantity" type="text" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="detail_discount" class="col-sm-3 text-right control-label col-form-label">Discount</label>
                                    <div class="col-sm-9">
                    <input class="mask-money form-control" name="detail_discount" type="text" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="detail_tax" class="col-sm-3 text-right control-label col-form-label">Tax</label>
                                    <div class="col-sm-9">
                    <input class=" form-control" name="detail_tax" type="text" />
                                    </div>
                                </div>


              </div>
              <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
              <button class="btn btn-success" type="button" id = "btn-add-detail" data-id = ""> Add</button>
              </div>
            </div>
          </div>
        </div>
        <!-- modal -->

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
    <?PHP $this->load->view('parts/inner_footer'); ?>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?PHP echo VENDORPATH; ?>/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?PHP echo JSPATH; ?>/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?PHP echo JSPATH; ?>/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?PHP echo JSPATH; ?>/custom.min.js"></script>
  
  <script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.full.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.min.js"></script>
  <script src="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  
  <script src="<?PHP echo JSPATH; ?>/jquery.validate.min.js"></script>
  <script src="<?PHP echo JSPATH; ?>/jquery.maskMoney.js"></script>
  <script src="<?PHP echo JSPATH; ?>/script.js"></script>
    <script>
    var currentRow = null;

  jQuery.validator.addMethod("notEqual", function(value, element, param) {
    return this.optional(element) || value != param;
  }, "Please specify a value");

  function resetDetailModal(){
    $("select[name='detail_barang']").select2('val', "0");
    $("input[name='detail_kode_barang']").val("");
    $("input[name='detail_harga_unit']").val("0");   
    $("input[name='detail_quantity']").val("0");  
    $("input[name='detail_discount']").val("0");  
    $("input[name='detail_tax']").val("0"); 
    currentRow = null;
  }
  function populateDetailModal(obj){
    /*var $option = $('<option value = "' + invoice + '" selected>' + no_invoice + '</option>').val(invoice);
        $("#detail_invoice").append($option).trigger('change'); */
    var barang = $(obj).parent().find("input[name='barang[]']").val();
    var kode_barang = $(obj).parent().find("input[name='text_kode_barang[]']").val();
    var nama_barang = $(obj).parent().find("input[name='text_nama_barang[]']").val();
    var quantity = $(obj).parent().find("input[name='quantity[]']").val();
    var harga_unit = $(obj).parent().find("input[name='harga_unit[]']").val();
    var discount = $(obj).parent().find("input[name='discount[]']").val();
    var tax = $(obj).parent().find("input[name='tax[]']").val();

    $("select[name='detail_barang']").select2('val', barang);
    $("input[name='detail_kode_barang']").val(kode_barang);
    $("input[name='detail_nama_barang']").val(nama_barang);
    $("input[name='detail_harga_unit']").val(formatCurrency(harga_unit));
    $("input[name='detail_quantity']").val(quantity);
    $("input[name='detail_discount']").val(formatCurrency(discount));
    $("input[name='detail_tax']").val(tax);

    currentRow = obj;
  }
  function validateDetailModal(){
    if($("select[name='detail_barang']").val() == "0") {
      alert("Barang tidak boleh kosong");
      return false;
    }
    if($("input[name='detail_quantity']").val() == "") {
      alert("Quantity tidak boleh kosong");
      return false;
    }
    if($("input[name='detail_discount']").val() == "") {
      alert("Discount tidak boleh kosong");
      return false;
    }
    if($("input[name='detail_tax']").val() == "") {
      alert("Tax tidak boleh kosong");
      return false;
    }
    return true;
  }

  function getBarangInfo(id){
        $.ajax({
              method: "POST",
              url: "mbarang/ajax_barang_info",
              data: { 
                id: id
              },
              dataType: 'json',
              success: function(json) {
                var kode = json['kode'];
                var nama = json['nama'];
                var harga = json['harga'];
                $("input[name='detail_kode_barang']").val(kode);
                if(harga != null){
                  $("input[name='detail_harga_unit']").val(formatCurrency(harga));
                }
            }
        });    
    }

    function calculateGrandTotal(){
      if($('#discount_value').val() == ""){
        $('#discount_value').val("0");  
      }
      if($('#freight_value').val() == ""){
        $('#freight_value').val("0"); 
      }
      var subtotal_price = 0;
      var discount_price = unformatCurrency($('#discount_value').val());
      var freight_price = unformatCurrency($('#freight_value').val());
      var total_price = 0;


      $('input[name^="jumlah"]').each( function() {
          subtotal_price += parseInt(unformatCurrency(this.value));
      });
      
    $('#subtotal').val(subtotal_price);
    $('#text_subtotal').html(formatCurrency(subtotal_price));


    total_price = parseInt(subtotal_price) - parseInt(discount_price) + parseInt(freight_price);


    $('#total').val(total_price);
    $('#text_total').html(formatCurrency(total_price));
    }

  $().ready(function() {
    $(".select2").select2();
    $(".mask-money").maskMoney({precision:0,thousands:',', allowZero: true});
    $('.datepicker').datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
            todayHighlight: true
    });
    $("#content_form").validate({
      rules: {
        supplier: {
          notEqual: '0'
        },
        ship_to: {
          required: true
        }
      }/*,
      messages: {
        role: {
          notEqual: "Wajib diisi."
        }
      }*/
    });
    $( "#btn-modal-detail" ).click(function() {
      resetDetailModal();     
    });
    $( "#btn-add-detail" ).click(function() {
      if(validateDetailModal()){
        var barang = $("select[name='detail_barang']").val();       
        var text_kode_barang = "";
        var text_nama_barang = "";
        if(barang != 0){
          var temp_barang = $("select[name='detail_barang'] :selected").text();
          var temp_barang_array = temp_barang.split(" - ");
          text_kode_barang = temp_barang_array[0];
          text_nama_barang = temp_barang_array[1]; 
        }
        var harga_unit = $("input[name='detail_harga_unit']").val();
        var quantity = $("input[name='detail_quantity']").val();
        var discount = $("input[name='detail_discount']").val();
        var tax = $("input[name='detail_tax']").val();        

        harga_unit = unformatCurrency(harga_unit);
        discount = unformatCurrency(discount);

        var jumlah_gross = parseInt(quantity) * parseInt(harga_unit);
        var jumlah = calculateTotal(jumlah_gross, discount, tax);

        if(currentRow != null){
          var selectedRow = $(currentRow).parent();

          selectedRow.find("th:first").html("<input type = 'hidden' name = 'text_kode_barang[]' value = '" + text_kode_barang + "'><span>" + text_kode_barang + "</span>");
          selectedRow.find("td:eq(0)").html("<input type = 'hidden' name = 'text_nama_barang[]' value = '" + text_nama_barang + "'><span>" + text_nama_barang + "</span>");
          selectedRow.find("td:eq(1)").html("<input type = 'hidden' name = 'quantity[]' value = '" + quantity + "'><span>" + quantity + "</span>");
          selectedRow.find("td:eq(2)").html("<input type = 'hidden' name = 'harga_unit[]' value = '" + harga_unit + "'><span>" + formatCurrency(harga_unit) + "</span>");
          selectedRow.find("td:eq(3)").html("<input type = 'hidden' name = 'discount[]' value = '" + discount + "'><span>" + formatCurrency(discount) + "</span>");
          selectedRow.find("td:eq(4)").html("<input type = 'hidden' name = 'tax[]' value = '" + tax + "'><span>" + tax + "</span>");
          selectedRow.find("td:eq(5)").html("<input type = 'hidden' name = 'jumlah[]' value = '" + jumlah + "'><span>" + formatCurrency(jumlah) + "</span>");

          selectedRow.find("input[name='barang[]']").val(barang);
        } else {
          var append = "<tr class = 'detailRow'>" +
              "<th scope='row'><input type = 'hidden' name = 'text_kode_barang[]' value = '" + text_kode_barang + "'><span>" + text_kode_barang + "</span></th>" +
              "<td><input type = 'hidden' name = 'text_nama_barang[]' value = '" + text_nama_barang + "'><span>" + text_nama_barang + "</span></td>" +
              "<td><input type = 'hidden' name = 'quantity[]' value = '" + quantity + "'><span>" + quantity + "</span></td>" +
              "<td><input type = 'hidden' name = 'harga_unit[]' value = '" + harga_unit + "'><span>" + formatCurrency(harga_unit) + "</span></td>" +
              "<td><input type = 'hidden' name = 'discount[]' value = '" + discount + "'><span>" + formatCurrency(discount) + "</span></td>" +
              "<td><input type = 'hidden' name = 'tax[]' value = '" + tax + "'><span>" + tax + "</span></td>" +
              "<td><input type = 'hidden' name = 'jumlah[]' value = '" + jumlah + "'><span>" + formatCurrency(jumlah) + "</span></td>" +
              "<td><a href = '#' class = 'dt_remove'>Delete</a></td>" +
              
              "<input type = 'hidden' name = 'barang[]' value = '" + barang + "'>" + 
            "</tr>";
          
          append = append + "<script>" + 
            "$( '.detailRow > th' ).click(function() { " +
              "populateDetailModal(this); $('#detailModal').modal('toggle'); " +
            "});" + 
            "$( '.detailRow > td:not(:last)' ).click(function() { " +
              "populateDetailModal(this); $('#detailModal').modal('toggle'); " + 
            "}); " + 
            "$( '.dt_remove' ).click(function(e) { " +
                        "e.preventDefault(); " +
                        "currentRow = this; " +
                          "$('#confirmModal .modal-body').html('Are you sure you want to delete this row ?'); " +
                          "$('#confirmModal').modal({ " +
                            "show: true " +
                          "}); " +
                      "}); " +
            "<\/script>";
          $( "#detail-table > tbody" ).append(append);  
        }

        $('#detailModal').modal('toggle');
        resetDetailModal();
        calculateGrandTotal();
      }
    });
    $( ".detailRow > th" ).click(function() {
      populateDetailModal(this); 
      $('#detailModal').modal('toggle');
    });
    $( ".detailRow > td:not(:last)" ).click(function() {
      populateDetailModal(this); 
      $('#detailModal').modal('toggle');
    });
    $( ".dt_remove" ).click(function(e) {
          e.preventDefault();
          currentRow = this;
            $("#confirmModal .modal-body").html("Are you sure you want to delete this row ?");
            $('#confirmModal').modal({
              show: true
            });

        });
        $('#confirmModal').on('click', '#btn-confirm', function (e) {
            $(currentRow).parent().parent().remove();
            currentRow = null;
            $('#confirmModal').modal('toggle');
        });

        $( "#detail_barang" ).change(function() {
            var id = $(this).val();
            getBarangInfo(id);
        });

        $( "#discount_value" ).change(function() {
        calculateGrandTotal();
    });

        $( "#freight_value" ).change(function() {
        calculateGrandTotal();
    });
  });
  </script>
</body>