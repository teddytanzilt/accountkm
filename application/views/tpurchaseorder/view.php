<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/select2/dist/css/select2.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/jquery-minicolors/jquery.minicolors.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
<link rel="stylesheet" type="text/css" href="<?PHP echo VENDORPATH; ?>/quill/dist/quill.snow.css">
<style>
table{
  font-size:10px;
}
tbody > tr, tbody > td {
   white-space: nowrap; overflow: hidden; text-overflow:ellipsis;
}
#text_subtotal, #text_total, #text_sisa{
  font-size:14px; 
}
</style>
<body>
    <?PHP $this->load->view('parts/preloader'); ?>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <?PHP $this->load->view('parts/header_nav'); ?>
        <?PHP $this->load->view('parts/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <?PHP $this->load->view('parts/breadcrumb'); ?>
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            
                                <input type = "hidden" name="id" value="<?PHP echo isset($id)?$id:''; ?>">
                                <div class="card-body">
                                    <h4 class="card-title">Order Info</h4>                
                  
                  <?PHP if($this->session->flashdata('success_message')!=""): ?>
                  <div class="alert alert-success">
                    <strong>Success!</strong> <?PHP echo $this->session->flashdata('success_message'); ?>
                  </div>
                  <?PHP endif; ?>
                  <?PHP if(isset($validation) && isset($message)): ?>
                    <?PHP if(!$validation && $message != ""): ?>
                    <div class="alert alert-block alert-danger">
                      <strong>Error!</strong> <?PHP echo $message; ?>
                    </div>
                    <?PHP endif; ?>
                  <?PHP endif; ?> 
                  
                  <div class="row">
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="no_transaksi" class="col-sm-3 text-right control-label col-form-label">No Transaksi *</label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="no_transaksi" name="no_transaksi" type="text" value = "<?PHP echo isset($no_transaksi)?$no_transaksi:''; ?>" readonly disabled="true" />
                                            </div>
                                      </div>
                                      </div>
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                        <label for="tanggal" class="col-sm-3 text-right control-label col-form-label">Tanggal *</label>
                        <div class="input-group col-sm-9">
                          <input type="text" class="form-control datepicker" placeholder="yyyy-mm-dd" id="tanggal" name="tanggal" value = "<?PHP echo isset($tanggal)?$tanggal:''; ?>" readonly disabled="true" />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="supplier" class="col-sm-3 text-right control-label col-form-label">Supplier *</label>
                                            <div class="col-sm-9">
                          <select class="select2 form-control custom-select" name = "supplier" style="width: 100%; height:36px;" readonly disabled="true">
                            <option value = '0'>Select</option>
                            <?PHP foreach($supplier_list as $d): ?>
                              <option value="<?PHP echo $d->id; ?>" <?PHP echo isset($supplier)?(($supplier==$d->id)?'selected':''):''; ?>><?PHP echo $d->nama; ?></option>
                            <?PHP endforeach; ?>
                          </select>
                                            </div>
                                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="ship_to" class="col-sm-3 text-right control-label col-form-label">Ship To *</label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="ship_to" name="ship_to" type="text" value = "<?PHP echo isset($ship_to)?$ship_to:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="terms" class="col-sm-3 text-right control-label col-form-label">Terms </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="terms" name="terms" type="text" value = "<?PHP echo isset($terms)?$terms:''; ?>" />
                                            </div>
                                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="fob" class="col-sm-3 text-right control-label col-form-label">FOB </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="fob" name="fob" type="text" value = "<?PHP echo isset($fob)?$fob:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="ship_via" class="col-sm-3 text-right control-label col-form-label">Ship Via </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="ship_via" name="ship_via" type="text" value = "<?PHP echo isset($ship_via)?$ship_via:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="expected_date" class="col-sm-3 text-right control-label col-form-label">Expected Date </label>
                                            <div class="col-sm-9">
                          <input class=" form-control datepicker" id="expected_date" name="expected_date" type="text" value = "<?PHP echo isset($expected_date)?$expected_date:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="taxable_vendor" class="col-sm-3 text-right control-label col-form-label">Taxable Vendor </label>
                                            <div class="col-sm-9">
                          <select class="select2 form-control custom-select" name = "taxable_vendor" style="width: 100%; height:36px;" readonly disabled="true">
                            <option value = '0' <?PHP echo isset($taxable_vendor)?(($taxable_vendor=='0')?'selected':''):''; ?>>No</option>
                            <option value = '1' <?PHP echo isset($taxable_vendor)?(($taxable_vendor=='1')?'selected':''):''; ?>>Yes</option>
                          </select>
                                            </div>
                                        </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group row">
                                            <label for="rate" class="col-sm-3 text-right control-label col-form-label">Rate </label>
                                            <div class="col-sm-9">
                          <input class=" form-control" id="rate" name="rate" type="text" value = "<?PHP echo isset($rate)?$rate:''; ?>" readonly />
                                            </div>
                                        </div>
                    </div>
                  </div>

                  <div class = "row">
                    <div class="col-sm-6">
                                          <div class="form-group row">
                                                                <label for="faktur_pajak" class="col-sm-3 text-right control-label col-form-label">Faktur Pajak </label>
                                                                <div class="col-sm-9">
                                              <input class=" form-control" id="faktur_pajak" name="faktur_pajak" type="text" value = "<?PHP echo isset($faktur_pajak)?$faktur_pajak:''; ?>"  readonly disabled="true" />
                                                                </div>
                                                            </div>
                                        </div>
                    <div class="col-sm-6">
                                        <div class="form-group row">
                                            <label for="expor" class="col-sm-3 text-right control-label col-form-label">PO Expor *</label>
                                            <div class="col-sm-9">
                                                <select class="select2 form-control custom-select" name = "expor" style="width: 100%; height:36px;" readonly disabled="true">
                                                    <option value="0" <?PHP echo isset($expor)?(($expor=='0')?'selected':''):''; ?>>No</option>
                                                    <option value="1" <?PHP echo isset($expor)?(($expor=='1')?'selected':''):''; ?>>Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                      </div>
                  
                  </div>
         
                  <div class="row">
                    <div class="col-12">
                      <div class="card">
                        <div class="card-body">
                          <h5 class="card-title m-b-0">List Detail</h5>
                        </div>
                                        <div class="table-responsive">
                          <table class="table table-striped table-bordered" id = "detail-table">
                              <thead>
                              <tr>
                                <th scope="col">Item</th>
                                <th scope="col">Description</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Unit Price</th>
                                <th scope="col">Discount</th>
                                <th scope="col">Tax %</th>
                                <th scope="col">Amount</th>
                              </tr>
                              </thead>
                              <tbody>
                              <?PHP if(isset($barang)): ?>
                                <?PHP for($x = 0 ; $x < count($barang) ; $x++): ?>

                                  <tr class = "detailRow">
                                    <th scope="row"><input type = "hidden" name = "text_kode_barang[]" value = "<?PHP echo $text_kode_barang[$x]; ?>"><span><?PHP echo $text_kode_barang[$x]; ?></span></th>
                                    <td><input type = "hidden" name = "text_nama_barang[]" value = "<?PHP echo $text_nama_barang[$x]; ?>"><span><?PHP echo $text_nama_barang[$x]; ?></span></td>
                                    <td><input type = "hidden" name = "quantity[]" value = "<?PHP echo $quantity[$x]; ?>"><span><?PHP echo $quantity[$x]; ?></span></td>
                                    <td><input type = "hidden" name = "harga_unit[]" value = "<?PHP echo $harga_unit[$x]; ?>"><span><?PHP echo number_format($harga_unit[$x],0,'.',','); ?></span></td>
                                    <td><input type = "hidden" name = "discount[]" value = "<?PHP echo $discount[$x]; ?>"><span><?PHP echo number_format($discount[$x],0,'.',','); ?></span></td>
                                    <td><input type = "hidden" name = "tax[]" value = "<?PHP echo $tax[$x]; ?>"><span><?PHP echo $tax[$x]; ?></span></td>
                                    <td><input type = "hidden" name = "jumlah[]" value = "<?PHP echo $jumlah[$x]; ?>"><span><?PHP echo number_format($jumlah[$x],0,'.',','); ?></span></td>
      

                                    <input type = "hidden" name = "barang[]" value = "<?PHP echo $barang[$x]; ?>">                        
                                            </tr>

                                <?PHP endfor; ?>
                              <?PHP endif; ?>
                              </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                    <div class="col-6">
                      <div class="form-group row">
                                            <label for="note" class="col-sm-3 text-right control-label col-form-label">Note </label>
                                            <div class="col-sm-12">
                                              <textarea class=" form-control" id="note" name="note" rows = 8>
                                                <?PHP echo isset($note)?$note:''; ?>
                                              </textarea>
                                            </div>
                                        </div>
                    </div>

                    <div class="col-6">
                      <table class="table table-striped table-bordered" id = "summary-table">
                          <tbody>
                            <tr>
                              <td>Subtotal</td>
                              <td style = "text-align: right;">
                                <input id="subtotal" name="subtotal" type="hidden" value = "<?PHP echo isset($subtotal)?$subtotal:'0'; ?>" />
                                <span id = "text_subtotal"><?PHP echo isset($subtotal)?number_format($subtotal,0,'.',','):'0'; ?></span>
                              </td>
                            </tr>
                            <tr>
                              <td>Discount</td>
                              <td style = "text-align: right;">
                                <input class=" form-control mask-money" style = "text-align: right;" id="discount_value" name="discount_value" type="text" value = "<?PHP echo isset($discount_value)?number_format($discount_value,0,'.',','):'0'; ?>" readonly />
                              </td>
                            </tr>
                            <tr>
                              <td>Freight Value</td>
                              <td style = "text-align: right;">
                                <input class=" form-control mask-money" style = "text-align: right;" id="freight_value" name="freight_value" type="text" value = "<?PHP echo isset($freight_value)?number_format($freight_value,0,'.',','):'0'; ?>" readonly />
                              </td>
                            </tr>
                            <tr>
                              <td>Total</td>
                              <td style = "text-align: right;">
                                <input id="total" name="total" type="hidden" value = "<?PHP echo isset($total)?$total:'0'; ?>" />
                                <span id = "text_total"><?PHP echo isset($total)?number_format($total,0,'.',','):'0'; ?></span>
                              </td>
                            </tr>
                            <tr>
                              <td>Sisa Pembayaran</td>
                              <td style = "text-align: right;">
                                <input id="sisa" name="sisa" type="hidden" value = "<?PHP echo isset($sisa)?$sisa:'0'; ?>" />
                                <span id = "text_sisa"><?PHP echo isset($sisa)?number_format($sisa,0,'.',','):'0'; ?></span>
                              </td>
                            </tr>
                          </tbody>
                      </table>
                    </div>            
                  </div>

                  
                                    <!--<div class="row">
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                          <label for="prepared_by" class="col-sm-3 text-right control-label col-form-label">Prepared By </label>
                                          <div class="col-sm-9">
                                            <input class=" form-control" id="prepared_by" name="prepared_by" type="text" value = "<?PHP echo isset($prepared_by)?$prepared_by:''; ?>" readonly />
                                          </div>
                                        </div>  
                                      </div>
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                          <label for="prepared_date" class="col-sm-3 text-right control-label col-form-label">Prepared Date </label>
                                          <div class="col-sm-9">
                                            <input class=" form-control datepicker" id="prepared_date" name="prepared_date" type="text" value = "<?PHP echo isset($prepared_date)?$prepared_date:''; ?>" readonly />
                                            </div>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="row">
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                          <label for="approved_by" class="col-sm-3 text-right control-label col-form-label">Approved By </label>
                                            <div class="col-sm-9">
                                              <input class=" form-control" id="approved_by" name="approved_by" type="text" value = "<?PHP echo isset($approved_by)?$approved_by:''; ?>" readonly />
                                            </div>
                                        </div>
                                      </div>
                                      <div class="col-sm-6">
                                        <div class="form-group row">
                                          <label for="approved_date" class="col-sm-3 text-right control-label col-form-label">Approved Date </label>
                                          <div class="col-sm-9">
                                            <input class=" form-control datepicker" id="approved_date" name="approved_date" type="text" value = "<?PHP echo isset($approved_date)?$approved_date:''; ?>" readonly />
                                          </div>
                                        </div>
                                      </div>
                                    </div>-->
                                    
                                    <div class="row">
                                      <div class="col-12">
                                        <div class="card">
                                          <div class="card-body">
                                            <h5 class="card-title m-b-0">Payment List</h5>
                                          </div>
                                          <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id = "detail-table">
                                                <thead>
                                                <tr>
                                                  <th scope="col">Tanggal</th>
                                                  <th scope="col">Jumlah Bayar</th>
                                                  <th scope="col">Bank</th>
                                                  <th scope="col">No Giro</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?PHP if(isset($payment_list)): ?>
                                                  <?PHP foreach($payment_list as $p): ?>
                                                    <tr class = "detailRow">
                                                      <th scope="row"><span><?PHP echo $p->tanggal_bayar; ?></span></th>
                                                      <td><span><?PHP echo number_format($p->jumlah_bayar,0,'.',','); ?></span></td>
                                                      <td><span><?PHP echo $p->nama_bank; ?></span></td>
                                                      <td><span><?PHP echo $p->note; ?></span></td>
                                                    </tr>
                                                  <?PHP endforeach; ?>
                                                <?PHP endif; ?>
                                                </tbody>
                                            </table>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    
                                </div>


                                <div class="border-top">
                                    <div class="card-body">
                                      <a href="<?PHP echo $this->module_name; ?>" class="btn btn-danger">Cancel</a>
                                      <a href="<?PHP echo $this->module_name; ?>/preview/<?PHP echo $id; ?>" class="btn btn-success">Print</a>
                                    </div>
                                </div>
                        </div>
                        

                    </div>
                   
                </div>
                
                <!-- Modal -->
                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModal" aria-hidden="true ">
          <div class="modal-dialog" role="document ">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="confirmModal">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true ">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Body goes here...
              </div>
              <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-warning" type="button" id = "btn-confirm" data-id = ""> Confirm</button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="detailModal" role="dialog" aria-labelledby="detailModal" aria-hidden="true ">
          <div class="modal-dialog modal-lg" role="document ">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="detailModal">Detail Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true ">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group row">
                                    <label for="detail_barang" class="col-sm-3 text-right control-label col-form-label">Barang</label>
                                    <div class="col-sm-9">
                                      <select class="select2 form-control custom-select" id = "detail_barang" name = "detail_barang" style="width: 100%; height:36px;">
                      <option value = '0'>Select</option>
                      <?PHP foreach($barang_list as $d): ?>
                        <option value="<?PHP echo $d->id; ?>"><?PHP echo $d->kode; ?> - <?PHP echo $d->nama; ?></option>
                      <?PHP endforeach; ?>
                    </select>
                    <input type = "hidden" name = "detail_kode_barang">
                    <input type = "hidden" name = "detail_harga_unit">
                                    </div>
                                </div>
                
                                <div class="form-group row">
                                    <label for="detail_quantity" class="col-sm-3 text-right control-label col-form-label">Quantity</label>
                                    <div class="col-sm-9">
                    <input class=" form-control" name="detail_quantity" type="text" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="detail_discount" class="col-sm-3 text-right control-label col-form-label">Discount</label>
                                    <div class="col-sm-9">
                    <input class=" form-control" name="detail_discount" type="text" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="detail_tax" class="col-sm-3 text-right control-label col-form-label">Tax</label>
                                    <div class="col-sm-9">
                    <input class=" form-control" name="detail_tax" type="text" />
                                    </div>
                                </div>


              </div>
              <div class="modal-footer">
              <button data-dismiss="modal" class="btn btn-danger" type="button">Close</button>
              <button class="btn btn-success" type="button" id = "btn-add-detail" data-id = ""> Add</button>
              </div>
            </div>
          </div>
        </div>
        <!-- modal -->

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
    <?PHP $this->load->view('parts/inner_footer'); ?>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?PHP echo VENDORPATH; ?>/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?PHP echo VENDORPATH; ?>/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?PHP echo JSPATH; ?>/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?PHP echo JSPATH; ?>/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?PHP echo JSPATH; ?>/custom.min.js"></script>
  
    <script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.full.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/select2/dist/js/select2.min.js"></script>
    <script src="<?PHP echo VENDORPATH; ?>/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  
    <script src="<?PHP echo JSPATH; ?>/jquery.validate.min.js"></script>
    <script src="<?PHP echo JSPATH; ?>/script.js"></script>
    <script>
    $().ready(function() {
    
    });
  </script>
</body>