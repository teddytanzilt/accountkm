<?php
function getLastDate($year, $month)
{
	if(intval($month) == 1){
		return "31";
	} else if(intval($month) == 2){
		if ($year % 4 == 0) {
			return "29";		
		} else {
			return "28";
		}
	} else if(intval($month) == 3){
		return "31";		
	} else if(intval($month) == 4){
		return "30";		
	} else if(intval($month) == 5){
		return "31";		
	} else if(intval($month) == 6){
		return "30";		
	} else if(intval($month) == 7){
		return "31";		
	} else if(intval($month) == 8){
		return "31";		
	} else if(intval($month) == 9){
		return "30";		
	} else if(intval($month) == 10){
		return "31";		
	} else if(intval($month) == 11){
		return "30";		
	} else if(intval($month) == 12){
		return "31";		
	} else {
		return "30";
	}
}
?>