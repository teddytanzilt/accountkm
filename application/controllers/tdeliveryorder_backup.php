<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tdeliveryorder extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('master_customer_model');
		$this->load->model('trans_delivery_order_header_model');
		$this->load->model('trans_delivery_order_detail_model');
		$this->load->model('trans_sales_invoice_header_model');
		$this->load->model('trans_sales_invoice_detail_model');
		$this->load->model('master_customer_model');
		$this->load->model('master_barang_model');
		
		$this->module_name = "tdeliveryorder";
		$this->module_title = "Delivery Order";
		$this->table_name = "trans_delivery_order_header";
		$this->model_object = $this->trans_delivery_order_header_model;
		$this->model_detail_object = $this->trans_delivery_order_detail_model;
		
		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";
		
		$this->msg_add_success = "Create DO Success.";
		$this->msg_edit_success = "Edit DO Success.";
		$this->msg_delete_success = "Delete DO Success.";
		
		$this->trans_prefix = "DO";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("DELIVERYORDER", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}

	public function index($data = null)
	{
		$this->module_subtitle = "List";
		$content_cond = array('trans_sales_invoice_header.record_status' => STATUS_ACTIVE);
		$this->trans_sales_invoice_header_model->setCond($content_cond);

		$data[$this->module_name.'_list'] = $this->trans_sales_invoice_header_model->getListArray();

		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->view_list;		
		$this->load->view('parts/template',$data);
	}

	public function deliver_load_data($data = null) {
		$data['customer_list'] = $this->master_customer_model->getActiveList();
		$data['barang_list'] = $this->master_barang_model->getActiveList();

		if(!isset($data['barang'])){
			$content_detail_list = $this->trans_sales_invoice_detail_model->getTransDetailCompositeByHeader($data['id']);
			$fillable_value = $this->trans_sales_invoice_detail_model->getFillableValueList();
			foreach($fillable_value as $fv){
				$data[$fv] = array();
			}
			foreach($content_detail_list as $d){
				foreach($fillable_value as $fv){
					$data[$fv][] = $d[$fv];
				}
				$data['text_kode_barang'][] = $d['text_kode_barang'];
				$data['text_nama_barang'][] = $d['text_nama_barang'];
			}
		}
		
		return $data;
	}

	public function deliver_validation_data($result, $data) {
		return $result;
	}

	public function view_load_data($data = null) {
		$data['customer_list'] = $this->master_customer_model->getActiveList();
		$data['barang_list'] = $this->master_barang_model->getActiveList();

		if(!isset($data['barang'])){
			$content_detail_list = $this->trans_sales_invoice_detail_model->getTransDetailCompositeByHeader($data['id']);
			$fillable_value = $this->trans_sales_invoice_detail_model->getFillableValueList();
			foreach($fillable_value as $fv){
				$data[$fv] = array();
			}
			foreach($content_detail_list as $d){
				foreach($fillable_value as $fv){
					$data[$fv][] = $d[$fv];
				}
				$data['text_kode_barang'][] = $d['text_kode_barang'];
				$data['text_nama_barang'][] = $d['text_nama_barang'];
			}
		}

		return $data;
	}

	public function view($id, $data = null)
	{
		$this->module_subtitle = "View";
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->trans_sales_invoice_header_model->setCond($content_cond);
		$content_detail = $this->trans_sales_invoice_header_model->getHeaderArray();	
		foreach($content_detail as $arr_name => $arr_value){
			$data[$arr_name] = $arr_value;
		}
		$data = $this->view_load_data($data);

		$delivery_order_cond = array('record_status' => STATUS_ACTIVE, 'invoice' => $id);
		$this->trans_delivery_order_header_model->setCond($delivery_order_cond);
		$delivery_order_header_value = $this->trans_delivery_order_header_model->getHeaderArray();	
		$data['no_transaksi'] = $delivery_order_header_value['no_transaksi'];

		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->module_name."/view";		
		$this->load->view('parts/template',$data);
	}

	public function get_data_list_active_json(){	
		$request_data= $_REQUEST;
		$column = $this->trans_sales_invoice_header_model->getDatatableDoShowValueList();
		$total_data = $this->trans_sales_invoice_header_model->getCountDoDataActive();
		$total_filtered = $this->trans_sales_invoice_header_model->getDataTableDoCountActive($request_data);
		$data_filtered = $this->trans_sales_invoice_header_model->getDataTableDoDataActive($request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}

	public function deliver($id, $data = null)
	{
		$this->module_subtitle = "Deliver";
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->trans_sales_invoice_header_model->setCond($content_cond);
		$content_detail = $this->trans_sales_invoice_header_model->getHeaderArray();	
		foreach($content_detail as $arr_name => $arr_value){
			$data[$arr_name] = $arr_value;
		}
		$data = $this->deliver_load_data($data);
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->module_name."/deliver";		
		$this->load->view('parts/template',$data);
	}

	public function deliver_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		if($result['validation']) {
			$result = $this->deliver_validation_data($result, $data);
		}	

		//var_dump($data); exit;
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$sales_invoice_header_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
					$this->trans_sales_invoice_header_model->setCond($sales_invoice_header_cond);	
					$sales_invoice_header_value = $this->trans_sales_invoice_header_model->getHeaderArray();
					$sales_invoice_header_value["delivery_status"] = STATUS_COMPLETE;
					$sales_invoice_header_value["modified_by"] = $this->session_user_id;
					$sales_invoice_header_value["modified_on"] = date('Y-m-d H:i:s');
					$this->trans_sales_invoice_header_model->setValueList($sales_invoice_header_value);		
					$this->trans_sales_invoice_header_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$data['customer'] = $sales_invoice_header_value['customer'];
					$data['ship_date'] = $sales_invoice_header_value['ship_date'];
					$data['prepared_date'] = $sales_invoice_header_value['prepared_date'];
					$data['approved_date'] = $sales_invoice_header_value['approved_date'];
					$data['shipped_date'] = $sales_invoice_header_value['shipped_date'];
					$data['received_date'] = $sales_invoice_header_value['received_date'];

					$next_id = $this->model_object->nextTrans();
					$data['invoice'] = $data['id'];
					$fillable_value = $this->model_object->getFillableValueList();
					$content_value = $this->model_object->getValueList();
					$content_value['no_transaksi'] = '';
					$content_value['tanggal'] = date('Y-m-d H:i:s');
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["created_by"] = $this->session_user_id;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->insertHeader();
					$content_id = $this->db->insert_id();
					$no_transaksi = $this->trans_prefix.date('Ym').sprintf("%05d", $next_id);
					$content_value['id'] = $content_id;
					$content_value['no_transaksi'] = $no_transaksi;
					//if($data['action'] == "Finish"){
						$content_value['trans_status'] = STATUS_COMPLETE;
					//} else {
					//	$content_value['trans_status'] = STATUS_PENDING;
					//}
					$this->model_object->setValueList($content_value);		
					$value_condition = array('id' => $content_id);
					$this->model_object->setCond($value_condition);
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }


					$sales_invoice_detail_cond = array('header_id' => $data['id']);
					$this->trans_sales_invoice_detail_model->setCond($sales_invoice_detail_cond);
					$sales_invoice_detail_list = $this->trans_sales_invoice_detail_model->getListArray();
					foreach ($sales_invoice_detail_list as $dtl) {
						$this->model_detail_object->refreshValueList();
						$fillable_value = $this->model_detail_object->getFillableValueList();
						$content_value = $this->model_detail_object->getValueList();
						$content_value['header_id'] = $content_id;
						foreach($fillable_value as $fv){
							$content_value[$fv] = $dtl[$fv];
						}
						$content_value["created_by"] = $this->session_user_id;
						$content_value["created_on"] = date('Y-m-d H:i:s');
						$this->model_detail_object->setValueList($content_value);		
						$this->model_detail_object->insertHeader();
						if ($this->db->trans_status() === FALSE){ break; }						
					}

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_edit_success);
			redirect($this->link_back);
		} else {
			$this->edit($data['id'], $data);
		}
	}

	public function preview($id, $data = null)
	{
		$this->load->library('datetime_library');
		$this->load->library('pdf');	
		$this->pdf->fontpath = 'assets/fonts/pdf/'; 
		
		$this->pdf->AddFont('Calibri');
			$this->pdf->AddFont('Calibri-Bold','','calibrib.php');
			$this->pdf->AliasNbPages();
			$this->pdf->Open();
			$this->pdf->SetAutoPageBreak(false);
			
			$this->pdf->AddPage();
			$this->pdf->SetFont('Calibri-Bold','',18);
			$x1 = 9;
			$y1 = 15;
			$w = 190;
			$h = 10; 
			$border = 1; 
			$lineBreak = 1; 
			$text = "INVOICE";
			$align = "C"; 
			$this->pdf->SetXY ($x1,$y1);
			$this->pdf->Cell($w,$h,$text,$border,$lineBreak,$align);
				
			$name = "PT SUKSES UTAMA NIAGA";
			$address = "Jl. Bungur Besar No.53 III-IV \n".
						"Kemayoran, Jakarta Pusat \n".
						"Daerah Khusus Ibukota Jakarta 10610, Indonesia \n".
						"Telp : (021) 4260190  \n".
						"Fax  : (021) 4226088";
			$x1 = 10;
			$y1 = 30;
			$this->pdf->SetXY ($x1,$y1);			
			$len = $this->pdf->GetStringWidth( $name );//the width
			$h = 2;			
			$this->pdf->SetFont('Calibri-Bold','',11);
			$this->pdf->Cell( $len, $h, $name);
			$this->pdf->SetXY( $x1, $y1 + 4 );			
			$length = $this->pdf->GetStringWidth( $address );
			$this->pdf->SetFont('Calibri','',11);
			$this->pdf->MultiCell($length, 4, $address);
		
			$tanggal = $this->datetime_library->indonesian_date($this->datetime_library->date_format('2018-11-12'.' 00:00:00', 'l jS F Y'), 'l, j F Y', '');
			$sDate = $tanggal;
			$sPO = $data['NoTransaction'];
			$pg = '1';
			$x1 = 132;
			$y1 = 30;
			$this->pdf->SetXY ($x1,$y1);
			$w = 33;
			$h=5;
			$this->pdf->SetFont('Calibri','',11);
			$this->pdf->Cell ($w,$h,'Date',1,'','R');
			$this->pdf->Cell($w+1,$h,$sDate,1,'','C');
			$this->pdf->SetXY ($x1,$y1+$h);
			$this->pdf->Cell ($w,$h,'P.O No.',1,'','R');
			$this->pdf->Cell($w+1,$h,$sPO,1,'','C');
			$this->pdf->SetXY ($x1,$y1+$h+$h);
			$this->pdf->Cell ($w,$h,'Page(s)',1,'','R');
			$this->pdf->Cell($w+1,$h,$pg,1,'','C');
			
			$name = "Supplier";
			$address = "Alamat Supplier";
			$x1 = 11;
			$y1 = 64;
			$this->pdf->SetXY ($x1,$y1);
			$w = 44;
			$h = 5;
			$this->pdf->SetFont('Calibri-Bold','',11);
			$this->pdf->SetTextColor(255,255,255); 
			$this->pdf->SetFillColor(128,128,128); 
			$this->pdf->Cell($w,$h,'Vendor Address','','','C',true);
			$this->pdf->SetTextColor(0,0,0);
			$x2 = 10;
			$y2 = 70;
			$this->pdf->SetXY ($x2,$y2);
			$len = $this->pdf->GetStringWidth( $name );
			$h = 2;
			$box_width = 90;
			$box_height = 30;
			$this->pdf->Rect($x2,$y2-1,$box_width,$box_height);
			$this->pdf->SetFont('Calibri-Bold','',11);
			$this->pdf->Cell( $len, $h, $name);
			$this->pdf->SetXY( $x2, $y2 + 4 );
			$length = $this->pdf->GetStringWidth( $address );
			$this->pdf->SetFont('Calibri','',11);
			$this->pdf->MultiCell($length, 4, $address);

			$supID = "CodeSup"	;
			$ReffNo = "XXX123";
			$DateReff = $tanggal;
			$ConfNo = "1235124";
			$ConfDate = $tanggal;
			$ETA = $tanggal;
			$x1 = 132;
			$y1 = 69;
			$this->pdf->SetXY ($x1,$y1);
			$w = 33;
			$h=5;
			$this->pdf->SetFont('Calibri','',11);
			$this->pdf->Cell ($w,$h,'Supplier ID',1,'','R');
			$this->pdf->Cell($w+1,$h,$supID,1,'','C');
			$this->pdf->SetXY ($x1,$y1+$h);
			$this->pdf->Cell ($w,$h,'No. Reff.',1,'','R');
			$this->pdf->Cell($w+1,$h,$ReffNo,1,'','C');
			$this->pdf->SetXY ($x1,$y1+$h+$h);
			$this->pdf->Cell ($w,$h,'Date Reff.',1,'','R');
			$this->pdf->Cell($w+1,$h,$DateReff,1,'','C');
			$this->pdf->SetXY ($x1,$y1+$h+$h+$h);
			$this->pdf->Cell ($w,$h,'Confirmation No.',1,'','R');
			$this->pdf->Cell($w+1,$h,$ConfNo,1,'','C');
			$this->pdf->SetXY ($x1,$y1+$h+$h+$h+$h);
			$this->pdf->Cell ($w,$h,'Confirmation Date.',1,'','R');
			$this->pdf->Cell($w+1,$h,$ConfDate,1,'','C');
			$this->pdf->SetXY ($x1,$y1+$h+$h+$h+$h+$h);
			$this->pdf->Cell ($w,$h,'Est. Arrival',1,'','R');
			$this->pdf->Cell($w+1,$h,$ETA,1,'','C');
			
			$x1 = 10;
			$y1 = 110;
			$this->pdf->SetXY ($x1,$y1);
			$this->pdf->SetFont('Calibri-Bold','',11);
			$this->pdf->SetTextColor(255,255,255); 
			$this->pdf->SetFillColor(128,128,128); 
			$this->pdf->Cell(9, 5, 'No', 1, '', 'C',true);
			$this->pdf->Cell(30, 5, 'PartNo', 1, '', 'C', true);
			$this->pdf->Cell(45, 5, 'Description', 1, '', 'C', true);
			$this->pdf->Cell(10, 5, 'Qty', 1, '', 'C', true);
			$this->pdf->Cell(25, 5, 'Discount', 1, '', 'C', true);
			$this->pdf->Cell(35, 5, 'Unit Price', 1, '', 'C', true);
			$this->pdf->Cell(35, 5, 'Total Price', 1, '', 'C', true);
			$this->pdf->Ln();
			$this->pdf->SetTextColor(0,0,0); 
			$this->pdf->SetFont('Calibri','',11);
			$num = 1;
			//foreach($ListPart as $valueBarang){
				$HargaSatuanOriginal 	= 0;
				$Merk					= "";
				$Tipe 					= "";
				

				$PartNoSupp 	= "PartNoSupp";
				$PartNo 		= "PartNo";
				$PartNoOrigin	= "PartNoOrigin";
				$Qty			= 2;
				$TipeHarga		= "TipeHarga";
				$HargaSatuan	= "12.000";
				$HargaSatuanOriginal = "11.000";
				$Diskon			= "1.000";
				$NamaBarang 	= "NamaBarang";
				$Merk			= "Merk";
				$KodeMerk		= "Kode Merk";
				$Satuan 		= "Pcs";
				$Subtotal	= "100";
				$Tipe			= $Tipe;
				$Kodekelompok 	= "GroupBarang";
				$this->pdf->Cell(9, 5, $num, 1, '', 'C');
				$this->pdf->Cell(30, 5, substr($PartNo, 0, 16), 1, '', 'C');
				$this->pdf->Cell(45, 5, substr($NamaBarang, 0, 27), 1, '', 'C');
				$this->pdf->Cell(10, 5, $Qty, 1, '', 'C');
				$this->pdf->Cell(25, 5, $Diskon.'%', 1, '', 'C');
				$this->pdf->Cell(35, 5, $HargaSatuan, 1, '', 'C');
				$this->pdf->Cell(35, 5, $Subtotal, 1, '', 'R');
				$this->pdf->Ln();	
				$num++;
			//}	
			for ($x = $num; $x <= 20; $x++) {
			$this->pdf->Cell(9, 5, $x, 1, '', 'C');
			$this->pdf->Cell(30, 5, '', 1, '', 'C');
			$this->pdf->Cell(45, 5, '', 1, '', 'C');
			$this->pdf->Cell(10, 5, '', 1, '', 'C');
			$this->pdf->Cell(25, 5, '', 1, '', 'C');
			$this->pdf->Cell(35, 5, '', 1, '', 'C');
			$this->pdf->Cell(35, 5, '', 1, '', 'C');
			$this->pdf->Ln();	
			}
			$this->pdf->Cell(119);
			$this->pdf->Cell(35, 5, 'Sub Total', 1, '', 'C');
			$this->pdf->Cell(35, 5, "200.000", 1, '', 'R');
			$this->pdf->Ln();
			$this->pdf->Cell(119);
			$this->pdf->Cell(35, 5, 'PPn '."10".'%', 1, '', 'C');
			$this->pdf->Cell(35, 5, "20.000", 1, '', 'R');
			$this->pdf->Ln();
			$this->pdf->Cell(119);
			$this->pdf->Cell(35, 5, 'Grand Total', 1, '', 'C');
			$this->pdf->Cell(35, 5, "220.000", 1, '', 'R');
			$this->pdf->Ln();
			$this->pdf->Ln();
			
			$x1 = 11;
			$this->pdf->SetX ($x1);
			$w = 44;
			$h = 5;
			$this->pdf->SetFont('Calibri-Bold','',11);
			$this->pdf->SetTextColor(255,255,255); 
			$this->pdf->SetFillColor(128,128,128); 
			$this->pdf->Cell($w,$h,'Remarks','','','C',true);
			$this->pdf->Ln();			
			$this->pdf->SetTextColor(0,0,0);
			$this->pdf->SetFont('Calibri','',11);
			
			$Num = 1;
			//foreach($ListTerms as $valueTerms){
				$Keterangan 	= "Note";
				$this->pdf->Cell(189, 5, $Keterangan, 1, 1); 
				$Num++;
			//}
			for ($x = $num; $x <= 4; $x++) {
				$this->pdf->Cell(189, 5, '', 1, 1); 
			}
			$this->pdf->Ln();
			
			$orderBy = "Teddy";
			$orderPost = "";
			$appBy = "Teddy";
			$apvPost = "Supplier";
			$this->pdf->SetFont('Calibri','',11);
			$this->pdf->Cell(9);
			$this->pdf->Cell(30, 5, 'Order By', '', '', 'C');
			$this->pdf->Cell(115); 
			$this->pdf->Cell(35, 5, 'Approve by', '', '', 'C');
			$this->pdf->Ln();
			$this->pdf->Ln();
			$this->pdf->Ln();
			$x1=19;
			$y1=285;
			$this->pdf->Cell(9); 
			$this->pdf->Cell(30, 5, $orderBy, '', '', 'C');
			$this->pdf->Line($x1,$y1,$x1+30,$y1);
			$this->pdf->Cell(115);
			$this->pdf->Cell(35, 5, $appBy, '', '', 'C');
			$this->pdf->Line($x1+30+115,$y1,$x1+30+115+35,$y1);
			$this->pdf->Ln();
			$this->pdf->Cell(9); 
			$this->pdf->Cell(30, 5, $orderPost, '', '', 'C');
			$this->pdf->Cell(115); 
			$this->pdf->Cell(35, 5, $apvPost, '', '', 'C');
			
			$this->pdf->Output();
	}
}