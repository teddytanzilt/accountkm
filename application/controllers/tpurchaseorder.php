<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tpurchaseorder extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('master_customer_model');
		$this->load->model('trans_purchase_order_header_model');
		$this->load->model('trans_purchase_order_detail_model');
		$this->load->model('master_supplier_model');
		$this->load->model('master_barang_model');
		$this->load->model('trans_cash_bank_model');
		$this->load->model('master_rekening_bank_model');
		$this->load->model('master_stock_barang_model');
		$this->load->model('trans_inventory_barang_model');
		$this->load->model('trans_purchase_order_payment_model');
		
		$this->module_name = "tpurchaseorder";
		$this->module_title = "Purchase Order";
		$this->table_name = "trans_purchase_order_header";
		$this->model_object = $this->trans_purchase_order_header_model;
		$this->model_detail_object = $this->trans_purchase_order_detail_model;
		
		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";
		
		$this->msg_add_success = "Create PO Success.";
		$this->msg_edit_success = "Edit PO Success.";
		$this->msg_delete_success = "Delete PO Success.";
		
		$this->trans_prefix = "PO";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("PURCHASEORDER", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}

	public function create_load_data($data = null) {
		$data['supplier_list'] = $this->master_supplier_model->getActiveList();
		$data['barang_list'] = $this->master_barang_model->getActiveList();
		$data['tanggal'] = date('Y-m-d');
		return $data;
	}

	public function create_validation_data($result, $data) {
		if($result['validation']) {
			if($data['action'] == "Finish"){
				if(!isset($data['barang'])){
					$result['validation'] = false;
					$result['message'] = "Transaksi yang di finish tidak bole tidak memiliki item";
				}
			}
		}	
		if($result['validation']) {
			if(count($data['barang']) > 10){
				$result['validation'] = false;
				$result['message'] = "Jumlah barang tidak boleh lebih dari 10";
			}
		}
		return $result;
	}
	
	public function edit_load_data($data = null) {
		if($data['trans_status'] == STATUS_COMPLETE) {
			redirect($this->link_view.$data['id']);
		} 
		
		$data['supplier_list'] = $this->master_supplier_model->getActiveList();
		$data['barang_list'] = $this->master_barang_model->getActiveList();

		if(!isset($data['barang'])){
			$content_detail_list = $this->model_detail_object->getTransDetailCompositeByHeader($data['id']);
			$fillable_value = $this->model_detail_object->getFillableValueList();
			foreach($fillable_value as $fv){
				$data[$fv] = array();
			}
			foreach($content_detail_list as $d){
				foreach($fillable_value as $fv){
					$data[$fv][] = $d[$fv];
				}
				$data['text_kode_barang'][] = $d['text_kode_barang'];
				$data['text_nama_barang'][] = $d['text_nama_barang'];
			}
		}
		
		return $data;
	}

	public function edit_validation_data($result, $data) {
		if($result['validation']) {
			if($data['action'] == "Finish"){
				if(!isset($data['barang'])){
					$result['validation'] = false;
					$result['message'] = "Transaksi yang di finish tidak bole tidak memiliki item";
				}
			}
		}	
		if($result['validation']) {
			if(count($data['barang']) > 10){
				$result['validation'] = false;
				$result['message'] = "Jumlah barang tidak boleh lebih dari 10";
			}
		}
		return $result;
	}

	public function view_load_data($data = null) {
		if($data['trans_status'] == STATUS_PENDING) {
			redirect($this->link_edit.$data['id']);
		}  

		$data['supplier_list'] = $this->master_supplier_model->getActiveList();
		$data['barang_list'] = $this->master_barang_model->getActiveList();

		/*$trans_purchase_order_payment_cond = array('record_status' => STATUS_ACTIVE, 'detail_status' => STATUS_ACTIVE, 'header_id' => $data['id']);
		$this->trans_purchase_order_payment_model->setCond($trans_purchase_order_payment_cond);
		$data['payment_list'] = $this->trans_purchase_order_payment_model->getList();*/

		$data['payment_list'] = $this->trans_purchase_order_payment_model->getPaymentComposite($data['id']);

		if(!isset($data['barang'])){
			$content_detail_list = $this->model_detail_object->getTransDetailCompositeByHeader($data['id']);
			$fillable_value = $this->model_detail_object->getFillableValueList();
			foreach($fillable_value as $fv){
				$data[$fv] = array();
			}
			foreach($content_detail_list as $d){
				foreach($fillable_value as $fv){
					$data[$fv][] = $d[$fv];
				}
				$data['text_kode_barang'][] = $d['text_kode_barang'];
				$data['text_nama_barang'][] = $d['text_nama_barang'];
			}
		}

		return $data;
	}

	public function get_data_list_active_json(){	
		$request_data= $_REQUEST;
		$column = $this->model_object->getDatatableShowValueList();
		$total_data = $this->model_object->getCountDataActive();
		$total_filtered = $this->model_object->getDataTableCountActive($request_data);
		$data_filtered = $this->model_object->getDataTableDataActive($request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}

	public function create_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		if($result['validation']) {
			$result = $this->create_validation_data($result, $data);
		}
		//$result['validation'] = false; //TEST
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$year = intval(date("Y",strtotime($data['tanggal'])));
					$month = intval(date("m",strtotime($data['tanggal'])));
					$next_id = $this->model_object->nextDynamicTrans($year, $month);
					//$next_id = $this->model_object->nextTrans();
					$fillable_value = $this->model_object->getFillableValueList();
					$content_value = $this->model_object->getValueList();
					$content_value['no_transaksi'] = '';
					//$content_value['tanggal'] = date('Y-m-d H:i:s');
					$content_value['tanggal'] = $data['tanggal'].' '.date('H:i:s');
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value['sisa'] = $data['total'];
					$content_value["created_by"] = $this->session_user_id;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->insertHeader();
					$content_id = $this->db->insert_id();
					$data['id'] = $content_id;
					$no_transaksi = $this->trans_prefix.date("Y",strtotime($data['tanggal'])).date("m",strtotime($data['tanggal'])).sprintf("%05d", $next_id);
					$content_value['id'] = $content_id;
					$content_value['no_transaksi'] = $no_transaksi;
					if($data['action'] == "Finish"){
						$content_value['trans_status'] = STATUS_COMPLETE;
					} else {
						$content_value['trans_status'] = STATUS_PENDING;
					}
					$this->model_object->setValueList($content_value);		
					$value_condition = array('id' => $content_id);
					$this->model_object->setCond($value_condition);
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					if($data['action'] == "Finish"){


					}
					if ($this->db->trans_status() === FALSE){ break; }

					$reff_count = $this->trans_inventory_barang_model->getCountReffId($content_id, 1);
					$second_added = intval($reff_count) * 30;
					$tanggal = $data['tanggal'];
					$dTanggal = new DateTime($tanggal);
					$dTanggal->add(new DateInterval('PT'.$second_added.'S'));
					$tanggal = $dTanggal->format('Y-m-d H:i:s');
					
					if(isset($data['barang'])){
						for($x = 0 ; $x < count($data['barang']) ; $x++){
							$this->model_detail_object->refreshValueList();
							$fillable_value = $this->model_detail_object->getFillableValueList();
							$content_value = $this->model_detail_object->getValueList();
							$content_value['header_id'] = $content_id;
							foreach($fillable_value as $fv){
								$content_value[$fv] = $data[$fv][$x];
							}
							$content_value["created_by"] = $this->session_user_id;
							$content_value["created_on"] = date('Y-m-d H:i:s');
							$this->model_detail_object->setValueList($content_value);		
							$this->model_detail_object->insertHeader();
							if ($this->db->trans_status() === FALSE){ break; }

							if($data['action'] == "Finish"){
								$gudang = 0;
								$saldo_awal = $this->master_stock_barang_model->saldoAwal($data['barang'][$x], $gudang);
								$initial_saldo = FALSE;
								if($saldo_awal == NULL){
									$initial_saldo = TRUE;
									$saldo_awal = 0;
								}
								$jumlah_masuk = $data['quantity'][$x];
								$jumlah_keluar = 0;
								$saldo_akhir = intval($saldo_awal) + intval($jumlah_masuk) - intval($jumlah_keluar);
								$content_value = $this->master_stock_barang_model->getValueList();
								$content_value['id_barang'] = $data['barang'][$x];
								$content_value['id_gudang'] = $gudang;
								$content_value['jumlah'] = $saldo_akhir;
								$this->master_stock_barang_model->setValueList($content_value);		
								if($initial_saldo){
									$this->master_stock_barang_model->insertHeader();
								} else {
									$value_condition = array('id_barang' => $data['barang'][$x] , 'id_gudang' => $gudang);
									$this->master_stock_barang_model->setCond($value_condition);
									$this->master_stock_barang_model->updateHeader();
								}
								if ($this->db->trans_status() === FALSE){ break; }
								
								$saldo_awal_inventory = $this->trans_inventory_barang_model->getSaldoPerTanggal($tanggal, $data['barang'][$x]);
								$saldo_akhir_inventory = intval($saldo_awal_inventory) + intval($jumlah_masuk) - intval($jumlah_keluar);
								$content_value = $this->trans_inventory_barang_model->getValueList();
								$content_value['id_barang'] = $data['barang'][$x];
								$content_value['id_gudang'] = $gudang;
								$content_value['id_user'] = $this->session_user_id;
								//$content_value['trans_date'] = date('Y-m-d H:i:s');
								$content_value['trans_date'] = $tanggal;
								$content_value['comment'] = $no_transaksi;
								//$content_value['saldo_awal'] = $saldo_awal;
								$content_value['saldo_awal'] = $saldo_awal_inventory;
								$content_value['jumlah_masuk'] = $jumlah_masuk;
								$content_value['jumlah_keluar'] = $jumlah_keluar;
								//$content_value['saldo_akhir'] = $saldo_akhir;
								$content_value['saldo_akhir'] = $saldo_akhir_inventory;
								$content_value['reff_id'] = $content_id;
								$content_value["created_by"] = $this->session_user_id;
								$content_value["created_on"] = date('Y-m-d H:i:s');
								$this->trans_inventory_barang_model->setValueList($content_value);		
								$this->trans_inventory_barang_model->insertHeader();
								$inventory_id = $this->db->insert_id();
								if ($this->db->trans_status() === FALSE){ break; }

								$updateAmount = intval($jumlah_masuk) - intval($jumlah_keluar);
								$this->trans_inventory_barang_model->updateSaldoPerTanggal($updateAmount, $tanggal, $inventory_id, $data['barang'][$x]);
								if ($this->db->trans_status() === FALSE){ break; }
							}
						}
						if ($this->db->trans_status() === FALSE){ break; }
					}
					
					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			if($data['action'] == "Finish"){
				redirect($this->link_back."/preview/".$data['id']);
			} else {
				$this->session->set_flashdata("success_message", $this->msg_add_success);
				redirect($this->link_back);
			}
		} else {
			$this->create($data);
		}
	}

	public function edit_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		if($result['validation']) {
			$result = $this->edit_validation_data($result, $data);
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$fillable_value = $this->model_object->getFillableValueList();
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
					$this->model_object->setCond($content_cond);	
					$content_value = $this->model_object->getHeaderArray();
					$no_transaksi = $content_value['no_transaksi'];
					$content_value['tanggal'] = $data['tanggal'].' '.date('H:i:s');
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value['sisa'] = $data['total'];
					$content_value["modified_by"] = $this->session_user_id;
					$content_value["modified_on"] = date('Y-m-d H:i:s');
					if($data['action'] == "Finish"){
						$content_value['trans_status'] = STATUS_COMPLETE;
					} else {
						$content_value['trans_status'] = STATUS_PENDING;
					}
					$this->model_object->setValueList($content_value);		
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					if($data['action'] == "Finish"){
						
					}
					if ($this->db->trans_status() === FALSE){ break; }

					$reff_count = $this->trans_inventory_barang_model->getCountReffId($data['id'], 1);
					$second_added = intval($reff_count) * 30;
					$tanggal = $data['tanggal'];
					$dTanggal = new DateTime($tanggal);
					$dTanggal->add(new DateInterval('PT'.$second_added.'S'));
					$tanggal = $dTanggal->format('Y-m-d H:i:s');
					
					if(isset($data['barang'])){
						$content_detail_cond = array('header_id' => $data['id']);
						$this->model_detail_object->setCond($content_detail_cond);	
						$this->model_detail_object->deleteHeader();
						if ($this->db->trans_status() === FALSE){ break; }

						for($x = 0 ; $x < count($data['barang']) ; $x++){
							$this->model_detail_object->refreshValueList();
							$fillable_value = $this->model_detail_object->getFillableValueList();
							$content_value = $this->model_detail_object->getValueList();
							$content_value['header_id'] = $data['id'];
							foreach($fillable_value as $fv){
								$content_value[$fv] = $data[$fv][$x];
							}
							$content_value["created_by"] = $this->session_user_id;
							$content_value["created_on"] = date('Y-m-d H:i:s');
							$this->model_detail_object->setValueList($content_value);		
							$this->model_detail_object->insertHeader();
							if ($this->db->trans_status() === FALSE){ break; }

							if($data['action'] == "Finish"){
								$gudang = 0;
								$saldo_awal = $this->master_stock_barang_model->saldoAwal($data['barang'][$x], $gudang);
								$initial_saldo = FALSE;
								if($saldo_awal == NULL){
									$initial_saldo = TRUE;
									$saldo_awal = 0;
								}
								$jumlah_masuk = $data['quantity'][$x];
								$jumlah_keluar = 0;
								$saldo_akhir = intval($saldo_awal) + intval($jumlah_masuk) - intval($jumlah_keluar);
								$content_value = $this->master_stock_barang_model->getValueList();
								$content_value['id_barang'] = $data['barang'][$x];
								$content_value['id_gudang'] = $gudang;
								$content_value['jumlah'] = $saldo_akhir;
								$this->master_stock_barang_model->setValueList($content_value);		
								if($initial_saldo){
									$this->master_stock_barang_model->insertHeader();
								} else {
									$value_condition = array('id_barang' => $data['barang'][$x] , 'id_gudang' => $gudang);
									$this->master_stock_barang_model->setCond($value_condition);
									$this->master_stock_barang_model->updateHeader();
								}
								if ($this->db->trans_status() === FALSE){ break; }
								
								$saldo_awal_inventory = $this->trans_inventory_barang_model->getSaldoPerTanggal($tanggal, $data['barang'][$x]);
								$saldo_akhir_inventory = intval($saldo_awal_inventory) + intval($jumlah_masuk) - intval($jumlah_keluar);
								$content_value = $this->trans_inventory_barang_model->getValueList();
								$content_value['id_barang'] = $data['barang'][$x];
								$content_value['id_gudang'] = $gudang;
								$content_value['id_user'] = $this->session_user_id;
								//$content_value['trans_date'] = date('Y-m-d H:i:s');
								$content_value['trans_date'] = $tanggal;
								$content_value['comment'] = $no_transaksi;
								//$content_value['saldo_awal'] = $saldo_awal;
								$content_value['saldo_awal'] = $saldo_awal_inventory;
								$content_value['jumlah_masuk'] = $jumlah_masuk;
								$content_value['jumlah_keluar'] = $jumlah_keluar;
								//$content_value['saldo_akhir'] = $saldo_akhir;
								$content_value['saldo_akhir'] = $saldo_akhir_inventory;
								$content_value['reff_id'] = $data['id'];
								$content_value["created_by"] = $this->session_user_id;
								$content_value["created_on"] = date('Y-m-d H:i:s');
								$this->trans_inventory_barang_model->setValueList($content_value);		
								$this->trans_inventory_barang_model->insertHeader();
								$inventory_id = $this->db->insert_id();
								if ($this->db->trans_status() === FALSE){ break; }

								$updateAmount = intval($jumlah_masuk) - intval($jumlah_keluar);
								$this->trans_inventory_barang_model->updateSaldoPerTanggal($updateAmount, $tanggal, $inventory_id, $data['barang'][$x]);
								if ($this->db->trans_status() === FALSE){ break; }
							}
						}
						if ($this->db->trans_status() === FALSE){ break; }
					}

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			if($data['action'] == "Finish"){
				redirect($this->link_back."/preview/".$data['id']);
			} else {
				$this->session->set_flashdata("success_message", $this->msg_edit_success);
				redirect($this->link_back);
			}			
		} else {
			$this->edit($data['id'], $data);
		}
	}

	public function pay($id, $data = null)
	{
		$this->module_subtitle = "Payment";
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		$content_detail = $this->model_object->getHeaderArray();	
		foreach($content_detail as $arr_name => $arr_value){
			$data[$arr_name] = $arr_value;
		}

		$data = $this->pay_load_data($data);
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->module_name."/pay";		
		$this->load->view('parts/template',$data);
	}

	public function pay_load_data($data = null) {
		if($data['pembayaran_status'] == STATUS_COMPLETE) {
			redirect($this->link_view.$data['id']);
		}
		if($data['trans_status'] == STATUS_PENDING) {
			redirect($this->link_edit.$data['id']);
		}  

		$data['supplier_list'] = $this->master_supplier_model->getActiveList();
		$data['barang_list'] = $this->master_barang_model->getActiveList();
		$data['bank_list'] = $this->master_rekening_bank_model->getActiveList();

		/*$trans_purchase_order_payment_cond = array('record_status' => STATUS_ACTIVE, 'detail_status' => STATUS_ACTIVE, 'header_id' => $data['id']);
		$this->trans_purchase_order_payment_model->setCond($trans_purchase_order_payment_cond);
		$data['payment_list'] = $this->trans_purchase_order_payment_model->getList();*/

		$data['payment_list'] = $this->trans_purchase_order_payment_model->getPaymentComposite($data['id']);

		if(!isset($data['barang'])){
			$content_detail_list = $this->model_detail_object->getTransDetailCompositeByHeader($data['id']);
			$fillable_value = $this->model_detail_object->getFillableValueList();
			foreach($fillable_value as $fv){
				$data[$fv] = array();
			}
			foreach($content_detail_list as $d){
				foreach($fillable_value as $fv){
					$data[$fv][] = $d[$fv];
				}
				$data['text_kode_barang'][] = $d['text_kode_barang'];
				$data['text_nama_barang'][] = $d['text_nama_barang'];
			}
		}

		return $data;
	}

	public function pay_validation_data($result, $data) {
		if($result['validation']) {
			$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
			$this->model_object->setCond($content_cond);	
			$trans_purchase_order_header_value = $this->model_object->getHeaderArray();
			$sisa = $trans_purchase_order_header_value['sisa'];
			if(intval($sisa) < intval($data['jumlah_bayar'])){
				$result['validation'] = false;
				$result['message'] = "Jumlah bayar melebihi total";
			}
		}	
		return $result;
	}

	public function pay_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		if($result['validation']) {
			$result = $this->pay_validation_data($result, $data);
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
					$this->model_object->setCond($content_cond);	
					$trans_purchase_order_header_value = $this->model_object->getHeaderArray();
					$trans_purchase_order_header_value['sisa'] = intval($trans_purchase_order_header_value['sisa']) - intval($data['jumlah_bayar']);
					if(intval($trans_purchase_order_header_value['sisa']) == 0){
						$trans_purchase_order_header_value['pembayaran_status'] = STATUS_COMPLETE;						
					}
					$trans_purchase_order_header_value["modified_by"] = $this->session_user_id;
					$trans_purchase_order_header_value["modified_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($trans_purchase_order_header_value);		
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$rekening_bank_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['rekening_bank']);
					$this->master_rekening_bank_model->setCond($rekening_bank_cond);	
					$rekening_bank_value = $this->master_rekening_bank_model->getHeaderArray();
					$rekening_bank_value["saldo"] = intval($rekening_bank_value['saldo']) - intval($data['jumlah_bayar']);
					$rekening_bank_value["modified_by"] = $this->session_user_id;
					$rekening_bank_value["modified_on"] = date('Y-m-d H:i:s');
					$this->master_rekening_bank_model->setValueList($rekening_bank_value);		
					$this->master_rekening_bank_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$saldo_awal_bank = $this->trans_cash_bank_model->getSaldoBankPerTanggal($data['tanggal_bayar'].' '.date('H:i:s'), $data['rekening_bank']);
					$saldo_akhir_bank = intval($saldo_awal_bank) - intval($data['jumlah_bayar']);
					

					$year = intval(date("Y",strtotime($data['tanggal_bayar'])));
					$month = intval(date("m",strtotime($data['tanggal_bayar'])));
					$next_id = $this->trans_cash_bank_model->nextDynamicTrans($year, $month);
					//$next_id = $this->trans_cash_bank_model->nextTrans();
					$fillable_value = $this->trans_cash_bank_model->getFillableValueList();
					$cash_bank_value = $this->trans_cash_bank_model->getValueList();
					$prefix = $this->trans_cash_bank_model->getPrefix();
					//$no_transaksi = $prefix.date('Ym').sprintf("%05d", $next_id);
					$no_transaksi = date('ym').$rekening_bank_value['kode'].sprintf("%05d", $next_id);
					$cash_bank_value['no_transaksi'] = $no_transaksi;
					$cash_bank_value['tanggal'] = $data['tanggal_bayar'];
					//$cash_bank_value['tanggal'] = date('Y-m-d H:i:s');
					$cash_bank_value['bank'] = $data['rekening_bank'];
					$cash_bank_value['type'] = 0;
					$cash_bank_value['kredit'] = $data['jumlah_bayar'];
					$cash_bank_value['debit'] = 0;
					//$cash_bank_value['saldo_awal'] = $this->trans_cash_bank_model->getSaldo();
					$cash_bank_value['saldo_awal'] = $this->trans_cash_bank_model->getSaldoPerTanggal($data['tanggal_bayar'].' '.date('H:i:s'));
					$cash_bank_value['saldo_akhir'] = intval($cash_bank_value['saldo_awal']) - intval($cash_bank_value['kredit']) + intval($cash_bank_value['debit']);
					$cash_bank_value['saldo_bank'] = $saldo_akhir_bank;
					$cash_bank_value['note'] = 'Pembayaran Untuk PO '.$trans_purchase_order_header_value['no_transaksi'];
					$cash_bank_value['reff_id'] = $trans_purchase_order_header_value['id'];
					$cash_bank_value['trans_status'] = STATUS_COMPLETE;
					$cash_bank_value["created_by"] = $this->session_user_id;
					$cash_bank_value["created_on"] = date('Y-m-d H:i:s');
					$this->trans_cash_bank_model->setValueList($cash_bank_value);		
					$this->trans_cash_bank_model->insertHeader();
					$cash_bank_id = $this->db->insert_id();
					if ($this->db->trans_status() === FALSE){ break; }

					$updateAmount = intval($cash_bank_value['debit']) - intval($cash_bank_value['kredit']);
					$this->trans_cash_bank_model->updateCashBank($updateAmount, $data['tanggal_bayar'].' '.date('H:i:s'), $cash_bank_id);
					if ($this->db->trans_status() === FALSE){ break; }

					$updateAmountBank = intval($cash_bank_value['debit']) - intval($cash_bank_value['kredit']);
					$this->trans_cash_bank_model->updateBankCashBank($updateAmountBank, $data['tanggal_bayar'].' '.date('H:i:s'), $cash_bank_id, $data['rekening_bank']);
					if ($this->db->trans_status() === FALSE){ break; }

					$payment_value = $this->trans_purchase_order_payment_model->getValueList();
					$payment_value['header_id'] = $data['id'];
					$payment_value['bank'] = $data['rekening_bank'];
					$payment_value['jumlah_bayar'] = $data['jumlah_bayar'];
					$payment_value["tanggal_bayar"] = $data['tanggal_bayar'];
					$payment_value['note'] = $data['note'];
					$payment_value["created_by"] = $this->session_user_id;
					$payment_value["created_on"] = date('Y-m-d H:i:s');
					$this->trans_purchase_order_payment_model->setValueList($payment_value);		
					$this->trans_purchase_order_payment_model->insertHeader();
					$content_id = $this->db->insert_id();
					if ($this->db->trans_status() === FALSE){ break; }

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_edit_success);
			redirect($this->link_back);
		} else {
			$this->pay($data['id'], $data);
		}
	}

	public function cancel_process($id)
	{
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		if(!$this->model_object->checkExist()){
			$result['validation'] = false;
			$result['message'] = "Invalid Content Id.";
		}
		if($result['validation']){
			$content_cond = array('record_status' => STATUS_DELETE, 'id' => $id);
			$this->model_object->setCond($content_cond);
			if($this->model_object->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Content has been deleted previously.";
			}
		}
		if($result['validation']){
			$content_cond = array('trans_status' => STATUS_PENDING, 'id' => $id);
			$this->model_object->setCond($content_cond);
			if($this->model_object->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Content has been cancel previously.";
			}
		}
		if($result['validation']){
			$trans_purchase_order_payment_cond = array('header_id' => $id);
			$this->trans_purchase_order_payment_model->setCond($trans_purchase_order_payment_cond);
			if($this->trans_purchase_order_payment_model->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Content has been paid.";
			}
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/	
		if($result['validation']){
			try {
				$this->db->trans_begin();
				while(true) {
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
					$this->model_object->setCond($content_cond);	
					$content_value = $this->model_object->getHeaderArray();
					$content_value["trans_status"] = STATUS_PENDING;
					$content_value["modified_by"] = $this->session_user_id;
					$content_value["modified_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$reff_count = $this->trans_inventory_barang_model->getCountReffId($id, 1);
					$second_added = intval($reff_count) * 30;
					$tanggal = $content_value['tanggal'];
					$dTanggal = new DateTime($tanggal);
					$dTanggal->add(new DateInterval('PT'.$second_added.'S'));
					$tanggal = $dTanggal->format('Y-m-d H:i:s');

					$no_transaksi = $content_value['no_transaksi'];
					$content_detail_list = $this->model_detail_object->getTransDetailCompositeByHeader($id);
					foreach($content_detail_list as $d){
						$barang = $d['barang'];
						$gudang = 0;
						$saldo_awal = $this->master_stock_barang_model->saldoAwal($barang, $gudang);
						$initial_saldo = FALSE;
						if($saldo_awal == NULL){
							$initial_saldo = TRUE;
							$saldo_awal = 0;
						}
						$jumlah_masuk = 0;
						$jumlah_keluar = $d['quantity'];
						$saldo_akhir = intval($saldo_awal) + intval($jumlah_masuk) - intval($jumlah_keluar);
						$content_value = $this->master_stock_barang_model->getValueList();
						$content_value['id_barang'] = $barang;
						$content_value['id_gudang'] = $gudang;
						$content_value['jumlah'] = $saldo_akhir;
						$this->master_stock_barang_model->setValueList($content_value);		
						if($initial_saldo){
							$this->master_stock_barang_model->insertHeader();
						} else {
							$value_condition = array('id_barang' => $barang , 'id_gudang' => $gudang);
							$this->master_stock_barang_model->setCond($value_condition);
							$this->master_stock_barang_model->updateHeader();
						}
						if ($this->db->trans_status() === FALSE){ break; }

						$saldo_awal_inventory = $this->trans_inventory_barang_model->getSaldoPerTanggal($tanggal, $barang);
						$saldo_akhir_inventory = intval($saldo_awal_inventory) + intval($jumlah_masuk) - intval($jumlah_keluar);
						$content_value = $this->trans_inventory_barang_model->getValueList();
						$content_value['id_barang'] = $barang;
						$content_value['id_gudang'] = $gudang;
						$content_value['id_user'] = $this->session_user_id;
						$content_value['trans_date'] = $tanggal;
						$content_value['comment'] = "-".$no_transaksi;
						$content_value['saldo_awal'] = $saldo_awal_inventory;
						$content_value['jumlah_masuk'] = $jumlah_masuk;
						$content_value['jumlah_keluar'] = $jumlah_keluar;
						$content_value['saldo_akhir'] = $saldo_akhir_inventory;
						$content_value["created_by"] = $this->session_user_id;
						$content_value["created_on"] = date('Y-m-d H:i:s');
						$content_value["trans_status"] = STATUS_CANCEL;
						$content_value['reff_id'] = $id;
						$this->trans_inventory_barang_model->setValueList($content_value);		
						$this->trans_inventory_barang_model->insertHeader();
						$inventory_id = $this->db->insert_id();
						if ($this->db->trans_status() === FALSE){ break; }

						$updateAmount = intval($jumlah_masuk) - intval($jumlah_keluar);
						$this->trans_inventory_barang_model->updateSaldoPerTanggal($updateAmount, $tanggal, $inventory_id, $barang);
						if ($this->db->trans_status() === FALSE){ break; }

						$this->trans_inventory_barang_model->cancelReffId($id, 1);
						if ($this->db->trans_status() === FALSE){ break; }
					}

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		/*********Process ends here ***********/	
		if($result['validation']){
			$this->session->set_flashdata("success_message", "Success");
			redirect(base_url().$this->link_back);
		} else {
			$this->index($result);
		}
	}

	public function cancel_payment_process($id)
	{
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		if(!$this->model_object->checkExist()){
			$result['validation'] = false;
			$result['message'] = "Invalid Content Id.";
		}
		if($result['validation']){
			$content_cond = array('record_status' => STATUS_DELETE, 'id' => $id);
			$this->model_object->setCond($content_cond);
			if($this->model_object->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Content has been deleted previously.";
			}
		}
		if($result['validation']){
			$trans_purchase_order_payment_cond = array('header_id' => $id, 'detail_status' => STATUS_ACTIVE);
			$this->trans_purchase_order_payment_model->setCond($trans_purchase_order_payment_cond);
			if(!$this->trans_purchase_order_payment_model->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Content has not been paid.";
			}
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/	
		if($result['validation']){
			try {
				$this->db->trans_begin();
				while(true) {
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
					$this->model_object->setCond($content_cond);	
					$content_value = $this->model_object->getHeaderArray();
					$content_value["pembayaran_status"] = STATUS_PENDING;
					$content_value["sisa"] = $content_value["total"];
					$content_value["modified_by"] = $this->session_user_id;
					$content_value["modified_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$this->trans_purchase_order_payment_model->cancelPayment($id);

					$cash_bank_cond = array('reff_id' => $id, 'trans_status' => STATUS_COMPLETE);
					$this->trans_cash_bank_model->setCond($cash_bank_cond);	
					$cash_bank_list = $this->trans_cash_bank_model->getListArray();

					foreach($cash_bank_list as $c){
						$cash_bank_cond = array('record_status' => STATUS_ACTIVE, 'id' => $c['id']);
						$this->trans_cash_bank_model->setCond($cash_bank_cond);	
						$cash_bank_value = $this->trans_cash_bank_model->getHeaderArray();
						$cash_bank_value["trans_status"] = STATUS_CANCEL;
						$cash_bank_value["modified_by"] = $this->session_user_id;
						$cash_bank_value["modified_on"] = date('Y-m-d H:i:s');
						$this->trans_cash_bank_model->setValueList($cash_bank_value);		
						$this->trans_cash_bank_model->updateHeader();
						if ($this->db->trans_status() === FALSE){ break; }

						$jenis = "debit";
						$jumlah = 0;
						if($cash_bank_value['debit'] != 0){
							$jenis = "debit";
							$jumlah = $cash_bank_value['debit'];
						} else if($cash_bank_value['kredit'] != 0){
							$jenis = "kredit";
							$jumlah = $cash_bank_value['kredit'];
						}

						$rekening_bank_cond = array('record_status' => STATUS_ACTIVE, 'id' => $cash_bank_value['bank']);
						$this->master_rekening_bank_model->setCond($rekening_bank_cond);	
						$rekening_bank_value = $this->master_rekening_bank_model->getHeaderArray();

						if($jenis == "debit"){
							$rekening_bank_value["saldo"] = intval($rekening_bank_value['saldo']) - intval($jumlah);
						} else if($jenis == "kredit"){
							$rekening_bank_value["saldo"] = intval($rekening_bank_value['saldo']) + intval($jumlah);
						}
						$rekening_bank_value["modified_by"] = $this->session_user_id;
						$rekening_bank_value["modified_on"] = date('Y-m-d H:i:s');
						$this->master_rekening_bank_model->setValueList($rekening_bank_value);		
						$this->master_rekening_bank_model->updateHeader();
						if ($this->db->trans_status() === FALSE){ break; }

						$second_added = 30;
						$tanggal = $cash_bank_value['tanggal'];
						$dTanggal = new DateTime($tanggal);
						$dTanggal->add(new DateInterval('PT'.$second_added.'S'));
						$tanggal = $dTanggal->format('Y-m-d H:i:s');

						$saldo_awal_bank = $this->trans_cash_bank_model->getSaldoBankPerTanggal($tanggal, $cash_bank_value['bank']);
						$saldo_akhir_bank = intval($saldo_awal_bank) - intval($cash_bank_value['debit']) + intval($cash_bank_value['kredit']);

						$data['saldo_awal'] = $this->trans_cash_bank_model->getSaldoPerTanggal($tanggal.' '.date('H:i:s'));
						$data['saldo_akhir'] = intval($data['saldo_awal']) - intval($cash_bank_value['debit']) + intval($cash_bank_value['kredit']);
						$data['saldo_bank'] = $saldo_akhir_bank;
						$year = intval(date("Y",strtotime($tanggal)));
						$month = intval(date("m",strtotime($tanggal)));
						$next_id = $this->trans_cash_bank_model->nextDynamicTrans($year, $month);
						//$next_id = $this->trans_cash_bank_model->nextTrans();
						$fillable_value = $this->trans_cash_bank_model->getFillableValueList();
						$this->trans_cash_bank_model->refreshValueList();
						$content_value = $this->trans_cash_bank_model->getValueList();
						$content_value['no_transaksi'] = '';
						//$content_value['tanggal'] = date('Y-m-d H:i:s');
						$content_value['tanggal'] = $tanggal;

						$content_value['bank'] = $cash_bank_value['bank'];
						$content_value['type'] = $cash_bank_value['type'];
						$content_value['kredit'] = $cash_bank_value['debit'];
						$content_value['debit'] = $cash_bank_value['kredit'];
						$content_value['saldo_awal'] = $data['saldo_awal'];
						$content_value['saldo_akhir'] = $data['saldo_akhir'];
						$content_value['saldo_bank'] = $data['saldo_bank'];
						$content_value['note'] = "Cancel ".$cash_bank_value['note'];
						$content_value['cek_giro'] = $cash_bank_value['cek_giro'];
						$content_value['trans_id'] = $c['id'];
						
						$content_value["created_by"] = $this->session_user_id;
						$content_value["created_on"] = date('Y-m-d H:i:s');
						$this->trans_cash_bank_model->setValueList($content_value);		
						$this->trans_cash_bank_model->insertHeader();
						$content_id = $this->db->insert_id();

						//$no_transaksi = $this->trans_prefix.date('Ym').sprintf("%05d", $next_id);
						//$no_transaksi = date('ym').$rekening_bank_value['kode'].sprintf("%05d", $next_id);
						$no_transaksi = date("y",strtotime($tanggal)).date("m",strtotime($tanggal)).$rekening_bank_value['kode'].sprintf("%05d", $next_id);
						$content_value['id'] = $content_id;
						$content_value['no_transaksi'] = $no_transaksi;
						$content_value['trans_status'] = STATUS_CANCEL;
						$this->trans_cash_bank_model->setValueList($content_value);		
						$value_condition = array('id' => $content_id);
						$this->trans_cash_bank_model->setCond($value_condition);
						$this->trans_cash_bank_model->updateHeader();
						if ($this->db->trans_status() === FALSE){ break; }

						$updateAmount = intval($cash_bank_value['kredit']) - intval($cash_bank_value['debit']);
						$this->trans_cash_bank_model->updateCashBank($updateAmount, $tanggal, $content_id);
						if ($this->db->trans_status() === FALSE){ break; }

						$updateAmountBank = intval($cash_bank_value['kredit']) - intval($cash_bank_value['debit']);
						$this->trans_cash_bank_model->updateBankCashBank($updateAmountBank, $tanggal, $content_id, $cash_bank_value['bank']);
						if ($this->db->trans_status() === FALSE){ break; }
					}

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		/*********Process ends here ***********/	
		if($result['validation']){
			$this->session->set_flashdata("success_message", "Success");
			redirect(base_url().$this->link_back);
		} else {
			$this->index($result);
		}
	}

	public function preview($id, $data = null)
	{
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		$content_detail = $this->model_object->getHeaderArray();	

		$core_app_config_cond = array('config_key' => 'COMPANY_NAME');
		$this->core_app_config_model->setCond($core_app_config_cond);
		$company_name =  $this->core_app_config_model->getHeaderField("config_value");

		$suppler_cond = array('record_status' => STATUS_ACTIVE, 'id' => $content_detail['supplier']);
		$this->master_supplier_model->setCond($suppler_cond);
		$supplier_detail = $this->master_supplier_model->getHeaderArray();	

		$content_detail_list = $this->model_detail_object->getTransDetailCompositeByHeader($id);

		$this->load->library('datetime_library');
		$this->load->library('pdf');	
		$this->pdf->fontpath = 'assets/fonts/pdf/'; 
		
		$this->pdf->AddFont('Calibri');
		$this->pdf->AddFont('Calibri-Bold','','calibrib.php');
		$this->pdf->AliasNbPages();
		$this->pdf->Open();
		$this->pdf->SetAutoPageBreak(false);
		
		$this->pdf->AddPage();
		
		$this->pdf->SetFont('Calibri-Bold','',16);
		$x1 = 9;
		$y1 = 15;
		$w = 190;
		$h = 10; 
		$border = 1; 
		$lineBreak = 1; 
		$text = "Purchase Order";
		$align = "C"; 
		$this->pdf->SetXY ($x1,$y1);
		$this->pdf->Cell($w,$h,$text,$border,$lineBreak,$align);
			
		$name = $company_name;
		$x1 = 10;
		$y1 = 28;
		$this->pdf->SetXY ($x1,$y1);			
		$len = $this->pdf->GetStringWidth( $name );//the width
		$h = 2;			
		$this->pdf->SetFont('Calibri-Bold','',11);
		$this->pdf->Cell( $len, $h, $name);
		
		/*$x1 = 10;
		$y1 = 26;
		$w1 = 178;
		$h1 = 93;
		$logo = base_url()."/assets/images/logo-report.png";
		$this->pdf->Cell( $w1, $h1, $this->pdf->Image($logo, $x1, $y1, 22), 0, 0, 'L', false );*/


		$x1 = 10;
		$y1 = 40;
		$this->pdf->SetXY ($x1,$y1);
		$w = 30;
		$h=5;
		$this->pdf->SetFont('Calibri','',11);
		$this->pdf->Cell ($w,$h,'Supplier',1,'','L');
		$this->pdf->Cell($w+30,$h,$supplier_detail['nama'],1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h);
		$this->pdf->Cell ($w,$h,'Ship To',1,'','L');
		$this->pdf->Cell($w+30,$h,$content_detail['ship_to'],1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h+$h);
		$this->pdf->Cell ($w,$h,'Ship Via',1,'','L');
		$this->pdf->Cell($w+30,$h,$content_detail['ship_via'],1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h+$h+$h);
		$this->pdf->Cell ($w,$h,'Terms',1,'','L');
		$this->pdf->Cell($w+30,$h,$content_detail['terms'],1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h+$h+$h+$h);
		$this->pdf->Cell ($w,$h,'FOB',1,'','L');
		$this->pdf->Cell($w+30,$h,$content_detail['fob'],1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h+$h+$h+$h+$h);
		$this->pdf->Cell ($w,$h,'Expor',1,'','L');
		$this->pdf->Cell($w+30,$h,($content_detail['expor']==1)?'Yes':'No',1,'','L');

		$sNo = $content_detail['no_transaksi'];
		$sDate = $this->datetime_library->indonesian_date($this->datetime_library->date_format($content_detail['tanggal'], 'l jS F Y'), 'j F Y', '');
		$sExpectedDate = $this->datetime_library->indonesian_date($this->datetime_library->date_format($content_detail['expected_date'], 'l jS F Y'), 'j F Y', '');
		$x1 = 132;
		$y1 = 40;
		$this->pdf->SetXY ($x1,$y1);
		$w = 33;
		$h=5;
		$this->pdf->SetFont('Calibri','',11);
		$this->pdf->Cell ($w,$h,'Nomor',1,'','L');
		$this->pdf->Cell($w+1,$h,$sNo,1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h);
		$this->pdf->Cell ($w,$h,'Tanggal',1,'','L');
		$this->pdf->Cell($w+1,$h,$sDate,1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h+$h);
		$this->pdf->Cell ($w,$h,'Expected Date',1,'','L');
		$this->pdf->Cell($w+1,$h,$sExpectedDate,1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h+$h+$h);
		$this->pdf->Cell ($w,$h,'Faktur Pajak',1,'','L');
		$this->pdf->Cell($w+1,$h,$content_detail['faktur_pajak'],1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h+$h+$h+$h);
		$this->pdf->Cell ($w,$h,'Taxable Vendor',1,'','L');
		$this->pdf->Cell($w+1,$h,($content_detail['taxable_vendor']==1)?'Yes':'No',1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h+$h+$h+$h+$h);
		$this->pdf->Cell ($w,$h,'Rate',1,'','L');
		$this->pdf->Cell($w+1,$h,$content_detail['rate'],1,'','L');
		
		$x1 = 10;
		$y1 = 90;
		$this->pdf->SetXY ($x1,$y1);
		$this->pdf->SetFont('Calibri-Bold','',11);
		
		//$this->pdf->SetTextColor(255,255,255); 
		//$this->pdf->SetFillColor(128,128,128); 

		$this->pdf->SetTextColor(0,0,0); 
		$this->pdf->SetFillColor(255,255,255); 
		$this->pdf->Cell(9, 5, 'No', 1, '', 'C',true);
		//$this->pdf->Cell(40, 5, 'Item', 1, '', 'C', true);
		$this->pdf->Cell(115, 5, 'Description', 1, '', 'L', true);
		$this->pdf->Cell(15, 5, 'Qty', 1, '', 'C', true);
		$this->pdf->Cell(25, 5, 'Unit Price', 1, '', 'R', true);
		$this->pdf->Cell(25, 5, 'Total', 1, '', 'R', true);
		$this->pdf->Ln();
		$this->pdf->SetTextColor(0,0,0); 
		$this->pdf->SetFont('Calibri','',11);
		$num = 1;
		foreach($content_detail_list as $valueBarang){
			$PartNo = $valueBarang['text_kode_barang'];
			$NamaBarang = $valueBarang['text_nama_barang'];
			$Qty = $valueBarang['quantity'];
			$HargaUnit = number_format($valueBarang['harga_unit'],0,'.',',');
			$Jumlah = number_format($valueBarang['jumlah'],0,'.',',');
			$this->pdf->Cell(9, 5, $num, 1, '', 'C');
			//$this->pdf->Cell(40, 5, substr($PartNo, 0, 16), 1, '', 'C');
			$this->pdf->Cell(115, 5, substr($NamaBarang, 0, 80), 1, '', 'L');
			$this->pdf->Cell(15, 5, $Qty, 1, '', 'C');
			$this->pdf->Cell(25, 5, $HargaUnit, 1, '', 'R');
			$this->pdf->Cell(25, 5, $Jumlah, 1, '', 'R');
			$this->pdf->Ln();	
			$num++;
		}	
		for ($x = $num; $x <= 10; $x++) {
		$this->pdf->Cell(9, 5, $x, 1, '', 'C');
		//$this->pdf->Cell(40, 5, '', 1, '', 'C');
		$this->pdf->Cell(115, 5, '', 1, '', 'C');
		$this->pdf->Cell(15, 5, '', 1, '', 'C');
		$this->pdf->Cell(25, 5, '', 1, '', 'C');
		$this->pdf->Cell(25, 5, '', 1, '', 'C');
		$this->pdf->Ln();	
		}

		$grandTotal = number_format($content_detail['total'],0,'.',',');
		$this->pdf->SetFont('Calibri-Bold','',11);
		$this->pdf->Cell(139);
		$this->pdf->Cell(25, 5, 'Grand Total', 1, '', 'C');
		$this->pdf->Cell(25, 5, (($content_detail['total'])?$grandTotal:'0'), 1, '', 'R');
		
		$this->pdf->Ln();	
		$this->pdf->Ln();	
		$this->pdf->SetFont('Calibri','',11);
		
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Cell(48, 5, 'Prepared By', '', '', 'C');
		$this->pdf->Cell(48, 5, 'Approved By', '', '', 'C');
		
		$this->pdf->Ln();
		$this->pdf->Ln();
		$this->pdf->Ln();
		
		$x1=19;
		$y1=180;
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		//$this->pdf->Line($x1,$y1,$x1+30,$y1);
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		//$this->pdf->Line($x1+48,$y1,$x1+48+30,$y1);
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Line($x1+48+48,$y1,$x1+48+48+30,$y1);
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Line($x1+48+48+48,$y1,$x1+48+48+48+30,$y1);

		$x1 = 10;
		$y1 = 163;
		$w = 80;
		$h = 10; 
		$border = 1; 
		$lineBreak = 1; 
		$text = "NB: Untuk Sementara Faktur Akan Menyusul";
		$align = "C"; 
		$this->pdf->SetXY ($x1,$y1);
		$this->pdf->Cell($w,$h,$text,$border,$lineBreak,$align);
		
		$this->pdf->Output();
	}
}