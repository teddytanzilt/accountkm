<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mbarang extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('master_barang_model');
		$this->load->model('master_satuan_model');
		
		$this->module_name = "mbarang";
		$this->module_title = "Master Barang";
		$this->table_name = "master_barang";
		$this->model_object = $this->master_barang_model;
		$this->validation_object = array();
		
		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";
		
		$this->msg_add_success = "Create Barang Success.";
		$this->msg_edit_success = "Edit Barang Success.";
		$this->msg_delete_success = "Delete Barang Success.";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("BARANG", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}

	public function create_load_data($data = null) {
		$data['satuan_list'] = $this->master_satuan_model->getActiveList();
		return $data;
	}
	
	public function edit_load_data($data = null) {
		$data['satuan_list'] = $this->master_satuan_model->getActiveList();
		return $data;
	}

	public function get_data_list_active_json(){	
		$request_data= $_REQUEST;
		$column = $this->model_object->getDatatableShowValueList();
		$total_data = $this->model_object->getCountDataActive();
		$total_filtered = $this->model_object->getDataTableCountActive($request_data);
		$data_filtered = $this->model_object->getDataTableDataActive($request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}
	
	function ajax_barang_info($harga_jual=0){
		$id = $this->input->post('id');
		header('Content-type: application/json');
		$barang_cond = array('id' => $id, 'record_status' => STATUS_ACTIVE);
		$this->master_barang_model->setCond($barang_cond);			
		$barang_value = $this->master_barang_model->getHeaderArray();

		if($harga_jual==1){
			$array = array('kode' => $barang_value['kode'], 'nama' => $barang_value['nama'], 'harga' => $barang_value['harga_jual'] );
		} else {
			$array = array('kode' => $barang_value['kode'], 'nama' => $barang_value['nama'], 'harga' => $barang_value['harga_modal'] );
		}
		echo json_encode($array);
	}
}