<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rlabarugi extends Report_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('trans_cash_bank_model');
		$this->load->model('master_type_cash_bank_model');

		$this->module_name = "rlabarugi";
		$this->module_title = "Laporan Laba Rugi";
		
		$this->model_object = $this->trans_cash_bank_model;
		
		$this->view_report = $this->module_name."/report";
		
		$this->report_title = "Laporan Laba Rugi";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("RLABARUGI", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}

	public function index($data = null)
	{	
		$this->module_subtitle = "Report";
		$data['title'] = $this->web_name.' | '.$this->module_title;
		$data['content'] = $this->view_report;		
		$this->load->view('parts/template',$data);
	}

	public function generate_report($data = null)
	{
		$data = $this->common_library->getData();

		$this->load->library('datetime_library');
		$this->load->library('pdf');	
		
		$marginX = 12;
		$marginY = 12;
		$paperW = 210; 
		$paperH = 297; 
		
		$this->pdf->fontpath = 'assets/fonts/pdf/'; 
		$this->pdf->AddFont('Calibri');
		$this->pdf->AddFont('Calibri-Bold','','calibrib.php');
		$this->pdf->AliasNbPages();
		$this->pdf->Open();
		$this->pdf->SetAutoPageBreak(true, '10');

		$last_date = getLastDate($data['tahun_ke'], $data['bulan_ke']);
		$data['tanggal_dari'] = $data['tahun_dari']."-".$data['bulan_dari']."-01";
		$data['tanggal_ke']	= $data['tahun_ke']."-".$data['bulan_ke']."-".$last_date;

		//$report_data = $this->model_object->getBukuBesar($data['tanggal_dari'], $data['tanggal_ke']);

		$total_penjualan_export = $this->model_object->getTotalPenjualan($data['tanggal_dari'], $data['tanggal_ke'], 1);
		$total_penjualan_lokal = $this->model_object->getTotalPenjualan($data['tanggal_dari'], $data['tanggal_ke'],0);
		$total_penjualan = intval($total_penjualan_lokal) + intval($total_penjualan_export);

		$saldo_awal = $this->model_object->getSaldoAwalRange($data['tanggal_dari'], $data['tanggal_ke']);
		$saldo_akhir = $this->model_object->getSaldoAkhirRange($data['tanggal_dari'], $data['tanggal_ke']);
		
		$total_pembelian = $this->model_object->getTotalPembelian($data['tanggal_dari'], $data['tanggal_ke'],0);
		$total_persediaan = intval($saldo_awal) + intval($total_pembelian);
		$harga_pokok_penjualan = intval($total_persediaan) - intval($saldo_akhir);

		$laba_kotor_usaha = intval($total_penjualan) - intval($harga_pokok_penjualan);

		$total_biaya_operasi = 0;
		$laba_bersih_usaha = 0;

		$laba_selisih_kurs = 0;
		$jasa_giro = 0;
		$pajak_jasa_giro = 0;
		$total_pendapatan_lain_lain = intval($laba_selisih_kurs) + intval($jasa_giro) - intval($pajak_jasa_giro);

		$laba_usaha_sebelum_fiskal = 0;

		$total_pendapatan_operasi = 0;

		//$tipe_kasbon_list = $this->master_type_cash_bank_model->getActiveList();
		$list_biaya_operasi = $this->trans_cash_bank_model->getDataKreditPerType('', '');
		$list_pendapatan_operasi = $this->trans_cash_bank_model->getDataDebitPerType('', '');

		$this->generate_report_header($this->pdf, $data);
		
		$no = 1;
		$fontSize = 8;
		$fontSize2 = 6;
		$titleFontSize = 9;

		$this->pdf->SetFont('Calibri-Bold','',$fontSize);
		$this->pdf->Cell(70, 5, 'Penjualan Ekspor (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, number_format($total_penjualan_export,0,'.',','), 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','', $fontSize);
		$this->pdf->Cell(70, 5, 'Penjualan Lokal (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, number_format($total_penjualan_lokal,0,'.',','), 'B', 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','', $fontSize);
		$this->pdf->Cell(70, 5, '', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, number_format($total_penjualan,0,'.',','), 0, 1, 'R', true);

		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','U', $titleFontSize);
		$this->pdf->Cell(190, 5, 'Harga Pokok Produksi', 0, 1, 'L', true);

		$this->pdf->SetFont('Calibri-Bold','', $fontSize);
		$this->pdf->Cell(70, 5, 'Persediaan Awal (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, number_format($saldo_awal,0,'.',','), 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','', $fontSize);
		$this->pdf->Cell(70, 5, 'Pembelian (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, number_format($total_pembelian,0,'.',','), 'B', 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->SetFont('Calibri-Bold','',$fontSize);
		$this->pdf->Cell(40, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','', $fontSize);
		$this->pdf->Cell(70, 5, 'Barang yang tersedia u/dijual (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, number_format($total_persediaan,0,'.',','), 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','', $fontSize);
		$this->pdf->Cell(70, 5, 'Persediaan Akhir (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, number_format($saldo_akhir,0,'.',','), 'B', 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 1, 'R', true);
		
		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(70, 5, 'Harga Pokok Produksi (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, number_format($harga_pokok_penjualan,0,'.',','), 'B', 1, 'R', true);

		$this->pdf->Cell(70, 5, 'Laba Kotor Usaha (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, number_format($laba_kotor_usaha,0,'.',','), 0, 1, 'R', true);

		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','U', $titleFontSize);
		$this->pdf->Cell(70, 5, 'Biaya Operasional', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','',$fontSize);
		foreach($list_biaya_operasi as $tkl) {
			$this->pdf->Cell(70, 5, (($tkl['type_cash_bank']=="")?"Lain Lain ":$tkl['type_cash_bank']). "(Rp)", 0, 0, 'L', true);
			$this->pdf->Cell(40, 5, number_format($tkl['total_kredit'],0,'.',','), 0, 0, 'R', true);
			$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
			$this->pdf->Cell(40, 5, '', 0, 1, 'R', true);

			$total_biaya_operasi = intval($total_biaya_operasi) + intval($tkl['total_kredit']);
		}
		$laba_bersih_usaha = intval($laba_kotor_usaha) - intval($total_biaya_operasi); 

		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(70, 5, 'Jumlah Biaya Operasi (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, number_format($total_biaya_operasi,0,'.',','), 'B', 1, 'R', true);

		$this->pdf->Cell(70, 5, 'Laba Bersih Usaha Komersil (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, number_format($laba_bersih_usaha,0,'.',','), 0, 1, 'R', true);

		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','U', $titleFontSize);
		$this->pdf->Cell(70, 5, 'Pendapatan Lain-lain', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','',$fontSize);
		foreach($list_pendapatan_operasi as $tkl) {
			$this->pdf->Cell(70, 5, (($tkl['type_cash_bank']=="")?"Lain Lain ":$tkl['type_cash_bank']). "(Rp)", 0, 0, 'L', true);
			$this->pdf->Cell(40, 5, number_format($tkl['total_debit'],0,'.',','), 0, 0, 'R', true);
			$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
			$this->pdf->Cell(40, 5, '', 0, 1, 'R', true);

			$total_pendapatan_operasi = intval($total_pendapatan_operasi) + intval($tkl['total_debit']);
		}

		$laba_usaha_sebelum_fiskal = intval($laba_bersih_usaha) + intval($total_pendapatan_operasi);

		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(70, 5, 'Jumlah Pendapatan Lain Lain (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, number_format($total_pendapatan_operasi,0,'.',','), 'B', 1, 'R', true);

		/*$this->pdf->SetFont('Calibri-Bold','U', $fontSize);
		$this->pdf->Cell(70, 5, 'Pendapatan Lain-lain', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(45, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','',$fontSize);

		$this->pdf->Cell(70, 5, "Laba Selisih Kurs (Rp)", 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, number_format($laba_selisih_kurs,0,'.',','), 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(70, 5, "Jasa Giro (Rp)", 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, number_format($jasa_giro,0,'.',','), 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 1, 'R', true);
		
		$this->pdf->Cell(70, 5, "Pajak Jasa Giro (Rp)", 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, number_format($pajak_jasa_giro,0,'.',','), 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(70, 5, 'Jumlah Pendapatan Lain-lain (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, number_format($total_pendapatan_lain_lain,0,'.',','), 0, 1, 'R', true);
		*/

		$this->pdf->Cell(70, 5, 'Laba Usaha Sebelum Koreksi Fiskal (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, '', 0, 0, 'R', true);
		$this->pdf->Cell(40, 5, number_format($laba_usaha_sebelum_fiskal,0,'.',','), 0, 1, 'R', true);

		
		
		$this->pdf->Ln(5);
		$this->pdf->Output();
	}
	
	public function generate_report_header($obj, $data)
	{
		$obj->AddPage();		
		
		$title = $this->report_title;
		$titleFontSize = 18;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(0, 0,strtoupper($title), 0, 0, 'C');
		$obj->Ln(8);

		/*$company_name = "";
		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'PT', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$company_name, 0, 1, 'L');	*/

		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'Periode', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$data['tanggal_dari'].' - '.$data['tanggal_ke'], 0, 1, 'L');	
		
		$obj->Ln(5);

		$titleFontSize = 9;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->SetFillColor(200,200,200);

		//$obj->Cell(100, 5, '', 1, 0, 'C', true);
		//$obj->Cell(45, 5, '', 1, 0, 'C', true);
		//$obj->Cell(45, 5, '', 1, 1, 'R', true);

		$obj->SetFillColor(255,255,255);
		$obj->SetFont('Calibri','',$fontSize);


	}



	public function excel($data = null)
	{	


		$list_biaya_operasi = $this->trans_cash_bank_model->getDataKreditPerType('', '');


		

      	$this->load->library("excel");
		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);

/*
		$table_columns = array("type_cash_bank", "total_kredit");

		$column = 0;

		foreach($table_columns as $field){

		$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);

		$column++;

		}

		$excel_row = 2;

		foreach($list_biaya_operasi as $tkl){

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $tkl['type_cash_bank']);

		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $tkl['total_kredit']);

		$excel_row++;

		}
*/

		$horizontal_center = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $horizontal_left = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	        )
	    );
	    $horizontal_right = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	        )
	    );

		
		$object->getActiveSheet()->getColumnDimension('A')->setWidth(40);
		$object->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$object->getActiveSheet()->getColumnDimension('C')->setWidth(30);

		$excel_row = 1;
		
		$object->getActiveSheet()->mergeCells("A".$excel_row.":C".$excel_row."");
		$object->getActiveSheet()->getStyle("A".$excel_row.":C".$excel_row."")->applyFromArray($horizontal_center);
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "LAPORAN LABA RUGI");
		$excel_row++;

		$object->getActiveSheet()->mergeCells("A".$excel_row.":C".$excel_row."");
		$object->getActiveSheet()->getStyle("A".$excel_row.":C".$excel_row."")->applyFromArray($horizontal_left);
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Periode : 2018-01-01 - 2019-12-31");
		$excel_row++;

		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Penjualan Ekspor (Rp)");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "1.000.000");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Penjualan Local (Rp)");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "1.000.000");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "2.000.000");
		$excel_row++;

		$excel_row++;

		$object->getActiveSheet()->mergeCells("A".$excel_row.":C".$excel_row."");
		$object->getActiveSheet()->getStyle("A".$excel_row.":C".$excel_row."")->applyFromArray($horizontal_left);
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Harga Pokok Produksi");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Persediaan Awal (Rp)");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "1.000.000");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Pembelian");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "1.000.000");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Barang yang tersedia u/dijual (Rp)");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "1.000.000");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Persediaan Akhir (Rp) ");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "1.000.000");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Harga Pokok Produksi (Rp)");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "1.000.000");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Laba Kotor Usaha (Rp)");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "1.000.000");
		$excel_row++;

		$object->getActiveSheet()->mergeCells("A".$excel_row.":C".$excel_row."");
		$object->getActiveSheet()->getStyle("A".$excel_row.":C".$excel_row."")->applyFromArray($horizontal_left);
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Biaya Operasional");
		$excel_row++;


		foreach($list_biaya_operasi as $tkl){
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $tkl['type_cash_bank']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $tkl['total_kredit']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "");
			$excel_row++;
		}

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Jumlah Biaya Operasi (Rp)");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "1.000.000");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Laba Bersih Usaha Komersil (Rp)");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "1.000.000");
		$excel_row++;

		$object->getActiveSheet()->mergeCells("A".$excel_row.":C".$excel_row."");
		$object->getActiveSheet()->getStyle("A".$excel_row.":C".$excel_row."")->applyFromArray($horizontal_left);
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Pendapatan Lain lain");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Lain lain (Rp)");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "1.000.000");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Saldo Awal (Rp)");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "1.000.000");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "");
		$excel_row++;
		
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Jumlah Pendapatan Lain lain (Rp) ");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "1.000.000");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "");
		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "Laba Usaha Sebelum Koreksi Fiskal (Rp)");
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, "");
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, "1.000.000");
		$excel_row++;


	    $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');

      	header('Content-Type: application/vnd.ms-excel');

      	header('Content-Disposition: attachment;filename="Laba Rugi.xls"');

      	$object_writer->save('php://output');


	}

}