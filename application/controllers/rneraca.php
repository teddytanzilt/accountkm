<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rneraca extends Report_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('trans_cash_bank_model');
		$this->load->model('master_type_cash_bank_model');
		$this->load->model('master_rekening_bank_model');

		$this->module_name = "rneraca";
		$this->module_title = "Neraca";
		
		$this->model_object = $this->trans_cash_bank_model;
		
		$this->view_report = $this->module_name."/report";
		
		$this->report_title = "Neraca";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("RNERACA", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}

	public function index($data = null)
	{	
		$this->module_subtitle = "Report";
		$data['title'] = $this->web_name.' | '.$this->module_title;
		$data['content'] = $this->view_report;		
		$this->load->view('parts/template',$data);
	}

	public function generate_report($data = null)
	{
		$data = $this->common_library->getData();

		$this->load->library('datetime_library');
		$this->load->library('pdf');	
		
		$marginX = 12;
		$marginY = 12;
		$paperW = 210; 
		$paperH = 297; 
		
		$this->pdf->fontpath = 'assets/fonts/pdf/'; 
		$this->pdf->AddFont('Calibri');
		$this->pdf->AddFont('Calibri-Bold','','calibrib.php');
		$this->pdf->AliasNbPages();
		$this->pdf->Open();
		$this->pdf->SetAutoPageBreak(true, '10');
		
		//$report_data = $this->model_object->getBukuBesar($data['tanggal_dari'], $data['tanggal_ke']);


		$bank_list = $this->master_rekening_bank_model->getActiveList();

		$this->generate_report_header($this->pdf, $data);
		
		$no = 1;
		$fontSize = 8;
		$fontSize2 = 6;
		$titleFontSize = 9;

		$this->pdf->SetFont('Calibri-Bold','',$fontSize);
		$this->pdf->Cell(137, 5, 'AKTIVA', 'LTR', 0, 'L', true);
		$this->pdf->SetFont('Calibri-Bold','', $fontSize);
		$this->pdf->Cell(137, 5, 'PASSIVA', 'LTR', 1, 'L', true);

		$this->pdf->Cell(137, 5, '', 'LR', 0, 'L', true);
		$this->pdf->Cell(137, 5, '', 'LR', 1, 'L', true);

		$this->pdf->SetFont('Calibri-Bold','U',$fontSize);
		$this->pdf->Cell(137, 5, 'Aktva Lancar', 'LR', 0, 'L', true);
		$this->pdf->SetFont('Calibri-Bold','U', $fontSize);
		$this->pdf->Cell(137, 5, 'Passiva Lancar', 'LR', 1, 'L', true);


		//$this->pdf->Cell(137, 5, '', 'LR', 0, 'L', true);
		//$this->pdf->Cell(137, 5, '', 'LR', 1, 'L', true);


		$x = 1;
		foreach($bank_list as $bl) {
			$this->pdf->SetFont('Calibri-Bold','',$fontSize);

			$this->pdf->Cell(77, 5, $bl->bank.' - '.$bl->nama_rekening, 'L', 0, 'L', true);
			$this->pdf->Cell(60, 5, '12.123.123', 'R', 0, 'R', true);

			if($x == 1){
				$this->pdf->Cell(77, 5, 'Hutang Dagang', 'L', 0, 'L', true);
				$this->pdf->Cell(60, 5, '12.123.123', 'R', 1, 'R', true);
			} else if($x == 2){
				$this->pdf->Cell(77, 5, 'Hutang PPh 29', 'L', 0, 'L', true);
				$this->pdf->Cell(60, 5, '12.123.123', 'R', 1, 'R', true);
			} else if($x == 3){
				$this->pdf->Cell(77, 5, 'Hutang PPN', 'L', 0, 'L', true);
				$this->pdf->Cell(60, 5, '12.123.123', 'R', 1, 'R', true);
			} else {
				$this->pdf->Cell(77, 5, '', 'L', 0, 'L', true);
				$this->pdf->Cell(60, 5, '', 'R', 1, 'R', true);
			}
			
			$x++;
		}

		if($x <= 3) {
			$num = $x;
			for ($x = $num; $x <= 3; $x++) {
				$this->pdf->SetFont('Calibri-Bold','',$fontSize);

				$this->pdf->Cell(77, 5, '', 'L', 0, 'L', true);
				$this->pdf->Cell(60, 5, '', 'R', 0, 'R', true);

				if($x == 1){
					$this->pdf->Cell(77, 5, 'Hutang Dagang', 'L', 0, 'L', true);
					$this->pdf->Cell(60, 5, '12.123.123', 'R', 1, 'R', true);
				} else if($x == 2){
					$this->pdf->Cell(77, 5, 'Hutang PPh 29', 'L', 0, 'L', true);
					$this->pdf->Cell(60, 5, '12.123.123', 'R', 1, 'R', true);
				} else if($x == 3){
					$this->pdf->Cell(77, 5, 'Hutang PPN', 'L', 0, 'L', true);
					$this->pdf->Cell(60, 5, '12.123.123', 'R', 1, 'R', true);
				} else {
					$this->pdf->Cell(77, 5, '', 'L', 0, 'L', true);
					$this->pdf->Cell(60, 5, '', 'R', 1, 'R', true);
				}

			}
		}

		$this->pdf->SetFont('Calibri-Bold','',$fontSize);
		$this->pdf->Cell(77, 5, 'Jumlah Aktiva Lancar', 'L', 0, 'L', true);
		$this->pdf->Cell(60, 5, '12.123.123', 'R', 0, 'R', true);

		$this->pdf->Cell(77, 5, 'Jumlah Hutang Lancar', 'L', 0, 'L', true);
		$this->pdf->Cell(60, 5, '12.123.123', 'R', 1, 'R', true);

		$this->pdf->Cell(137, 5, '', 'LR', 0, 'L', true);
		$this->pdf->Cell(137, 5, '', 'LR', 1, 'L', true);

		$this->pdf->SetFont('Calibri-Bold','U',$fontSize);
		$this->pdf->Cell(137, 5, 'Aktiva Tetap', 'LR', 0, 'L', true);
		$this->pdf->SetFont('Calibri-Bold','U', $fontSize);
		$this->pdf->Cell(137, 5, 'Hutang Jangka Panjang', 'LR', 1, 'L', true);

		




/*



		$this->pdf->SetFont('Calibri-Bold','',$fontSize);
		$this->pdf->Cell(145, 5, 'Penjualan Ekspor (Rp)', 0, 0, 'L', true);
		$this->pdf->SetFont('Calibri-Bold','', $fontSize);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 1, '', true);

		$this->pdf->Cell(145, 5, 'Penjualan Lokal (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 1, 'L', true);

		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(190, 5, 'Harga Pokok Penjualan', 0, 1, 'L', true);

		$this->pdf->Cell(100, 5, 'Persediaan Awal (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 0, 'R', true);
		$this->pdf->Cell(45, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(100, 5, 'Pembelian (Rp)', 0, 0, 'L', true);

		$this->pdf->SetFont('Calibri-Bold','U', $fontSize);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 0, 'R', true);
		$this->pdf->SetFont('Calibri-Bold','',$fontSize);
		$this->pdf->Cell(45, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(100, 5, 'Barang yang tersedia u/dijual (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 0, 'R', true);
		$this->pdf->Cell(45, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(100, 5, 'Persediaan Akhir (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 0, 'R', true);
		$this->pdf->Cell(45, 5, '', 0, 1, 'R', true);
		
		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(145, 5, 'Harga Pokok Penjualan (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 1, 'R', true);

		$this->pdf->Cell(145, 5, 'Laba Kotor Usaha (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 1, 'R', true);


		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','U', $fontSize);
		$this->pdf->Cell(145, 5, 'Biaya Operasional', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','',$fontSize);

//		foreach($tipe_kasbon_list as $tkl) {
//			$this->pdf->Cell(100, 5, $tkl->nama. "(Rp)", 0, 0, 'L', true);
//			$this->pdf->Cell(45, 5, '12.123.123', 0, 0, 'R', true);
//			$this->pdf->Cell(45, 5, '', 0, 1, 'R', true);
//		}

		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(145, 5, 'Jumlah Biaya Operasi (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 1, 'R', true);

		$this->pdf->Cell(145, 5, 'Laba Bersih Usaha Komersil (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','U', $fontSize);
		$this->pdf->Cell(145, 5, 'Pendapatan Lain-lain', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->SetFont('Calibri-Bold','',$fontSize);

		$this->pdf->Cell(100, 5, "Laba Selisih Kurs (Rp)", 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 0, 'R', true);
		$this->pdf->Cell(45, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(100, 5, "Jasa Giro (Rp)", 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 0, 'R', true);
		$this->pdf->Cell(45, 5, '', 0, 1, 'R', true);
		
		$this->pdf->Cell(100, 5, "Pajak Jasa Giro (Rp)", 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 0, 'R', true);
		$this->pdf->Cell(45, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(190, 5, '', 0, 1, 'R', true);

		$this->pdf->Cell(145, 5, 'Jumlah Pendapatan Lain-lain (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 1, 'R', true);

		$this->pdf->Cell(145, 5, 'Laba Usaha Sebelum Koreksi Fiskal (Rp)', 0, 0, 'L', true);
		$this->pdf->Cell(45, 5, '12.123.123', 0, 1, 'R', true);

		*/
		
		$this->pdf->Ln(5);
		$this->pdf->Output();
	}
	
	public function generate_report_header($obj, $data)
	{
		$obj->AddPage('L');		
		
		$title = $this->report_title;
		$titleFontSize = 18;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(0, 0,strtoupper($title), 0, 0, 'C');
		$obj->Ln(8);

		/*$company_name = "";
		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'PT', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$company_name, 0, 1, 'L');	*/

		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'Periode', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$data['tanggal_dari'].' - '.$data['tanggal_ke'], 0, 1, 'L');	
		
		$obj->Ln(5);

		$titleFontSize = 9;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->SetFillColor(200,200,200);

		//$obj->Cell(100, 5, '', 1, 0, 'C', true);
		//$obj->Cell(45, 5, '', 1, 0, 'C', true);
		//$obj->Cell(45, 5, '', 1, 1, 'R', true);

		$obj->SetFillColor(255,255,255);
		$obj->SetFont('Calibri','',$fontSize);


	}

}