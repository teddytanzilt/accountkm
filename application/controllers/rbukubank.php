<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rbukubank extends Report_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('trans_cash_bank_model');
		$this->load->model('master_rekening_bank_model');

		$this->module_name = "rbukubank";
		$this->module_title = "Laporan Buku Bank";
		
		$this->model_object = $this->trans_cash_bank_model;
		
		$this->view_report = $this->module_name."/report";
		
		$this->report_title = "Laporan Buku Bank";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("RBUKUBANK", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}

	public function index($data = null)
	{	
		$this->module_subtitle = "Report";
		$data['bank_list'] = $this->master_rekening_bank_model->getActiveList();
		$data['title'] = $this->web_name.' | '.$this->module_title;
		$data['content'] = $this->view_report;		
		$this->load->view('parts/template',$data);
	}

	public function generate_report($data = null)
	{
		$data = $this->common_library->getData();

		$this->load->library('datetime_library');
		$this->load->library('pdf');	
		
		$marginX = 12;
		$marginY = 12;
		$paperW = 210; 
		$paperH = 297; 
		
		$this->pdf->fontpath = 'assets/fonts/pdf/'; 
		$this->pdf->AddFont('Calibri');
		$this->pdf->AddFont('Calibri-Bold','','calibrib.php');
		$this->pdf->AliasNbPages();
		$this->pdf->Open();
		$this->pdf->SetAutoPageBreak(true, '10');
		
		//$report_data = $this->model_object->getHistoryPerBank($data['bank']);
		$report_data = $this->model_object->getHistoryPerBankRange($data['bank'], $data['tanggal_dari'], $data['tanggal_ke']);

		$master_rekening_bank_cond = array('id' => $data['bank'], 'record_status' => STATUS_ACTIVE);
		$this->master_rekening_bank_model->setCond($master_rekening_bank_cond);
		$bank = $this->master_rekening_bank_model->getHeaderArray();
		$data['bank_detail'] = $bank;

		$total_debit = 0;
		$total_kredit = 0;
		$saldo_akhir = 0;

		$this->generate_report_header($this->pdf, $data);
		
		$no = 1;
		$fontSize = 8;
		$fontSize2 = 6;
		foreach($report_data as $rd){
			if($no != 0 && $no % 33 == 0){
				$this->generate_report_header($this->pdf, $data);
			}
			$this->pdf->SetFont('Calibri','',$fontSize);
			
			$titleFontSize = 9;
			$this->pdf->SetFont('Calibri-Bold','',$titleFontSize);

			$tanggal = substr($rd['tanggal'], 8, 2)."/".substr($rd['tanggal'], 5, 2)."/".substr($rd['tanggal'], 0, 4);

			//$this->pdf->Cell(10, 5, $no, 1, 0, 'C', true);
			$this->pdf->Cell(18, 5, $tanggal, 1, 0, 'C', true);
			$this->pdf->Cell(90, 5, $rd['note'], 1, 0, 'L', true);
			$this->pdf->Cell(25, 5, number_format($rd['debit'],0,'.',','), 1, 0, 'R', true);
			$this->pdf->Cell(25, 5, number_format($rd['kredit'],0,'.',','), 1, 0, 'R', true);
			$this->pdf->Cell(30, 5, number_format($rd['saldo_bank'],0,'.',','), 1, 1, 'R', true);

			$saldo_akhir = $rd['saldo_bank'];
			$total_debit = intval($total_debit) + intval($rd['debit']);
			$total_kredit = intval($total_kredit) + intval($rd['kredit']);
			$no++;
		}
		
		$titleFontSize = 9;
		$this->pdf->SetFont('Calibri-Bold','',$titleFontSize);
		//$this->pdf->Cell(10, 5, $no, 1, 0, 'C', true);
		$this->pdf->Cell(108, 5, 'Total', 1, 0, 'L', true);
		$this->pdf->Cell(25, 5, number_format($total_debit,0,'.',','), 1, 0, 'R', true);
		$this->pdf->Cell(25, 5, number_format($total_kredit,0,'.',','), 1, 0, 'R', true);

		$this->pdf->Ln();

		$this->pdf->Cell(108, 5, 'Saldo Akhir', 1, 0, 'L', true);
		$this->pdf->Cell(50, 5, number_format($saldo_akhir,0,'.',','), 1, 0, 'R', true);
		


		$this->pdf->Ln(5);
		$this->pdf->Output();
	}
	
	public function generate_report_header($obj, $data)
	{
		$obj->AddPage();		
		
		$title = $this->report_title;
		$titleFontSize = 18;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(0, 0,strtoupper($title), 0, 0, 'C');
		$obj->Ln(8);

		/*$company_name = "";
		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'PT', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$company_name, 0, 1, 'L');	*/

		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'Bank', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$data['bank_detail']['bank'].' - '.$data['bank_detail']['no_rekening'].'/'.$data['bank_detail']['nama_rekening'], 0, 1, 'L');	
		
		$obj->Ln(5);

		$titleFontSize = 9;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->SetFillColor(200,200,200);
		//$obj->Cell(10, 5, 'No', 1, 0, 'C', true);
		$obj->Cell(18, 5, 'Tanggal', 1, 0, 'C', true);
		$obj->Cell(90, 5, 'Keterangan', 1, 0, 'C', true);
		$obj->Cell(25, 5, 'Debit', 1, 0, 'C', true);
		$obj->Cell(25, 5, 'Kredit', 1, 0, 'C', true);
		$obj->Cell(30, 5, 'Saldo', 1, 0, 'C', true);

		$obj->SetFillColor(255,255,255);
		$obj->SetFont('Calibri','',$fontSize);




		$this->pdf->Ln();
	}

}