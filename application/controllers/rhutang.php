<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rhutang extends Report_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('trans_purchase_order_header_model');

		$this->module_name = "rhutang";
		$this->module_title = "Laporan Hutang";
		
		$this->model_object = $this->trans_purchase_order_header_model;
		
		$this->view_report = $this->module_name."/report";
		
		$this->report_title = "Laporan Hutang";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("RHUTANG", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}

	public function index($data = null)
	{	
		$this->module_subtitle = "Report";
		$data['title'] = $this->web_name.' | '.$this->module_title;
		$data['content'] = $this->view_report;		
		$this->load->view('parts/template',$data);
	}

	public function generate_report($data = null)
	{
		$data = $this->common_library->getData();


		if($data['report'] == "0"){
			$this->load->library('datetime_library');
			$this->load->library('pdf');	
			
			$marginX = 12;
			$marginY = 12;
			$paperW = 210; 
			$paperH = 297; 
			
			$this->pdf->fontpath = 'assets/fonts/pdf/'; 
			$this->pdf->AddFont('Calibri');
			$this->pdf->AddFont('Calibri-Bold','','calibrib.php');
			$this->pdf->AliasNbPages();
			$this->pdf->Open();
			$this->pdf->SetAutoPageBreak(true, '10');
			
			$report_data = $this->model_object->getListPembelianPerSupplier($data['tanggal_dari'], $data['tanggal_ke']);

			$this->generate_report_header($this->pdf, $data);
			
			$no = 1;
			$fontSize = 8;
			$fontSize2 = 6;

			$current_supplier = "";
			foreach($report_data as $rd){
				if($no != 0 && $no % 33 == 0){
					$this->generate_report_header($this->pdf, $data);
				}

				if($current_supplier == "" || $current_supplier != $rd['nama_supplier']){
					$current_supplier = $rd['nama_supplier'];

					$titleFontSize = 9;
					$this->pdf->SetFont('Calibri-Bold','',$titleFontSize);
					$this->pdf->Cell(190, 5, $current_supplier, 1, 1, 'L', true);
					$no++;
				} 

				$this->pdf->SetFont('Calibri','',$fontSize);
				
				$titleFontSize = 9;
				$this->pdf->SetFont('Calibri-Bold','',$titleFontSize);

				$dTanggalJatuhTempo = new DateTime($rd['tanggal']);
				$dTanggalJatuhTempo->add(new DateInterval('P30D'));
				//$tanggalJatuhTempo = $dTanggalJatuhTempo->format('Y-m-d H:i:s');
				$tanggalJatuhTempo = $dTanggalJatuhTempo->format('d/m/Y');

				$tanggal = substr($rd['tanggal'], 8, 2)."/".substr($rd['tanggal'], 5, 2)."/".substr($rd['tanggal'], 0, 4);
				$this->pdf->Cell(65, 5, $rd['faktur_pajak'], 1, 0, 'C', true);
				$this->pdf->Cell(25, 5, $tanggal, 1, 0, 'C', true);
				$this->pdf->Cell(25, 5, $tanggalJatuhTempo, 1, 0, 'R', true);
				$this->pdf->Cell(25, 5, number_format($rd['total'],0,'.',','), 1, 0, 'R', true);
				$this->pdf->Cell(25, 5, number_format($rd['total'],0,'.',','), 1, 0, 'R', true);
				$this->pdf->Cell(25, 5, number_format($rd['total'],0,'.',','), 1, 1, 'R', true);
				//$this->pdf->Cell(40, 5, '', 1, 1, 'C', true);

				$no++;
			}
			
			$this->pdf->Ln(5);
			$this->pdf->Output();
		} else {
			$this->load->library("excel");
			$object = new PHPExcel();

			$horizontal_center = array(
		        'alignment' => array(
		            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		        )
		    );
		    $horizontal_left = array(
		        'alignment' => array(
		            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
		        )
		    );
		    $horizontal_right = array(
		        'alignment' => array(
		            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
		        )
		    );

		    $report_data = $this->model_object->getListPembelianPerSupplier($data['tanggal_dari'], $data['tanggal_ke']);

			$object->setActiveSheetIndex(0);
			
			$object->getActiveSheet()->getColumnDimension('A')->setWidth(30);
			$object->getActiveSheet()->getColumnDimension('B')->setWidth(40);
			$object->getActiveSheet()->getColumnDimension('C')->setWidth(40);
			$object->getActiveSheet()->getColumnDimension('D')->setWidth(40);
			$object->getActiveSheet()->getColumnDimension('E')->setWidth(40);
			$object->getActiveSheet()->getColumnDimension('F')->setWidth(40);

			$excel_row = 1;
			
			$object->getActiveSheet()->mergeCells("A".$excel_row.":F".$excel_row."");
			$object->getActiveSheet()->getStyle("A".$excel_row.":F".$excel_row."")->applyFromArray($horizontal_center);
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "LAPORAN HUTANG");
			$excel_row++;


			$excel_row++;

			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, 'No Faktur');
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, 'Tanggal');
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, 'Jatuh Tempo');
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, 'Nilai Faktur');
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, 'Hutang(Asing)');
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, 'Hutang Pajak');
			$excel_row++;

			
			$current_supplier = "";
			foreach($report_data as $rd){
				if($current_supplier == "" || $current_supplier != $rd['nama_supplier']){
					$current_supplier = $rd['nama_supplier'];

					$object->getActiveSheet()->mergeCells("A".$excel_row.":F".$excel_row."");
					$object->getActiveSheet()->getStyle("A".$excel_row.":F".$excel_row."")->applyFromArray($horizontal_left);
					$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $current_supplier);
					$excel_row++;
				}


				$dTanggalJatuhTempo = new DateTime($rd['tanggal']);
				$dTanggalJatuhTempo->add(new DateInterval('P30D'));
				//$tanggalJatuhTempo = $dTanggalJatuhTempo->format('Y-m-d H:i:s');
				$tanggalJatuhTempo = $dTanggalJatuhTempo->format('d/m/Y');

				$tanggal = substr($rd['tanggal'], 8, 2)."/".substr($rd['tanggal'], 5, 2)."/".substr($rd['tanggal'], 0, 4);
				
				$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $rd['faktur_pajak']);
				$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $tanggal);
				$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $tanggalJatuhTempo);
				$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, number_format($rd['total'],0,'.',','));
				$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, number_format($rd['total'],0,'.',','));
				$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, number_format($rd['total'],0,'.',','));

				$excel_row++;
			}

		    $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');

	      	header('Content-Type: application/vnd.ms-excel');

	      	header('Content-Disposition: attachment;filename="Hutang.xls"');

	      	$object_writer->save('php://output');


		}
	}
	
	public function generate_report_header($obj, $data)
	{
		$obj->AddPage();		
		
		$title = $this->report_title;
		$titleFontSize = 18;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(0, 0,strtoupper($title), 0, 0, 'C');
		$obj->Ln(8);

		/*$company_name = "";
		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'PT', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$company_name, 0, 1, 'L');	*/

		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'Periode', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$data['tanggal_dari'].' - '.$data['tanggal_ke'], 0, 1, 'L');	
		
		$obj->Ln(5);

		$titleFontSize = 9;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->SetFillColor(200,200,200);

		$obj->Cell(65, 5, 'No. Faktur', 1, 0, 'C', true);
		$obj->Cell(25, 5, 'Tanggal', 1, 0, 'C', true);
		$obj->Cell(25, 5, 'Jatuh Tempo', 1, 0, 'C', true);
		$obj->Cell(25, 5, 'Nilai Faktur', 1, 0, 'C', true);
		$obj->Cell(25, 5, 'Hutang(Asing)', 1, 0, 'C', true);
		$obj->Cell(25, 5, 'Hutang Pajak', 1, 1, 'C', true);
		//$obj->Cell(40, 5, 'Umur', 1, 1, 'C', true);

		$obj->SetFillColor(255,255,255);
		$obj->SetFont('Calibri','',$fontSize);


	}

	public function generate_excel($data = null)
	{	
		$data = $this->common_library->getData();

      	$this->load->library("excel");
		$object = new PHPExcel();

		$horizontal_center = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        )
	    );
	    $horizontal_left = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	        )
	    );
	    $horizontal_right = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	        )
	    );

	    $report_data = $this->model_object->getListPembelianPerSupplier($data['tanggal_dari'], $data['tanggal_ke']);

		$object->setActiveSheetIndex(0);
		
		$object->getActiveSheet()->getColumnDimension('A')->setWidth(30);
		$object->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$object->getActiveSheet()->getColumnDimension('C')->setWidth(40);
		$object->getActiveSheet()->getColumnDimension('D')->setWidth(40);
		$object->getActiveSheet()->getColumnDimension('E')->setWidth(40);
		$object->getActiveSheet()->getColumnDimension('F')->setWidth(40);

		$excel_row = 1;
		
		$object->getActiveSheet()->mergeCells("A".$excel_row.":F".$excel_row."");
		$object->getActiveSheet()->getStyle("A".$excel_row.":F".$excel_row."")->applyFromArray($horizontal_center);
		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, "LAPORAN HUTANG");
		$excel_row++;


		$excel_row++;

		$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, 'No Faktur');
		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, 'Tanggal');
		$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, 'Jatuh Tempo');
		$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, 'Nilai Faktur');
		$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, 'Hutang(Asing)');
		$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, 'Hutang Pajak');
		$excel_row++;

		
		$current_supplier = "";
		foreach($report_data as $rd){
			if($current_supplier == "" || $current_supplier != $rd['nama_supplier']){
				$current_supplier = $rd['nama_supplier'];

				$object->getActiveSheet()->mergeCells("A".$excel_row.":F".$excel_row."");
				$object->getActiveSheet()->getStyle("A".$excel_row.":F".$excel_row."")->applyFromArray($horizontal_left);
				$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $current_supplier);
				$excel_row++;
			}


			$dTanggalJatuhTempo = new DateTime($rd['tanggal']);
			$dTanggalJatuhTempo->add(new DateInterval('P30D'));
			//$tanggalJatuhTempo = $dTanggalJatuhTempo->format('Y-m-d H:i:s');
			$tanggalJatuhTempo = $dTanggalJatuhTempo->format('d/m/Y');

			$tanggal = substr($rd['tanggal'], 8, 2)."/".substr($rd['tanggal'], 5, 2)."/".substr($rd['tanggal'], 0, 4);
			
			$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $rd['faktur_pajak']);
			$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $tanggal);
			$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $tanggalJatuhTempo);
			$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, number_format($rd['total'],0,'.',','));
			$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, number_format($rd['total'],0,'.',','));
			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, number_format($rd['total'],0,'.',','));

			$excel_row++;
		}

	    $object_writer = PHPExcel_IOFactory::createWriter($object, 'Excel5');

      	header('Content-Type: application/vnd.ms-excel');

      	header('Content-Disposition: attachment;filename="Laba Rugi.xls"');

      	$object_writer->save('php://output');


	}
}