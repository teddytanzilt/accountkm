<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tdeliveryorder extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('master_customer_model');
		$this->load->model('trans_delivery_order_header_model');
		$this->load->model('trans_delivery_order_detail_model');
		$this->load->model('trans_sales_invoice_header_model');
		$this->load->model('trans_sales_invoice_detail_model');
		$this->load->model('master_customer_model');
		$this->load->model('master_barang_model');
		
		$this->module_name = "tdeliveryorder";
		$this->module_title = "Delivery Order";
		$this->table_name = "trans_delivery_order_header";
		$this->model_object = $this->trans_delivery_order_header_model;
		$this->model_detail_object = $this->trans_delivery_order_detail_model;
		
		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";
		
		$this->msg_add_success = "Create DO Success.";
		$this->msg_edit_success = "Edit DO Success.";
		$this->msg_delete_success = "Delete DO Success.";
		
		$this->trans_prefix = "DO";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("DELIVERYORDER", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}

	public function create_load_data($data = null) {
		$data['customer_list'] = $this->master_customer_model->getActiveList();
		$data['barang_list'] = $this->master_barang_model->getActiveList();
		$data['tanggal'] = date('Y-m-d');
		return $data;
	}

	public function create_validation_data($result, $data) {
		if($result['validation']) {
			if(!isset($data['barang'])){
				$result['validation'] = false;
				$result['message'] = "Tidak ada barang dalam dokumen ini.";
			}
		}
		if($result['validation']) {
			if(count($data['barang']) > 10){
				$result['validation'] = false;
				$result['message'] = "Jumlah barang tidak boleh lebih dari 10";
			}
		}
		return $result;
	}

	public function edit_load_data($data = null) {
		if($data['trans_status'] == STATUS_COMPLETE) {
			redirect($this->link_view.$data['id']);
		} 

		$data['customer_list'] = $this->master_customer_model->getActiveList();
		$data['barang_list'] = $this->master_barang_model->getActiveList();

		if(!isset($data['barang'])){
			$content_detail_list = $this->model_detail_object->getTransDetailCompositeByHeader($data['id']);
			$fillable_value = $this->model_detail_object->getFillableValueList();
			foreach($fillable_value as $fv){
				$data[$fv] = array();
			}
			foreach($content_detail_list as $d){
				foreach($fillable_value as $fv){
					$data[$fv][] = $d[$fv];
				}
				$data['text_kode_barang'][] = $d['text_kode_barang'];
				$data['text_nama_barang'][] = $d['text_nama_barang'];
			}
		}
		
		return $data;
	}

	public function edit_validation_data($result, $data) {
		if($result['validation']) {
			if(!isset($data['barang'])){
				$result['validation'] = false;
				$result['message'] = "Tidak ada barang dalam dokumen ini.";
			}
		}
		if($result['validation']) {
			if(count($data['barang']) > 10){
				$result['validation'] = false;
				$result['message'] = "Jumlah barang tidak boleh lebih dari 10";
			}
		}
		return $result;
	}

	public function view_load_data($data = null) {
		if($data['trans_status'] == STATUS_PENDING) {
			redirect($this->link_edit.$data['id']);
		}  
		
		$data['customer_list'] = $this->master_customer_model->getActiveList();
		$data['barang_list'] = $this->master_barang_model->getActiveList();

		if(!isset($data['barang'])){
			$content_detail_list = $this->model_detail_object->getTransDetailCompositeByHeader($data['id']);
			$fillable_value = $this->model_detail_object->getFillableValueList();
			foreach($fillable_value as $fv){
				$data[$fv] = array();
			}
			foreach($content_detail_list as $d){
				foreach($fillable_value as $fv){
					$data[$fv][] = $d[$fv];
				}
				$data['text_kode_barang'][] = $d['text_kode_barang'];
				$data['text_nama_barang'][] = $d['text_nama_barang'];
			}
		}

		return $data;
	}
	
	public function get_data_list_active_json(){	
		$request_data= $_REQUEST;
		$column = $this->model_object->getDatatableShowValueList();
		$total_data = $this->model_object->getCountDataActive();
		$total_filtered = $this->model_object->getDataTableCountActive($request_data);
		$data_filtered = $this->model_object->getDataTableDataActive($request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}

	public function create_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		if($result['validation']) {
			$result = $this->create_validation_data($result, $data);
		}	
		//$result['validation'] = false; //TEST
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$year = intval(date("Y",strtotime($data['tanggal'])));
					$month = intval(date("m",strtotime($data['tanggal'])));
					$next_id = $this->model_object->nextDynamicTrans($year, $month);
					//$next_id = $this->model_object->nextTrans();
					$fillable_value = $this->model_object->getFillableValueList();
					$content_value = $this->model_object->getValueList();
					$content_value['no_transaksi'] = '';
					//$content_value['tanggal'] = date('Y-m-d H:i:s');
					$content_value['tanggal'] = $data['tanggal'].' '.date('H:i:s');
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["created_by"] = $this->session_user_id;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->insertHeader();
					$content_id = $this->db->insert_id();
					$data['id'] = $content_id;
					$no_transaksi = $this->trans_prefix.date("Y",strtotime($data['tanggal'])).date("m",strtotime($data['tanggal'])).sprintf("%05d", $next_id);
					$content_value['id'] = $content_id;
					$content_value['no_transaksi'] = $no_transaksi;
					//if($data['action'] == "Finish"){
						//$content_value['trans_status'] = STATUS_COMPLETE;
					//} else {
						$content_value['trans_status'] = STATUS_PENDING;
					//}
					$this->model_object->setValueList($content_value);		
					$value_condition = array('id' => $content_id);
					$this->model_object->setCond($value_condition);
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					if($data['action'] == "Finish"){


					}
					if ($this->db->trans_status() === FALSE){ break; }
					
					if(isset($data['barang'])){
						for($x = 0 ; $x < count($data['barang']) ; $x++){
							$this->model_detail_object->refreshValueList();
							$fillable_value = $this->model_detail_object->getFillableValueList();
							$content_value = $this->model_detail_object->getValueList();
							$content_value['header_id'] = $content_id;
							foreach($fillable_value as $fv){
								$content_value[$fv] = $data[$fv][$x];
							}
							$content_value["created_by"] = $this->session_user_id;
							$content_value["created_on"] = date('Y-m-d H:i:s');
							$this->model_detail_object->setValueList($content_value);		
							$this->model_detail_object->insertHeader();
							if ($this->db->trans_status() === FALSE){ break; }
						}
						if ($this->db->trans_status() === FALSE){ break; }
					}
					
					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			//$this->session->set_flashdata("success_message", $this->msg_add_success);
			//redirect($this->link_back);
			redirect($this->link_back."/preview/".$data['id']);
		} else {
			$this->create($data);
		}
	}


	public function edit_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		if($result['validation']) {
			$result = $this->edit_validation_data($result, $data);
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$fillable_value = $this->model_object->getFillableValueList();
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
					$this->model_object->setCond($content_cond);	
					$content_value = $this->model_object->getHeaderArray();
					$no_transaksi = $content_value['no_transaksi'];
					$content_value['tanggal'] = $data['tanggal'].' '.date('H:i:s');
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["modified_by"] = $this->session_user_id;
					$content_value["modified_on"] = date('Y-m-d H:i:s');
					//if($data['action'] == "Finish"){
						//$content_value['trans_status'] = STATUS_COMPLETE;
					//} else {
						$content_value['trans_status'] = STATUS_PENDING;
					//}
					$this->model_object->setValueList($content_value);		
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					if($data['action'] == "Finish"){
						
					}
					if ($this->db->trans_status() === FALSE){ break; }
					
					if(isset($data['barang'])){
						$content_detail_cond = array('header_id' => $data['id']);
						$this->model_detail_object->setCond($content_detail_cond);	
						$this->model_detail_object->deleteHeader();
						if ($this->db->trans_status() === FALSE){ break; }

						for($x = 0 ; $x < count($data['barang']) ; $x++){
							$this->model_detail_object->refreshValueList();
							$fillable_value = $this->model_detail_object->getFillableValueList();
							$content_value = $this->model_detail_object->getValueList();
							$content_value['header_id'] = $data['id'];
							foreach($fillable_value as $fv){
								$content_value[$fv] = $data[$fv][$x];
							}
							$content_value["created_by"] = $this->session_user_id;
							$content_value["created_on"] = date('Y-m-d H:i:s');
							$this->model_detail_object->setValueList($content_value);		
							$this->model_detail_object->insertHeader();
							if ($this->db->trans_status() === FALSE){ break; }

							if($data['action'] == "Finish"){
								
							}
						}
						if ($this->db->trans_status() === FALSE){ break; }
					}

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			//$this->session->set_flashdata("success_message", $this->msg_edit_success);
			//redirect($this->link_back);
			redirect($this->link_back."/preview/".$data['id']);
		} else {
			$this->edit($data['id'], $data);
		}
	}









	/*public function deliver_load_data($data = null) {
		$data['customer_list'] = $this->master_customer_model->getActiveList();
		$data['barang_list'] = $this->master_barang_model->getActiveList();

		if(!isset($data['barang'])){
			$content_detail_list = $this->trans_sales_invoice_detail_model->getTransDetailCompositeByHeader($data['id']);
			$fillable_value = $this->trans_sales_invoice_detail_model->getFillableValueList();
			foreach($fillable_value as $fv){
				$data[$fv] = array();
			}
			foreach($content_detail_list as $d){
				foreach($fillable_value as $fv){
					$data[$fv][] = $d[$fv];
				}
				$data['text_kode_barang'][] = $d['text_kode_barang'];
				$data['text_nama_barang'][] = $d['text_nama_barang'];
			}
		}
		
		return $data;
	}

	public function deliver_validation_data($result, $data) {
		return $result;
	}*/

	/*public function deliver($id, $data = null)
	{
		$this->module_subtitle = "Deliver";
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->trans_sales_invoice_header_model->setCond($content_cond);
		$content_detail = $this->trans_sales_invoice_header_model->getHeaderArray();	
		foreach($content_detail as $arr_name => $arr_value){
			$data[$arr_name] = $arr_value;
		}
		$data = $this->deliver_load_data($data);
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->module_name."/deliver";		
		$this->load->view('parts/template',$data);
	}*/

	/*public function deliver_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());*/
		/*********Validation starts here ***********/		
		/*if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		if($result['validation']) {
			$result = $this->deliver_validation_data($result, $data);
		}*/

		//var_dump($data); exit;
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		/*if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$sales_invoice_header_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
					$this->trans_sales_invoice_header_model->setCond($sales_invoice_header_cond);	
					$sales_invoice_header_value = $this->trans_sales_invoice_header_model->getHeaderArray();
					$sales_invoice_header_value["delivery_status"] = STATUS_COMPLETE;
					$sales_invoice_header_value["modified_by"] = $this->session_user_id;
					$sales_invoice_header_value["modified_on"] = date('Y-m-d H:i:s');
					$this->trans_sales_invoice_header_model->setValueList($sales_invoice_header_value);		
					$this->trans_sales_invoice_header_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$data['customer'] = $sales_invoice_header_value['customer'];
					$data['ship_date'] = $sales_invoice_header_value['ship_date'];
					$data['prepared_date'] = $sales_invoice_header_value['prepared_date'];
					$data['approved_date'] = $sales_invoice_header_value['approved_date'];
					$data['shipped_date'] = $sales_invoice_header_value['shipped_date'];
					$data['received_date'] = $sales_invoice_header_value['received_date'];

					$next_id = $this->model_object->nextTrans();
					$data['invoice'] = $data['id'];
					$fillable_value = $this->model_object->getFillableValueList();
					$content_value = $this->model_object->getValueList();
					$content_value['no_transaksi'] = '';
					$content_value['tanggal'] = date('Y-m-d H:i:s');
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["created_by"] = $this->session_user_id;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->insertHeader();
					$content_id = $this->db->insert_id();
					$no_transaksi = $this->trans_prefix.date('Ym').sprintf("%05d", $next_id);
					$content_value['id'] = $content_id;
					$content_value['no_transaksi'] = $no_transaksi;
					//if($data['action'] == "Finish"){
						$content_value['trans_status'] = STATUS_COMPLETE;
					//} else {
					//	$content_value['trans_status'] = STATUS_PENDING;
					//}
					$this->model_object->setValueList($content_value);		
					$value_condition = array('id' => $content_id);
					$this->model_object->setCond($value_condition);
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }


					$sales_invoice_detail_cond = array('header_id' => $data['id']);
					$this->trans_sales_invoice_detail_model->setCond($sales_invoice_detail_cond);
					$sales_invoice_detail_list = $this->trans_sales_invoice_detail_model->getListArray();
					foreach ($sales_invoice_detail_list as $dtl) {
						$this->model_detail_object->refreshValueList();
						$fillable_value = $this->model_detail_object->getFillableValueList();
						$content_value = $this->model_detail_object->getValueList();
						$content_value['header_id'] = $content_id;
						foreach($fillable_value as $fv){
							$content_value[$fv] = $dtl[$fv];
						}
						$content_value["created_by"] = $this->session_user_id;
						$content_value["created_on"] = date('Y-m-d H:i:s');
						$this->model_detail_object->setValueList($content_value);		
						$this->model_detail_object->insertHeader();
						if ($this->db->trans_status() === FALSE){ break; }						
					}

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);*/
		/*********Process ends here ***********/	
		/*if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_edit_success);
			redirect($this->link_back);
		} else {
			$this->edit($data['id'], $data);
		}
	}*/

	public function preview($id, $data = null)
	{
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		$content_detail = $this->model_object->getHeaderArray();	

		$core_app_config_cond = array('config_key' => 'COMPANY_NAME');
		$this->core_app_config_model->setCond($core_app_config_cond);
		$company_name =  $this->core_app_config_model->getHeaderField("config_value");

		$customer_cond = array('record_status' => STATUS_ACTIVE, 'id' => $content_detail['customer']);
		$this->master_customer_model->setCond($customer_cond);
		$customer_detail = $this->master_customer_model->getHeaderArray();	

		$content_detail_list = $this->model_detail_object->getTransDetailCompositeByHeader($id);

		$this->load->library('datetime_library');
		$this->load->library('pdf');	
		$this->pdf->fontpath = 'assets/fonts/pdf/'; 
			
		$this->pdf->AddFont('Calibri');
		$this->pdf->AddFont('Calibri-Bold','','calibrib.php');
		$this->pdf->AliasNbPages();
		$this->pdf->Open();
		$this->pdf->SetAutoPageBreak(false);

		$this->pdf->AddPage();

		$this->pdf->SetFont('Calibri-Bold','',16);
		$x1 = 9;
		$y1 = 15;
		$w = 190;
		$h = 10; 
		$border = 1; 
		$lineBreak = 1; 
		$text = "Delivery Order";
		$align = "C"; 
		$this->pdf->SetXY ($x1,$y1);
		$this->pdf->Cell($w,$h,$text,$border,$lineBreak,$align);
			
		$name = $company_name;
		$x1 = 10;
		$y1 = 28;
		$this->pdf->SetXY ($x1,$y1);			
		$len = $this->pdf->GetStringWidth( $name );//the width
		$h = 2;			
		$this->pdf->SetFont('Calibri-Bold','',11);
		$this->pdf->Cell( $len, $h, $name);
		
		/*$x1 = 10;
		$y1 = 26;
		$w1 = 178;
		$h1 = 93;
		$logo = base_url()."/assets/images/logo-report.png";
		$this->pdf->Cell( $w1, $h1, $this->pdf->Image($logo, $x1, $y1, 22), 0, 0, 'L', false );*/


		$x1 = 10;
		$y1 = 33;
		$this->pdf->SetXY ($x1,$y1);
		$w = 30;
		$h=5;
		$this->pdf->SetFont('Calibri','',11);
		$this->pdf->Cell ($w,$h,'Customer',1,'','L');
		$this->pdf->Cell($w+30,$h,$customer_detail['nama'],1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h);
		$this->pdf->Cell ($w,$h,'Ship To',1,'','L');
		$this->pdf->Cell($w+30,$h,$content_detail['ship_to'],1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h+$h);
		$this->pdf->Cell ($w,$h,'Pengangkutan',1,'','L');
		$this->pdf->Cell($w+30,$h,$content_detail['ship_via'],1,'','L');
		//$this->pdf->SetXY ($x1,$y1+$h+$h);
		//$this->pdf->Cell ($w,$h,'Ship Via',1,'','R');
		//$this->pdf->Cell($w+30,$h,$content_detail['ship_via'],1,'','C');

		$sNo = $content_detail['no_transaksi'];
		$sDate = $this->datetime_library->indonesian_date($this->datetime_library->date_format($content_detail['tanggal'], 'l jS F Y'), 'j F Y', '');
		$sPO = $content_detail['po_no'];
		$x1 = 132;
		$y1 = 33;
		$this->pdf->SetXY ($x1,$y1);
		$w = 33;
		$h=5;
		$this->pdf->SetFont('Calibri','',11);
		$this->pdf->Cell ($w,$h,'Nomor',1,'','L');
		$this->pdf->Cell($w+1,$h,$sNo,1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h);
		$this->pdf->Cell ($w,$h,'Tanggal',1,'','L');
		$this->pdf->Cell($w+1,$h,$sDate,1,'','L');
		$this->pdf->SetXY ($x1,$y1+$h+$h);
		$this->pdf->Cell ($w,$h,'P.O No.',1,'','L');
		$this->pdf->Cell($w+1,$h,$sPO,1,'','L');
		
		$x1 = 10;
		$y1 = 53;
		$this->pdf->SetXY ($x1,$y1);
		$this->pdf->SetFont('Calibri-Bold','',11);
		
		//$this->pdf->SetTextColor(255,255,255); 
		//$this->pdf->SetFillColor(128,128,128); 

		$this->pdf->SetTextColor(0,0,0); 
		$this->pdf->SetFillColor(255,255,255); 
		$this->pdf->Cell(9, 5, 'No', 1, '', 'C',true);
		//$this->pdf->Cell(40, 5, 'Item', 1, '', 'C', true);
		$this->pdf->Cell(140, 5, 'Description', 1, '', 'L', true);
		$this->pdf->Cell(15, 5, 'Qty', 1, '', 'C', true);
		$this->pdf->Cell(25, 5, 'Note', 1, '', 'C', true);
		$this->pdf->Ln();
		$this->pdf->SetTextColor(0,0,0); 
		$this->pdf->SetFont('Calibri','',11);
		$num = 1;
		foreach($content_detail_list as $valueBarang){
			$PartNo = $valueBarang['text_kode_barang'];
			$NamaBarang = $valueBarang['text_nama_barang'];
			$Qty = $valueBarang['quantity'];
			$Note = $valueBarang['note'];
			$this->pdf->Cell(9, 5, $num, 1, '', 'C');
			//$this->pdf->Cell(40, 5, substr($PartNo, 0, 16), 1, '', 'C');
			$this->pdf->Cell(140, 5, substr($NamaBarang, 0, 80), 1, '', 'L');
			$this->pdf->Cell(15, 5, $Qty, 1, '', 'C');
			$this->pdf->Cell(25, 5, substr($Note, 0, 10), 1, '', 'C');
			$this->pdf->Ln();	
			$num++;
		}	
		for ($x = $num; $x <= 10; $x++) {
		$this->pdf->Cell(9, 5, $x, 1, '', 'C');
		//$this->pdf->Cell(40, 5, '', 1, '', 'C');
		$this->pdf->Cell(140, 5, '', 1, '', 'C');
		$this->pdf->Cell(15, 5, '', 1, '', 'C');
		$this->pdf->Cell(25, 5, '', 1, '', 'C');
		$this->pdf->Ln();	
		}
		
		$this->pdf->Ln();	
		$this->pdf->SetFont('Calibri','',11);
		
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Cell(48, 5, 'Shipped By', '', '', 'C');
		$this->pdf->Cell(48, 5, 'Received By', '', '', 'C');
		
		$this->pdf->Ln();
		$this->pdf->Ln();
		$this->pdf->Ln();
		
		$x1=19;
		$y1=135;
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		//$this->pdf->Line($x1,$y1,$x1+30,$y1);
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		//$this->pdf->Line($x1+48,$y1,$x1+48+30,$y1);
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Line($x1+48+48,$y1,$x1+48+48+30,$y1);
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Line($x1+48+48+48,$y1,$x1+48+48+48+30,$y1);

		$x1 = 10;
		$y1 = 120;
		$w = 80;
		$h = 10; 
		$border = 1; 
		$lineBreak = 1; 
		$text = "NB: Untuk Sementara Faktur Akan Menyusul";
		$align = "C"; 
		$this->pdf->SetXY ($x1,$y1);
		$this->pdf->Cell($w,$h,$text,$border,$lineBreak,$align);
		
		$this->pdf->Output();
	}
}