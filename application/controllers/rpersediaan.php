<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpersediaan extends Report_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('master_stock_barang_model');
		$this->load->model('trans_inventory_barang_model');

		$this->module_name = "rpersediaan";
		$this->module_title = "Laporan Persediaan";
		
		$this->model_object = $this->trans_inventory_barang_model;
		
		$this->view_report = $this->module_name."/report";
		
		$this->report_title = "Laporan Persediaan";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("RPERSEDIAAN", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}

	public function index($data = null)
	{	
		$this->module_subtitle = "Report";
		$data['title'] = $this->web_name.' | '.$this->module_title;
		$data['content'] = $this->view_report;		
		$this->load->view('parts/template',$data);
	}

	public function generate_report($data = null)
	{
		$data = $this->common_library->getData();

		$this->load->library('datetime_library');
		$this->load->library('pdf');	
		
		$marginX = 12;
		$marginY = 12;
		$paperW = 210; 
		$paperH = 297; 
		
		$this->pdf->fontpath = 'assets/fonts/pdf/'; 
		$this->pdf->AddFont('Calibri');
		$this->pdf->AddFont('Calibri-Bold','','calibrib.php');
		$this->pdf->AliasNbPages();
		$this->pdf->Open();
		$this->pdf->SetAutoPageBreak(true, '10');

		//$report_data = $this->model_object->getMutasiDetail($data['tanggal_dari'], $data['tanggal_ke']);
		$report_data = $this->model_object->getMutasiDetailWithTransInfo($data['tanggal_dari'], $data['tanggal_ke']);

		$this->generate_report_header($this->pdf, $data);
		
		$no = 1;
		$fontSize = 10;
		$titleFontSize = 9;
		foreach($report_data as $rd){
			if($no != 0 && $no % 49 == 0){
				$this->generate_report_header($this->pdf, $data);
			}
			$this->pdf->SetFont('Calibri','',$fontSize);
			
			$this->pdf->Cell(10, 5, $no, 1, 0, 'C', true);
			$this->pdf->Cell(20, 5, $rd['kode_barang'], 1, 0, 'C', true);
			//$this->pdf->Cell(30, 5, $rd['comment'], 1, 0, 'C', true);
			$this->pdf->Cell(30, 5, $rd['faktur_pajak'], 1, 0, 'C', true);
			$this->pdf->Cell(35, 5, $rd['trans_date'], 1, 0, 'C', true);
			$this->pdf->SetFont('Calibri-Bold','',$titleFontSize);
			$this->pdf->Cell(20, 5, $rd['saldo_awal'], 1, 0, 'C', true);
			$this->pdf->Cell(20, 5, $rd['jumlah_masuk'], 1, 0, 'C', true);
			$this->pdf->Cell(20, 5, $rd['jumlah_keluar'], 1, 0, 'C', true);
			$this->pdf->Cell(20, 5, $rd['saldo_akhir'], 1, 0, 'C', true);
			$this->pdf->SetFont('Calibri','',$titleFontSize);
			$this->pdf->Cell(15, 5, $rd['kode_gudang'], 1, 1, 'C', true);
			
			
			$no++;
		}
		
		$this->pdf->Ln(5);
		$this->pdf->Output();
	}
	
	public function generate_report_header($obj, $data)
	{
		$obj->AddPage();		
		
		$title = $this->report_title;
		$titleFontSize = 18;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(0, 0,strtoupper($title), 0, 0, 'C');
		$obj->Ln(8);

		$titleFontSize = 10;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->Cell(30, 5, 'Periode', 0, 0, 'L');
		$fontSize = 10;
		$obj->SetFont('Calibri','',$fontSize);
		$obj->Cell(0, 5, ': '.$data['tanggal_dari'].' - '.$data['tanggal_ke'], 0, 1, 'L');	
		
		$obj->Ln(5);

		$titleFontSize = 9;
		$obj->SetFont('Calibri-Bold','',$titleFontSize);
		$obj->SetFillColor(200,200,200);
		$obj->Cell(10, 5, 'No', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Kode Barang', 1, 0, 'C', true);
		$obj->Cell(30, 5, 'Keterangan', 1, 0, 'C', true);
		$obj->Cell(35, 5, 'Tanggal', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Saldo Awal', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Pemasukan', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Pengeluaran', 1, 0, 'C', true);
		$obj->Cell(20, 5, 'Saldo Akhir', 1, 0, 'C', true);
		$obj->Cell(15, 5, 'Gudang', 1, 1, 'C', true);
		
		$obj->SetFillColor(255,255,255);
		$obj->SetFont('Calibri','',$fontSize);

	}

}