<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mprofile extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		
		$this->module_name = "mprofile";
		$this->module_title = "Master Profile";
		$this->table_name = "master_user";
		$this->model_object = $this->master_user_model;
		
		$this->link_back = $this->module_name;
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		
		$this->view_edit = $this->module_name."/edit";
		
		$this->msg_edit_success = "Edit Setting Success.";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
	}
	
	public function index($data = null)
	{
		$this->edit($this->session_user_id, $data);
	}
	
	public function edit_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}		
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$fillable_value = $this->model_object->getFillableValueList();
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
					$this->model_object->setCond($content_cond);	
					$content_value = $this->model_object->getHeaderArray();
					$content_value["password"] = md5($data["password"]);
					$content_value["modified_by"] = $this->session_user_id;
					$content_value["modified_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_edit_success);
			redirect($this->link_back);
		} else {
			$this->edit($id, $data);
		}
	}
}