<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tkasbondtl extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('trans_cash_bank_model');
		$this->load->model('master_rekening_bank_model');
		$this->load->model('master_type_cash_bank_model');
		//$this->load->model('trans_history_bank_model');

		$this->module_name = "tkasbondtl";
		$this->module_title = "Cash Bank";
		$this->table_name = "trans_cash_bank";
		$this->model_object = $this->trans_cash_bank_model;

		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";
		
		$this->msg_add_success = "Create Cash Bank Success.";
		$this->msg_edit_success = "Edit Cash Bank Success.";
		$this->msg_delete_success = "Delete Cash Bank Success.";
		
		$this->trans_prefix = "KAS";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("KASBONDTL", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}

	public function bank($bank = null, $data = null)
	{
		$this->module_subtitle = "List";
		$content_cond = array($this->table_name.'.record_status' => STATUS_ACTIVE);
		$this->model_object->setCond($content_cond);
		$data[$this->module_name.'_list'] = $this->model_object->getListArray();
		$data['bank_list'] = $this->master_rekening_bank_model->getActiveList();
		if(!isset($bank) || $bank == null){
			$bank = $data['bank_list'][0]->id;
		}
		$data['bank'] = $bank;
		$data['title'] = $this->web_name.' | '.$this->module_title;	
		$data['content'] = $this->view_list;		
		$this->load->view('parts/template',$data);
	}

	public function get_data_list_active_json($bank){	
		$request_data= $_REQUEST;
		$column = $this->model_object->getDatatableShowValueList($bank);
		$total_data = $this->model_object->getCountDataBankActive($bank);
		$total_filtered = $this->model_object->getDataTableCountBankActive($bank, $request_data);
		$data_filtered = $this->model_object->getDataTableDataBankActive($bank, $request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}




}