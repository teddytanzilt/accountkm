<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apitrans extends Common_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('common_library');  
		$this->load->model('master_user_model');
		$this->load->model('master_barang_model');
		$this->load->model('trans_sales_invoice_detail_model');
		$this->load->model('trans_purchase_order_header_model');
		$this->load->model('trans_purchase_order_detail_model');
	}

  	function ajax_pending_export_inv(){
        //header('Content-type: application/json');
        $array = array();
        $query = '%'.$_GET['q'].'%';
        $invoice_detail_array = $this->trans_sales_invoice_detail_model->getSearchExporTransDetailCompositeByHeaderBarang($query);
        foreach ($invoice_detail_array as $il) {
			$array[] = array ('id'=>$il['id'], 'text'=>$il['no_transaksi'].' - '.$il['text_nama_barang']);
        }
        echo json_encode($array);
    }

    function ajax_export_inv_detail_info(){
		$id = $this->input->post('id');
		header('Content-type: application/json');

		$invoice_detail_array = $this->trans_sales_invoice_detail_model->getExporTransDetailComposite($id);
		
		$array = array('jumlah' => $invoice_detail_array['jumlah'], 'barang' => $invoice_detail_array['text_nama_barang'] );
		echo json_encode($array);
	}

    function ajax_update_export_inv(){
        //header('Content-type: application/json');
        $id = '%'.$_GET['q'].'%';
        $result['validation'] = true;
        $result['message'] = "";
        /*********Validation starts here ***********/		
		$trans_sales_invoice_detail_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->trans_sales_invoice_detail_model->setCond($trans_sales_invoice_detail_cond);
		if(!$this->trans_sales_invoice_detail_model->checkExist()){
			$result['validation'] = false;
			$result['message'] = "Invalid Content Id.";
		}
		if($result['validation']){
			$trans_sales_invoice_detail_cond = array('record_status' => STATUS_ACTIVE, 'detail_status' => STATUS_COMPLETE, 'id' => $id);
			$this->trans_sales_invoice_detail_model->setCond($trans_sales_invoice_detail_cond);
			if($this->trans_sales_invoice_detail_model->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Content has been used previously.";
			}
		}
		/*********Validation ends here ***********/	
        if($result['validation']){
			try {
				$this->db->trans_begin();
				while(true) {
					$trans_sales_invoice_detail_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
					$this->trans_sales_invoice_detail_model->setCond($trans_sales_invoice_detail_cond);	
					$trans_sales_invoice_detail_value = $this->trans_sales_invoice_detail_model->getHeaderArray();
					$trans_sales_invoice_detail_value["detail_status"] = STATUS_COMPLETE;
					$trans_sales_invoice_detail_value["modified_by"] = "API";
					$trans_sales_invoice_detail_value["modified_on"] = date('Y-m-d H:i:s');
					$this->trans_sales_invoice_detail_model->setValueList($trans_sales_invoice_detail_value);		
					$this->trans_sales_invoice_detail_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$result['message'] = "Update success";

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}

        $array = array('status' => $result['validation'], 'message' => $result['message']);
		echo json_encode($array);
    }

    function ajax_pending_export_po(){
        //header('Content-type: application/json');
        $array = array();
        $query = '%'.$_GET['q'].'%';
        $po_detail_array = $this->trans_purchase_order_detail_model->getSearchExporTransDetailCompositeByHeaderBarang($query);
        foreach ($po_detail_array as $il) {
			$array[] = array ('id'=>$il['id'], 'text'=>$il['no_transaksi'].' - '.$il['text_nama_barang']);
        }
        echo json_encode($array);
    }

    function ajax_export_po_detail_info(){
		$id = $this->input->post('id');
		header('Content-type: application/json');

		$po_detail_array = $this->trans_purchase_order_detail_model->getExporTransDetailComposite($id);
		
		$array = array('jumlah' => $po_detail_array['jumlah'], 'barang' => $po_detail_array['text_nama_barang'] );
		echo json_encode($array);
	}

    function ajax_update_export_po(){
        //header('Content-type: application/json');
        $id = '%'.$_GET['q'].'%';
        $result['validation'] = true;
        $result['message'] = "";
        /*********Validation starts here ***********/		
		$trans_purchase_order_detail_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->trans_purchase_order_detail_model->setCond($trans_purchase_order_detail_cond);
		if(!$this->trans_purchase_order_detail_model->checkExist()){
			$result['validation'] = false;
			$result['message'] = "Invalid Content Id.";
		}
		if($result['validation']){
			$trans_purchase_order_detail_cond = array('record_status' => STATUS_ACTIVE, 'detail_status' => STATUS_COMPLETE, 'id' => $id);
			$this->trans_purchase_order_detail_model->setCond($trans_purchase_order_detail_cond);
			if($this->trans_purchase_order_detail_model->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Content has been used previously.";
			}
		}
		/*********Validation ends here ***********/	
        if($result['validation']){
			try {
				$this->db->trans_begin();
				while(true) {
					$trans_purchase_order_detail_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
					$this->trans_purchase_order_detail_model->setCond($trans_purchase_order_detail_cond);	
					$trans_purchase_order_detail_value = $this->trans_purchase_order_detail_model->getHeaderArray();
					$trans_purchase_order_detail_value["detail_status"] = STATUS_COMPLETE;
					$trans_purchase_order_detail_value["modified_by"] = "API";
					$trans_purchase_order_detail_value["modified_on"] = date('Y-m-d H:i:s');
					$this->trans_purchase_order_detail_model->setValueList($trans_purchase_order_detail_value);		
					$this->trans_purchase_order_detail_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$result['message'] = "Update success";

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}

        $array = array('status' => $result['validation'], 'message' => $result['message']);
		echo json_encode($array);
    }

    function ajax_add_po(){
		$data = $this->common_library->getData();
		/*foreach($_POST as $arr_name => $arr_value){
			if (is_array($arr_value)) {
				$data[$arr_name] = $arr_value;
			} elseif($arr_name == "content" || $arr_name == "description" || $arr_name == "product_content" || $arr_name == "social_media_link" || $arr_name == "tanggal"){
				$data[$arr_name] = $arr_value;
			} elseif((strpos($arr_name, "jumlah") !== false) || (strpos($arr_name, "harga") !== false)){
				$data[$arr_name] = clean_value_harga($arr_value);
			} else {
				$data[$arr_name] = clean_value($arr_value);
			}
		}	*/

//var_dump($data['barang']); exit;
		$result['validation'] = true;
        $result['message'] = "";
        /*********Validation starts here ***********/
		if($result['validation']) {
			if(count($data['barang']) > 10){
				$result['validation'] = false;
				$result['message'] = "Jumlah barang tidak boleh lebih dari 10";
			}
		}
		//var_dump($data); exit;
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$year = intval(date("Y",strtotime($data['tanggal'])));
					$month = intval(date("m",strtotime($data['tanggal'])));
					$next_id = $this->trans_purchase_order_header_model->nextDynamicTrans($year, $month);
					//$next_id = $this->trans_purchase_order_header_model->nextTrans();
					$fillable_value = $this->trans_purchase_order_header_model->getFillableValueList();
					$content_value = $this->trans_purchase_order_header_model->getValueList();
					$content_value['no_transaksi'] = '';
					//$content_value['tanggal'] = date('Y-m-d H:i:s');
					$content_value['tanggal'] = $data['tanggal'].' '.date('H:i:s');
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value['sisa'] = $data['total'];
					$content_value["created_by"] = 0;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->trans_purchase_order_header_model->setValueList($content_value);		
					$this->trans_purchase_order_header_model->insertHeader();
					$content_id = $this->db->insert_id();
					$data['id'] = $content_id;
					$no_transaksi = $this->trans_prefix.date("Y",strtotime($data['tanggal'])).date("m",strtotime($data['tanggal'])).sprintf("%05d", $next_id);
					$content_value['id'] = $content_id;
					$content_value['no_transaksi'] = $no_transaksi;
					$content_value['trans_status'] = STATUS_PENDING;
					$this->trans_purchase_order_header_model->setValueList($content_value);		
					$value_condition = array('id' => $content_id);
					$this->trans_purchase_order_header_model->setCond($value_condition);
					$this->trans_purchase_order_header_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					if(isset($data['barang'])){
						for($x = 0 ; $x < count($data['barang']) ; $x++){
							$this->trans_purchase_order_detail_model->refreshValueList();
							$fillable_value = $this->trans_purchase_order_detail_model->getFillableValueList();
							$content_value = $this->trans_purchase_order_detail_model->getValueList();
							$content_value['header_id'] = $content_id;
							foreach($fillable_value as $fv){
								$content_value[$fv] = $data[$fv][$x];
							}
							$content_value["created_by"] = 0;
							$content_value["created_on"] = date('Y-m-d H:i:s');
							$this->trans_purchase_order_detail_model->setValueList($content_value);		
							$this->trans_purchase_order_detail_model->insertHeader();
							if ($this->db->trans_status() === FALSE){ break; }
						}
						if ($this->db->trans_status() === FALSE){ break; }
					}
					
					//$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		$array = array('status' => $result['validation'], 'message' => $result['message']);
		echo json_encode($array);
    }

    function ajax_add_inv(){
    	$data = $this->common_library->getData();
		$result['validation'] = true;
        $result['message'] = "";
        /*********Validation starts here ***********/
		if($result['validation']) {
			if(count($data['barang']) > 10){
				$result['validation'] = false;
				$result['message'] = "Jumlah barang tidak boleh lebih dari 10";
			}
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$year = intval(date("Y",strtotime($data['tanggal'])));
					$month = intval(date("m",strtotime($data['tanggal'])));
					$next_id = $this->model_object->nextDynamicTrans($year, $month);
					//$next_id = $this->model_object->nextTrans();
					$fillable_value = $this->model_object->getFillableValueList();
					$content_value = $this->model_object->getValueList();
					$content_value['no_transaksi'] = '';
					//$content_value['tanggal'] = date('Y-m-d H:i:s');
					$content_value['tanggal'] = $data['tanggal'].' '.date('H:i:s');
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value['sisa'] = $data['total'];
					$content_value["created_by"] = $this->session_user_id;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->insertHeader();
					$content_id = $this->db->insert_id();
					$data['id'] = $content_id;
					$no_transaksi = $this->trans_prefix.date("Y",strtotime($data['tanggal'])).date("m",strtotime($data['tanggal'])).sprintf("%05d", $next_id);
					$content_value['id'] = $content_id;
					$content_value['no_transaksi'] = $no_transaksi;
					$content_value['trans_status'] = STATUS_PENDING;
					$this->model_object->setValueList($content_value);		
					$value_condition = array('id' => $content_id);
					$this->model_object->setCond($value_condition);
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }
					
					if(isset($data['barang'])){
						for($x = 0 ; $x < count($data['barang']) ; $x++){
							$this->model_detail_object->refreshValueList();
							$fillable_value = $this->model_detail_object->getFillableValueList();
							$content_value = $this->model_detail_object->getValueList();
							$content_value['header_id'] = $content_id;
							foreach($fillable_value as $fv){
								$content_value[$fv] = $data[$fv][$x];
							}
							$content_value["created_by"] = $this->session_user_id;
							$content_value["created_on"] = date('Y-m-d H:i:s');
							$this->model_detail_object->setValueList($content_value);		
							$this->model_detail_object->insertHeader();
							if ($this->db->trans_status() === FALSE){ break; }
						}
						if ($this->db->trans_status() === FALSE){ break; }
					}
					
					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		$array = array('status' => $result['validation'], 'message' => $result['message']);
		echo json_encode($array);
    }

}