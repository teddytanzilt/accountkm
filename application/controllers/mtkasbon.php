<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mtkasbon extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('master_type_cash_bank_model');
		
		$this->module_name = "mtkasbon";
		$this->module_title = "Master Type Cash Bank";
		$this->table_name = "master_type_cash_bank";
		$this->model_object = $this->master_type_cash_bank_model;
		$this->validation_object = array('kode');
		
		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";
		
		$this->msg_add_success = "Create Type Cash Bank Success.";
		$this->msg_edit_success = "Edit Type Cash Bank Success.";
		$this->msg_delete_success = "Delete Type Cash Bank Success.";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("TKASBON", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}
	
}