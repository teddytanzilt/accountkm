<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mrole extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('master_role_model');
		$this->load->model('master_module_model');
		$this->load->model('master_role_module_model');

		$this->module_name = "mrole";
		$this->module_title = "Master Role";
		$this->table_name = "master_role";
		$this->model_object = $this->master_role_model;
		$this->validation_object = array('kode');
		
		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";
		
		$this->msg_add_success = "Create Role Success.";
		$this->msg_edit_success = "Edit Role Success.";
		$this->msg_delete_success = "Delete Role Success.";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("ROLE", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}
	
	public function create_load_data($data = null) {
		$data['module_list'] = $this->master_module_model->getActiveList();
		return $data;
	}
	
	public function create_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Data Exist : ".$data[$vo];
					}
				}
			}
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$fillable_value = $this->model_object->getFillableValueList();
					$content_value = $this->model_object->getValueList();
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["created_by"] = $this->session_user_id;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->insertHeader();
					$content_id = $this->db->insert_id();
					if ($this->db->trans_status() === FALSE){ break; }

					if(isset($data['module'])){
						foreach($data['module'] as $m){
							$role_module_value = $this->master_role_module_model->getValueList();
							$role_module_value['id_role'] = $content_id;
							$role_module_value['id_module'] = $m;
							$this->master_role_module_model->setValueList($role_module_value);		
							$this->master_role_module_model->insertHeader();
							if ($this->db->trans_status() === FALSE){ break; }
						}
						if ($this->db->trans_status() === FALSE){ break; }
					}

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_add_success);
			redirect($this->link_back);
		} else {
			$this->create($data);
		}
	}
	
	public function edit_load_data($data = null) {
		$data['module_list'] = $this->master_module_model->getActiveList();
		$role_module_cond = array('id_role' => $data['id']);
		$this->master_role_module_model->setCond($role_module_cond);	
		$role_module_list = $this->master_role_module_model->getList();
		$selected_module = array();
		foreach($role_module_list as $m){
			$selected_module[] = $m->id_module;
		}
		$data['selected_module'] = $selected_module;
		return $data;
	}
	
	public function edit_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo], 'id !=' => $data['id']);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$fillable_value = $this->model_object->getFillableValueList();
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
					$this->model_object->setCond($content_cond);	
					$content_value = $this->model_object->getHeaderArray();
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["modified_by"] = $this->session_user_id;
					$content_value["modified_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$role_module_cond = array('id_role' => $data['id']);
					$this->master_role_module_model->setCond($role_module_cond);	
					$this->master_role_module_model->deleteHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					foreach($data['module'] as $m){
						$role_module_value = $this->master_role_module_model->getValueList();
						$role_module_value['id_role'] = $data['id'];
						$role_module_value['id_module'] = $m;
						$this->master_role_module_model->setValueList($role_module_value);		
						$this->master_role_module_model->insertHeader();
						if ($this->db->trans_status() === FALSE){ break; }
					}
					if ($this->db->trans_status() === FALSE){ break; }

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_edit_success);
			redirect($this->link_back);
		} else {
			$this->edit($data['id'], $data);
		}
	}
}