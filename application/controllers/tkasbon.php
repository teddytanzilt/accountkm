<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tkasbon extends Crud_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('trans_cash_bank_model');
		$this->load->model('master_rekening_bank_model');
		$this->load->model('master_type_cash_bank_model');
		//$this->load->model('trans_history_bank_model');

		$this->module_name = "tkasbon";
		$this->module_title = "Cash Bank";
		$this->table_name = "trans_cash_bank";
		$this->model_object = $this->trans_cash_bank_model;

		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";
		
		$this->msg_add_success = "Create Cash Bank Success.";
		$this->msg_edit_success = "Edit Cash Bank Success.";
		$this->msg_delete_success = "Delete Cash Bank Success.";
		
		$this->trans_prefix = "KAS";
		
		if($this->session_library->check_session_auth_exist(FALSE)){
			redirect('home/login');
			exit;
		}
		if(!in_array("KASBON", $this->session->userdata('session_user_module'))){
			redirect('home/dashboard');
			exit;
		}
	}

	public function create_load_data($data = null) {
		$data['bank_list'] = $this->master_rekening_bank_model->getActiveList();
		$data['type_cash_bank_list'] = $this->master_type_cash_bank_model->getActiveList();
		$data['tanggal'] = date('Y-m-d');
		return $data;
	}
	
	public function edit_load_data($data = null) {
		$data['bank_list'] = $this->master_rekening_bank_model->getActiveList();
		$data['type_cash_bank_list'] = $this->master_type_cash_bank_model->getActiveList();
		return $data;
	}

	public function view_load_data($data = null) {
		$data['bank_list'] = $this->master_rekening_bank_model->getActiveList();
		$data['type_cash_bank_list'] = $this->master_type_cash_bank_model->getActiveList();
		return $data;
	}

	public function get_data_list_active_json(){	
		$request_data= $_REQUEST;
		$column = $this->model_object->getDatatableShowValueList();
		$total_data = $this->model_object->getCountDataActive();
		$total_filtered = $this->model_object->getDataTableCountActive($request_data);
		$data_filtered = $this->model_object->getDataTableDataActive($request_data);


		$data = array();
		foreach($data_filtered as $row_id => $row_content){
			$nested_data=array(); 
			foreach($column as $dt_id => $dt_val){
				$nested_data[] = $row_content[$dt_val];
			}
			$data[] = $nested_data;
		}
		$json_data = array(
			"draw"            => intval( $request_data['draw'] ),
			"recordsTotal"    => intval( $total_data ),  
			"recordsFiltered" => intval( $total_filtered ), 
			"data"            => $data  
		);
		echo json_encode($json_data);
	}

	public function create_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}
		//var_dump($data); exit;
		//$result['validation'] = false; //TEST
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$rekening_bank_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['bank']);
					$this->master_rekening_bank_model->setCond($rekening_bank_cond);	
					$rekening_bank_value = $this->master_rekening_bank_model->getHeaderArray();
					if($data['jenis'] == "debit"){
						$rekening_bank_value["saldo"] = intval($rekening_bank_value['saldo']) + intval($data['jumlah']);
					} else if($data['jenis'] == "kredit"){
						$rekening_bank_value["saldo"] = intval($rekening_bank_value['saldo']) - intval($data['jumlah']);
					}
					$rekening_bank_value["modified_by"] = $this->session_user_id;
					$rekening_bank_value["modified_on"] = date('Y-m-d H:i:s');
					$this->master_rekening_bank_model->setValueList($rekening_bank_value);		
					$this->master_rekening_bank_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					if($data['jenis'] == "debit"){
						$data['debit'] = $data['jumlah'];
						$data['kredit'] = 0;
					} else if($data['jenis'] == "kredit"){
						$data['kredit'] = $data['jumlah'];
						$data['debit'] = 0;
					}

					$saldo_awal_bank = $this->trans_cash_bank_model->getSaldoBankPerTanggal($data['tanggal'].' '.date('H:i:s'), $data['bank']);
					$saldo_akhir_bank = intval($saldo_awal_bank) - intval($data['kredit']) + intval($data['debit']);
					
					//$data['saldo_awal'] = $this->model_object->getSaldo();
					$data['saldo_awal'] = $this->model_object->getSaldoPerTanggal($data['tanggal'].' '.date('H:i:s'));
					$data['saldo_akhir'] = intval($data['saldo_awal']) - intval($data['kredit']) + intval($data['debit']);
					$data['saldo_bank'] = $saldo_akhir_bank;
					
					$year = intval(date("Y",strtotime($data['tanggal'])));
					$month = intval(date("m",strtotime($data['tanggal'])));
					$next_id = $this->model_object->nextDynamicTrans($year, $month);
					//$next_id = $this->model_object->nextTrans();
					$fillable_value = $this->model_object->getFillableValueList();
					$content_value = $this->model_object->getValueList();
					$content_value['no_transaksi'] = '';
					//$content_value['tanggal'] = date('Y-m-d H:i:s');
					$content_value['tanggal'] = $data['tanggal'].' '.date('H:i:s');
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["created_by"] = $this->session_user_id;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->insertHeader();
					$content_id = $this->db->insert_id();

					//$no_transaksi = $this->trans_prefix.date('Ym').sprintf("%05d", $next_id);
					//$no_transaksi = date('ym').$rekening_bank_value['kode'].sprintf("%05d", $next_id);
					$no_transaksi = date("y",strtotime($data['tanggal'])).date("m",strtotime($data['tanggal'])).$rekening_bank_value['kode'].sprintf("%05d", $next_id);
					$content_value['id'] = $content_id;
					$content_value['no_transaksi'] = $no_transaksi;
					//if($data['action'] == "Finish"){
						$content_value['trans_status'] = STATUS_COMPLETE;
					//} else {
						//$content_value['trans_status'] = STATUS_PENDING;
					//}
					$this->model_object->setValueList($content_value);		
					$value_condition = array('id' => $content_id);
					$this->model_object->setCond($value_condition);
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$updateAmount = intval($data['debit']) - intval($data['kredit']);
					$this->trans_cash_bank_model->updateCashBank($updateAmount, $content_value['tanggal'].' '.date('H:i:s'), $content_id);
					if ($this->db->trans_status() === FALSE){ break; }

					$updateAmountBank = intval($data['debit']) - intval($data['kredit']);
					$this->trans_cash_bank_model->updateBankCashBank($updateAmountBank, $data['tanggal'].' '.date('H:i:s'), $content_id, $data['bank']);
					if ($this->db->trans_status() === FALSE){ break; }

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_add_success);
			redirect($this->link_back);
		} else {
			$this->create($data);
		}
	}


	public function edit_process()
	{
		$data = $this->common_library->getData();
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		if($result['validation']) {
			if(!empty($this->validation_object)){
				foreach($this->validation_object as $vo){
					$content_cond = array('record_status' => STATUS_ACTIVE, $vo => $data[$vo]);
					$this->model_object->setCond($content_cond);
					if($this->model_object->checkExist()){
						$result['validation'] = false;
						$result['message'] = "Column Exist : ".$vo;
					}
				}
			}
		}	
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/
		if($result['validation']) {
			try {
				$this->db->trans_begin();
				while(true) {
					$fillable_value = $this->model_object->getFillableValueList();
					$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $data['id']);
					$this->model_object->setCond($content_cond);	
					$content_value = $this->model_object->getHeaderArray();
					foreach($fillable_value as $fv){
						$content_value[$fv] = $data[$fv];
					}
					$content_value["modified_by"] = $this->session_user_id;
					$content_value["modified_on"] = date('Y-m-d H:i:s');
					if($data['action'] == "Finish"){
						$content_value['trans_status'] = STATUS_COMPLETE;
					} else {
						$content_value['trans_status'] = STATUS_PENDING;
					}
					$this->model_object->setValueList($content_value);		
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					if($data['action'] == "Finish"){
						//update status trans laen yg di lock
					}

					if(isset($data['no_container'])){
						$content_detail_cond = array('header_id' => $data['id']);
						$this->model_detail_object->setCond($content_detail_cond);	
						$this->model_detail_object->deleteHeader();
						if ($this->db->trans_status() === FALSE){ break; }

						for($x = 0 ; $x < count($data['no_container']) ; $x++){
							$this->model_detail_object->refreshValueList();
							$fillable_value = $this->model_detail_object->getFillableValueList();
							$content_value = $this->model_detail_object->getValueList();
							$content_value['header_id'] = $data['id'];
							foreach($fillable_value as $fv){
								$content_value[$fv] = $data[$fv][$x];
							}
							$content_value["created_by"] = $this->session_user_id;
							$content_value["created_on"] = date('Y-m-d H:i:s');
							$this->model_detail_object->setValueList($content_value);		
							$this->model_detail_object->insertHeader();
							if ($this->db->trans_status() === FALSE){ break; }
						}
						if ($this->db->trans_status() === FALSE){ break; }
					}

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		$data = array_merge($data, $result);
		/*********Process ends here ***********/	
		if($result['validation']) {
			$this->session->set_flashdata("success_message", $this->msg_edit_success);
			redirect($this->link_back);
		} else {
			$this->edit($data['id'], $data);
		}
	}

	public function cancel_process($id)
	{
		$result = array("validation" => true, "message" => "", "data_json" => array());
		/*********Validation starts here ***********/		
		$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
		$this->model_object->setCond($content_cond);
		if(!$this->model_object->checkExist()){
			$result['validation'] = false;
			$result['message'] = "Invalid Content Id.";
		}
		if($result['validation']){
			$content_cond = array('record_status' => STATUS_DELETE, 'id' => $id);
			$this->model_object->setCond($content_cond);
			if($this->model_object->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Content has been deleted previously.";
			}
		}
		if($result['validation']){
			$content_cond = array('trans_status' => STATUS_CANCEL, 'id' => $id);
			$this->model_object->setCond($content_cond);
			if($this->model_object->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Content has been cancel previously.";
			}
		}
		if($result['validation']){
			$content_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id, 'reff_id IS NOT NULL' => NULL);
			$this->model_object->setCond($content_cond);
			if($this->model_object->checkExist()){
				$result['validation'] = false;
				$result['message'] = "Pembayaran PO dan Invoice tidak dapat di cancel.";
			}
		}
		/*********Validation ends here ***********/	
		/*********Process starts here ***********/	
		if($result['validation']){
			try {
				$this->db->trans_begin();
				while(true) {
					$cash_bank_cond = array('record_status' => STATUS_ACTIVE, 'id' => $id);
					$this->trans_cash_bank_model->setCond($cash_bank_cond);	
					$cash_bank_value = $this->trans_cash_bank_model->getHeaderArray();
					$cash_bank_value["trans_status"] = STATUS_CANCEL;
					$cash_bank_value["modified_by"] = $this->session_user_id;
					$cash_bank_value["modified_on"] = date('Y-m-d H:i:s');
					$this->trans_cash_bank_model->setValueList($cash_bank_value);		
					$this->trans_cash_bank_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$jenis = "debit";
					$jumlah = 0;
					if($cash_bank_value['debit'] != 0){
						$jenis = "debit";
						$jumlah = $cash_bank_value['debit'];
					} else if($cash_bank_value['kredit'] != 0){
						$jenis = "kredit";
						$jumlah = $cash_bank_value['kredit'];
					}

					$rekening_bank_cond = array('record_status' => STATUS_ACTIVE, 'id' => $cash_bank_value['bank']);
					$this->master_rekening_bank_model->setCond($rekening_bank_cond);	
					$rekening_bank_value = $this->master_rekening_bank_model->getHeaderArray();

					if($jenis == "debit"){
						$rekening_bank_value["saldo"] = intval($rekening_bank_value['saldo']) - intval($jumlah);
					} else if($jenis == "kredit"){
						$rekening_bank_value["saldo"] = intval($rekening_bank_value['saldo']) + intval($jumlah);
					}
					$rekening_bank_value["modified_by"] = $this->session_user_id;
					$rekening_bank_value["modified_on"] = date('Y-m-d H:i:s');
					$this->master_rekening_bank_model->setValueList($rekening_bank_value);		
					$this->master_rekening_bank_model->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$second_added = 30;
					$tanggal = $cash_bank_value['tanggal'];
					$dTanggal = new DateTime($tanggal);
					$dTanggal->add(new DateInterval('PT'.$second_added.'S'));
					$tanggal = $dTanggal->format('Y-m-d H:i:s');

					$saldo_awal_bank = $this->trans_cash_bank_model->getSaldoBankPerTanggal($tanggal, $cash_bank_value['bank']);
					$saldo_akhir_bank = intval($saldo_awal_bank) - intval($cash_bank_value['debit']) + intval($cash_bank_value['kredit']);

					$data['saldo_awal'] = $this->model_object->getSaldoPerTanggal($tanggal.' '.date('H:i:s'));
					$data['saldo_akhir'] = intval($data['saldo_awal']) - intval($cash_bank_value['debit']) + intval($cash_bank_value['kredit']);
					$data['saldo_bank'] = $saldo_akhir_bank;
					$year = intval(date("Y",strtotime($tanggal)));
					$month = intval(date("m",strtotime($tanggal)));
					$next_id = $this->model_object->nextDynamicTrans($year, $month);
					//$next_id = $this->model_object->nextTrans();
					$fillable_value = $this->model_object->getFillableValueList();
					$this->model_object->refreshValueList();
					$content_value = $this->model_object->getValueList();
					$content_value['no_transaksi'] = '';
					//$content_value['tanggal'] = date('Y-m-d H:i:s');
					$content_value['tanggal'] = $tanggal;

					$content_value['bank'] = $cash_bank_value['bank'];
					$content_value['type'] = $cash_bank_value['type'];
					$content_value['kredit'] = $cash_bank_value['debit'];
					$content_value['debit'] = $cash_bank_value['kredit'];
					$content_value['saldo_awal'] = $data['saldo_awal'];
					$content_value['saldo_akhir'] = $data['saldo_akhir'];
					$content_value['saldo_bank'] = $data['saldo_bank'];
					$content_value['note'] = "Cancel ".$cash_bank_value['note'];
					$content_value['cek_giro'] = $cash_bank_value['cek_giro'];
					$content_value['trans_id'] = $id;
					
					$content_value["created_by"] = $this->session_user_id;
					$content_value["created_on"] = date('Y-m-d H:i:s');
					$this->model_object->setValueList($content_value);		
					$this->model_object->insertHeader();
					$content_id = $this->db->insert_id();

					//$no_transaksi = $this->trans_prefix.date('Ym').sprintf("%05d", $next_id);
					//$no_transaksi = date('ym').$rekening_bank_value['kode'].sprintf("%05d", $next_id);
					$no_transaksi = date("y",strtotime($tanggal)).date("m",strtotime($tanggal)).$rekening_bank_value['kode'].sprintf("%05d", $next_id);
					$content_value['id'] = $content_id;
					$content_value['no_transaksi'] = $no_transaksi;
					$content_value['trans_status'] = STATUS_CANCEL;
					$this->model_object->setValueList($content_value);		
					$value_condition = array('id' => $content_id);
					$this->model_object->setCond($value_condition);
					$this->model_object->updateHeader();
					if ($this->db->trans_status() === FALSE){ break; }

					$updateAmount = intval($cash_bank_value['kredit']) - intval($cash_bank_value['debit']);
					$this->trans_cash_bank_model->updateCashBank($updateAmount, $tanggal, $content_id);
					if ($this->db->trans_status() === FALSE){ break; }

					$updateAmountBank = intval($cash_bank_value['kredit']) - intval($cash_bank_value['debit']);
					$this->trans_cash_bank_model->updateBankCashBank($updateAmountBank, $tanggal, $content_id, $cash_bank_value['bank']);
					if ($this->db->trans_status() === FALSE){ break; }

					$this->log_library->writeLog($result);
					
					break;
				}
				if ($this->db->trans_status() === FALSE){	
					$result['validation'] = false;
					$result['message'] = $this->db->_error_number()." : ".$this->db->_error_message();
					$this->db->trans_rollback();				
				} else {
					$this->db->trans_commit();				
				}
			} catch (Exception $e) {
				$result['validation'] = false;
				$result['message'] = $e->getMessage();
				$this->db->trans_rollback();	
			}
		}
		/*********Process ends here ***********/	
		if($result['validation']){
			$this->session->set_flashdata("success_message", "Success");
			redirect(base_url().$this->link_back);
		} else {
			$this->index($result);
		}
	}

	function ajax_order_detail_pending(){
		//header('Content-type: application/json');
		$array = array();
		//$trans_order_header_cond = array('pembayaran_kapal_status' => STATUS_PENDING);
		//$this->trans_order_header_model->setCond($trans_order_header_cond);			
		$order_list = $this->trans_order_detail_model->getActiveList();
		foreach ($order_list as $ol) {
			$array[] = array ('id'=>$ol->id, 'text'=>$ol->no_container);
		}
		echo json_encode($array);
	}

	function ajax_order_list_info(){
		$order = $this->input->post('order');
		$customer = $this->input->post('customer');
		header('Content-type: application/json');
		$order_detail_list = $this->trans_order_detail_model->getTransDetailCompositeByHeaderCustomer($order, $customer);
		$array = array('order_detail' => $order_detail_list);
		echo json_encode($array);
	}

}