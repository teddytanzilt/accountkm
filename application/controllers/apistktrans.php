<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apistktrans extends REST_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('master_user_model');
		$this->load->model('master_barang_model');
		$this->load->model('master_satuan_model');
		
		$this->module_name = "mbarang";
		$this->module_title = "Master Barang";
		$this->table_name = "master_barang";
		$this->model_object = $this->master_barang_model;
		$this->validation_object = array('kode');
		
		$this->link_back = $this->module_name;
		$this->link_add = $this->module_name."/create";
		$this->link_add_submit = $this->module_name."/create_process";
		$this->link_edit = $this->module_name."/edit/";
		$this->link_edit_submit = $this->module_name."/edit_process";
		$this->link_view = $this->module_name."/view/";
		$this->link_delete = $this->module_name."/delete_process/";
		
		$this->view_list = $this->module_name."/list";
		$this->view_edit = $this->module_name."/edit";
		$this->view_add = $this->module_name."/add";
		$this->view_view = $this->module_name."/view";
		
		$this->msg_add_success = "Create Barang Success.";
		$this->msg_edit_success = "Edit Barang Success.";
		$this->msg_delete_success = "Delete Barang Success.";
		
		
	}

    public function index_get()
  	{
  		/*$this->response([
		'returned from get:' => 'ad',
	]);*/
		//$this->response($this->master_user_model->getList());

		$this->response([
			'param' => $this->get('param'),
			'X' => $this->master_user_model->getList(),
			'Y' => $this->master_barang_model->getList()
		]);
    	
    	// Display all books
  	}

  	public function index_post()
  	{
  		$this->response([
			'satuan' => $this->master_user_model->getList(),
			'param' => $this->post('param')
		]);
    	// Create a new book
  	}


  	public function teddy_post()
  	{
  		/*$this->response([
		'returned from get:' => 'ad',
	]);*/
		//$this->response($this->master_user_model->getList());

		$this->response([
			'param' => $this->post('param').' POST'
		]);
    	
    	// Display all books
  	}

  	public function teddy_get()
  	{
  		/*$this->response([
		'returned from get:' => 'ad',
	]);*/
		//$this->response($this->master_user_model->getList());

		$this->response([
			'param' => $this->input->get('param').' GET'
		]);
    	
    	// Display all books
  	}
}