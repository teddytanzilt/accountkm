CREATE TABLE `trans_cash_bank` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `bank` int(10) NOT NULL,
  `kredit` int(20) NOT NULL, 
  `debit` int(20) NOT NULL, 
  `saldo_awal` int(20) NOT NULL, 
  `saldo_akhir` int(20) NOT NULL,  
  `note` text,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'PENDING',
  PRIMARY KEY (`id`),
  KEY `bank` (`bank`),
  KEY `no_transaksi_status` (`no_transaksi`,`trans_status`),
  CONSTRAINT `tcashbank_bank` FOREIGN KEY (`bank`) REFERENCES `master_rekening_bank` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `master_module` (`kode`, `nama`, `link`, `created_by`, `created_on`, `record_status`) VALUES ('KASBON', 'Cash Bank', 'tkasbon', '1', '2018-09-01 00:00:00', 'ACTIVE');



ALTER TABLE `trans_cash_bank` ADD COLUMN `saldo_bank` INT(20) NULL AFTER `saldo_akhir`;



ALTER TABLE `trans_cash_bank` 
ADD COLUMN `type` INT(10) NOT NULL AFTER `bank`;



INSERT INTO `master_module` (`kode`, `nama`, `link`, `created_by`, `created_on`, `record_status`) VALUES ('SALESINVOICE', 'Sales Invoice', 'tsalesinvoice', '1', '2018-09-01 00:00:00', 'ACTIVE');









CREATE TABLE `trans_sales_invoice_header` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `customer` int(10) NOT NULL,
  `ship_to` varchar(100) DEFAULT NULL,
  `terms` varchar(100) DEFAULT NULL,
  `fob` varchar(100) DEFAULT NULL,
  `ship_via` varchar(100) DEFAULT NULL,
  `ship_date` datetime DEFAULT NULL,
  `po_no` varchar(100) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `note` text,
  `subtotal` int(20) NOT NULL,
  `discount_value` int(20) NOT NULL,
  `ppn_rate` int(20) NOT NULL,
  `ppn_value` int(20) NOT NULL,
  `freight_value` int(20) NOT NULL,
  `total` int(20) NOT NULL,
  `prepared_by` int(10) DEFAULT NULL,
  `prepared_date` datetime NOT NULL,
  `approved_by` int(10) DEFAULT NULL,
  `approved_date` datetime NOT NULL,
  `shipped_by` int(10) DEFAULT NULL,
  `shipped_date` datetime NOT NULL,
  `received_by` int(10) DEFAULT NULL,
  `received_date` datetime NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'PENDING',
  `pembayaran_kapal_status` varchar(20) DEFAULT 'PENDING',
  PRIMARY KEY (`id`),
  KEY `no_transaksi` (`no_transaksi`),
  KEY `customer` (`customer`),
  CONSTRAINT `tinvoiceheader_customer` FOREIGN KEY (`customer`) REFERENCES `master_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1



CREATE TABLE `trans_sales_invoice_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `header_id` int(10) NOT NULL,
  `barang` int(10) NOT NULL,
  `note` text,
  `quantity` int(10) NOT NULL,
  `harga_modal_unit` int(20) NOT NULL,
  `harga_unit` int(20) NOT NULL,
  `discount` int(20) NOT NULL,
  `tax` int(20) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `serial_no` varchar(50)  NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `detail_status` varchar(20) DEFAULT 'PENDING',
  PRIMARY KEY (`id`),
  KEY `header_id` (`header_id`),
  KEY `barang` (`barang`),
  KEY `header_barang_status` (`header_id`,`barang`,`detail_status`),
  CONSTRAINT `tinvoicedetail_barang` FOREIGN KEY (`barang`) REFERENCES `master_barang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tinvoicedetail_header` FOREIGN KEY (`header_id`) REFERENCES `trans_sales_invoice_header` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1



ALTER TABLE `trans_sales_invoice_header` 
CHANGE COLUMN `pembayaran_kapal_status` `pembayaran_status` VARCHAR(20) NULL DEFAULT 'PENDING' ;








CREATE TABLE `master_stock_barang` (
  `id_barang` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL DEFAULT '0',
  `jumlah` int(10) NOT NULL DEFAULT '0',
  `import_flag` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_barang`,`id_gudang`),
  KEY `id_barang` (`id_barang`),
  KEY `id_gudang` (`id_gudang`),
  KEY `barang_gudang` (`id_barang`,`id_gudang`),
  CONSTRAINT `mstockbarang_barang` FOREIGN KEY (`id_barang`) REFERENCES `master_barang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1



CREATE TABLE `trans_inventory_barang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_barang` int(10) NOT NULL,
  `id_gudang` int(10) NOT NULL DEFAULT '0',
  `id_user` int(10) NOT NULL,
  `trans_date` datetime NOT NULL,
  `comment` text NOT NULL,
  `saldo_awal` int(10) NOT NULL DEFAULT '0',
  `jumlah_masuk` int(10) NOT NULL DEFAULT '0',
  `jumlah_keluar` int(10) NOT NULL DEFAULT '0',
  `saldo_akhir` int(10) NOT NULL DEFAULT '0',
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `id_barang` (`id_barang`),
  KEY `id_gudang` (`id_gudang`),
  KEY `id_user` (`id_user`),
  KEY `bahan_baku_gudang` (`id_barang`,`id_gudang`),
  CONSTRAINT `tinventorybarang_barang` FOREIGN KEY (`id_barang`) REFERENCES `master_barang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1


INSERT INTO `master_module` (`id`, `kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES (NULL, 'DELIVERYORDER', 'Delivery Order', 'tdeliveryorder', NULL, '1', '2019-03-07 00:00:00', NULL, NULL, 'ACTIVE');

ALTER TABLE `trans_delivery_order_header` CHANGE `pembayaran_kapal_status` `pembayaran_status` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'PENDING';

ALTER TABLE `trans_sales_invoice_header` ADD COLUMN `delivery_status` VARCHAR(20)  NULL DEFAULT 'PENDING';


ALTER TABLE `trans_delivery_order_header` 
ADD COLUMN `invoice` INT(10) NOT NULL AFTER `customer`;


ALTER TABLE `trans_delivery_order_header` 
ADD INDEX `invoice` (`invoice` ASC);





INSERT INTO `master_module` (`id`, `kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES (NULL, 'RBUKUBESAR', 'Buku Besar', 'rbukubesar', NULL, '1', '2019-03-07 00:00:00', NULL, NULL, 'ACTIVE');



INSERT INTO `master_module` (`id`, `kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES (NULL, 'RBUKUBANK', 'Buku Bank', 'rbukubank', NULL, '1', '2019-03-07 00:00:00', NULL, NULL, 'ACTIVE');






INSERT INTO `master_module` (`id`, `kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES (NULL, 'RHUTANG', 'Hutang', 'rhutang', NULL, '1', '2019-03-07 00:00:00', NULL, NULL, 'ACTIVE');
INSERT INTO `master_module` (`id`, `kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES (NULL, 'RPIUTANG', 'Piutang', 'rpiutang', NULL, '1', '2019-03-07 00:00:00', NULL, NULL, 'ACTIVE');
INSERT INTO `master_module` (`id`, `kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES (NULL, 'RPENJUALAN', 'Penjualan', 'rpenjualan', NULL, '1', '2019-03-07 00:00:00', NULL, NULL, 'ACTIVE');
INSERT INTO `master_module` (`id`, `kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES (NULL, 'RPEMBELIAN', 'Pembelian', 'rpembelian', NULL, '1', '2019-03-07 00:00:00', NULL, NULL, 'ACTIVE');
INSERT INTO `master_module` (`id`, `kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES (NULL, 'RNERACA', 'Neraca', 'rneraca', NULL, '1', '2019-03-07 00:00:00', NULL, NULL, 'ACTIVE');
INSERT INTO `master_module` (`id`, `kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES (NULL, 'RLABARUGI', 'Laba Rugi', 'rlabarugi', NULL, '1', '2019-03-07 00:00:00', NULL, NULL, 'ACTIVE');
INSERT INTO `master_module` (`id`, `kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES (NULL, 'RPERSEDIAAN', 'Persediaan', 'rpersediaan', NULL, '1', '2019-03-07 00:00:00', NULL, NULL, 'ACTIVE');




ALTER TABLE `trans_purchase_order_header` 
CHANGE COLUMN `prepared_by` `prepared_by` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `prepared_date` `prepared_date` DATETIME NULL ,
CHANGE COLUMN `approved_by` `approved_by` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `approved_date` `approved_date` DATETIME NULL ;


ALTER TABLE `trans_sales_invoice_header` 
CHANGE COLUMN `prepared_by` `prepared_by` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `prepared_date` `prepared_date` DATETIME NULL ,
CHANGE COLUMN `approved_by` `approved_by` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `approved_date` `approved_date` DATETIME NULL ,
CHANGE COLUMN `shipped_by` `shipped_by` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `shipped_date` `shipped_date` DATETIME NULL ,
CHANGE COLUMN `received_by` `received_by` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `received_date` `received_date` DATETIME NULL ;


ALTER TABLE `trans_delivery_order_header` 
CHANGE COLUMN `prepared_by` `prepared_by` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `prepared_date` `prepared_date` DATETIME NULL ,
CHANGE COLUMN `approved_by` `approved_by` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `approved_date` `approved_date` DATETIME NULL ,
CHANGE COLUMN `shipped_by` `shipped_by` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `shipped_date` `shipped_date` DATETIME NULL ,
CHANGE COLUMN `received_by` `received_by` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `received_date` `received_date` DATETIME NULL ;










CREATE TABLE `trans_purchase_order_payment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `header_id` int(10) NOT NULL,
  `bank` int(10) NOT NULL,
  `jumlah_bayar` int(20) NOT NULL,
  `tanggal_bayar` datetime NOT NULL,
  `note` text,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `detail_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `header_id` (`header_id`),
  KEY `bank` (`bank`),
  KEY `header_bank_status` (`header_id`,`bank`,`detail_status`),
  CONSTRAINT `tpopayment_bank` FOREIGN KEY (`bank`) REFERENCES `master_rekening_bank` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpopayment_header` FOREIGN KEY (`header_id`) REFERENCES `trans_purchase_order_header` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1





ALTER TABLE `trans_purchase_order_header` ADD COLUMN `sisa` INT(20) NULL AFTER `total`;




CREATE TABLE `trans_sales_invoice_payment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `header_id` int(10) NOT NULL,
  `bank` int(10) NOT NULL,
  `jumlah_bayar` int(20) NOT NULL,
  `tanggal_bayar` datetime NOT NULL,
  `note` text,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `detail_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `header_id` (`header_id`),
  KEY `bank` (`bank`),
  KEY `header_bank_status` (`header_id`,`bank`,`detail_status`),
  CONSTRAINT `tsipayment_bank` FOREIGN KEY (`bank`) REFERENCES `master_rekening_bank` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tsipayment_header` FOREIGN KEY (`header_id`) REFERENCES `trans_sales_invoice_header` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1





ALTER TABLE `trans_sales_invoice_header` ADD COLUMN `sisa` INT(20) NULL AFTER `total`;





ALTER TABLE `trans_sales_invoice_header` ADD COLUMN `faktur_pajak` varchar(45) NULL AFTER `currency`;



ALTER TABLE `trans_cash_bank` 
ADD COLUMN `cek_giro` VARCHAR(60) NULL DEFAULT NULL AFTER `note`;





CREATE TABLE `trans_history_bank` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL,
  `bank` int(10) NOT NULL,
  `kredit` int(20) NOT NULL, 
  `debit` int(20) NOT NULL, 
  `saldo_awal` int(20) NOT NULL, 
  `saldo_akhir` int(20) NOT NULL,  
  `note` text,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'PENDING',
  PRIMARY KEY (`id`),
  KEY `bank` (`bank`),
  CONSTRAINT `thistorybank_bank` FOREIGN KEY (`bank`) REFERENCES `master_rekening_bank` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




ALTER TABLE `trans_delivery_order_header` CHANGE `invoice` `invoice` INT(10) NULL;






ALTER TABLE `trans_sales_invoice_header` 
ADD COLUMN `do` INT(10) NOT NULL AFTER `customer`;



ALTER TABLE `trans_sales_invoice_header` 
ADD INDEX `do` (`do` ASC);


ALTER TABLE `trans_sales_invoice_header` CHANGE `do` `do` INT(10) NULL;








ALTER TABLE `master_barang` 
ADD COLUMN `barang_expor` INT(1) NOT NULL DEFAULT 0 AFTER `satuan_berat`;






ALTER TABLE `trans_cash_bank` ADD COLUMN `reff_id` INT(20) NULL AFTER `cek_giro`;





ALTER TABLE `trans_sales_invoice_header` 
ADD COLUMN `expor` INT(1) NOT NULL DEFAULT 0 AFTER `faktur_pajak`;


ALTER TABLE `trans_purchase_order_header` 
ADD COLUMN `expor` INT(1) NOT NULL DEFAULT 0 AFTER `rate`;
