
		$this->pdf->SetFont('Calibri-Bold','',16);
		$x1 = 9;
		$y1 = 150;
		$w = 190;
		$h = 10; 
		$border = 1; 
		$lineBreak = 1; 
		$text = "Delivery Order";
		$align = "C"; 
		$this->pdf->SetXY ($x1,$y1);
		$this->pdf->Cell($w,$h,$text,$border,$lineBreak,$align);
			
		$name = $company_name;
		$x1 = 10;
		$y1 = 163;
		$this->pdf->SetXY ($x1,$y1);			
		$len = $this->pdf->GetStringWidth( $name );//the width
		$h = 2;			
		$this->pdf->SetFont('Calibri-Bold','',11);
		$this->pdf->Cell( $len, $h, $name);
		
		$x1 = 10;
		$y1 = 168;
		$this->pdf->SetXY ($x1,$y1);
		$w = 30;
		$h=5;
		$this->pdf->SetFont('Calibri','',11);
		$this->pdf->Cell ($w,$h,'Customer',1,'','R');
		$this->pdf->Cell($w+30,$h,$customer_detail['nama'],1,'','C');
		$this->pdf->SetXY ($x1,$y1+$h);
		$this->pdf->Cell ($w,$h,'Ship To',1,'','R');
		$this->pdf->Cell($w+30,$h,$content_detail['ship_to'],1,'','C');
		$this->pdf->SetXY ($x1,$y1+$h+$h);
		$this->pdf->Cell ($w,$h,'Ship Via',1,'','R');
		$this->pdf->Cell($w+30,$h,$content_detail['ship_via'],1,'','C');

		$sNo = $content_detail['no_transaksi'];
		$sDate = $content_detail['tanggal'];
		$sPO = $content_detail['po_no'];
		$x1 = 132;
		$y1 = 168;
		$this->pdf->SetXY ($x1,$y1);
		$w = 33;
		$h=5;
		$this->pdf->SetFont('Calibri','',11);
		$this->pdf->Cell ($w,$h,'Nomor',1,'','R');
		$this->pdf->Cell($w+1,$h,$sNo,1,'','C');
		$this->pdf->SetXY ($x1,$y1+$h);
		$this->pdf->Cell ($w,$h,'Tanggal',1,'','R');
		$this->pdf->Cell($w+1,$h,$sDate,1,'','C');
		$this->pdf->SetXY ($x1,$y1+$h+$h);
		$this->pdf->Cell ($w,$h,'P.O No.',1,'','R');
		$this->pdf->Cell($w+1,$h,$sPO,1,'','C');
		
		
		$x1 = 10;
		$y1 = 188;
		$this->pdf->SetXY ($x1,$y1);
		$this->pdf->SetFont('Calibri-Bold','',11);
		$this->pdf->SetTextColor(255,255,255); 
		$this->pdf->SetFillColor(128,128,128); 
		$this->pdf->Cell(9, 5, 'No', 1, '', 'C',true);
		//$this->pdf->Cell(40, 5, 'Item', 1, '', 'C', true);
		$this->pdf->Cell(140, 5, 'Description', 1, '', 'C', true);
		$this->pdf->Cell(15, 5, 'Qty', 1, '', 'C', true);
		$this->pdf->Cell(25, 5, 'Serial No', 1, '', 'C', true);
		$this->pdf->Ln();
		$this->pdf->SetTextColor(0,0,0); 
		$this->pdf->SetFont('Calibri','',11);
		$num = 1;
		foreach($content_detail_list as $valueBarang){
			$PartNo = $valueBarang['text_kode_barang'];
			$NamaBarang = $valueBarang['text_nama_barang'];
			$Qty = $valueBarang['quantity'];
			$Serial = $valueBarang['serial_no'];
			$this->pdf->Cell(9, 5, $num, 1, '', 'C');
			//$this->pdf->Cell(40, 5, substr($PartNo, 0, 16), 1, '', 'C');
			$this->pdf->Cell(140, 5, substr($NamaBarang, 0, 80), 1, '', 'C');
			$this->pdf->Cell(15, 5, $Qty, 1, '', 'C');
			$this->pdf->Cell(25, 5, $Serial, 1, '', 'C');
			$this->pdf->Ln();	
			$num++;
		}	
		for ($x = $num; $x <= 10; $x++) {
		$this->pdf->Cell(9, 5, $x, 1, '', 'C');
		//$this->pdf->Cell(40, 5, '', 1, '', 'C');
		$this->pdf->Cell(140, 5, '', 1, '', 'C');
		$this->pdf->Cell(15, 5, '', 1, '', 'C');
		$this->pdf->Cell(25, 5, '', 1, '', 'C');
		$this->pdf->Ln();	
		}
		
		$this->pdf->Ln();	
		$this->pdf->SetFont('Calibri','',11);
		
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Cell(48, 5, 'Shipped By', '', '', 'C');
		$this->pdf->Cell(48, 5, 'Received By', '', '', 'C');
		
		$this->pdf->Ln();
		$this->pdf->Ln();
		$this->pdf->Ln();
		
		$x1=19;
		$y1=270;
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		//$this->pdf->Line($x1,$y1,$x1+30,$y1);
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		//$this->pdf->Line($x1+48,$y1,$x1+48+30,$y1);
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Line($x1+48+48,$y1,$x1+48+48+30,$y1);
		$this->pdf->Cell(48, 5, '', '', '', 'C');
		$this->pdf->Line($x1+48+48+48,$y1,$x1+48+48+48+30,$y1);
		