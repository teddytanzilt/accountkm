ALTER TABLE `master_user` 
ADD COLUMN `is_admin` INT(1) NOT NULL DEFAULT 0 AFTER `role`;

UPDATE `master_user` SET `is_admin` = '1' WHERE `master_user`.`id` = 1;


ALTER TABLE `trans_inventory_barang` 
ADD COLUMN `trans_status` VARCHAR(20) NULL DEFAULT 'COMPLETED' AFTER `record_status`;

ALTER TABLE `trans_inventory_barang` ADD COLUMN `reff_id` INT(20) NULL AFTER `saldo_akhir`;



ALTER TABLE `trans_cash_bank` 
ADD COLUMN `trans_id` INT(20) NULL DEFAULT NULL AFTER `reff_id`;





update trans_sales_invoice_detail SET detail_status = 'PENDING' WHERE detail_status = 'ACTIVE'



update trans_purchase_order_detail SET detail_status = 'PENDING' WHERE detail_status = 'ACTIVE'



ALTER TABLE `master_barang` CHANGE `kode` `kode` VARCHAR(20) NOT NULL;




ALTER TABLE `trans_purchase_order_header` ADD COLUMN `faktur_pajak` varchar(45) NULL AFTER `rate`;

INSERT INTO `master_module` (`id`, `kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES (NULL, 'KASBONDTL', 'Cash Bank Detail', 'tkasbondtl', NULL, '1', '', NULL, NULL, 'ACTIVE');






INSERT INTO `master_module` (`kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES ('LMIELIDI', 'Mie Lidi', '#', NULL, '1', '2019-09-25 00:00:00', NULL, NULL, 'ACTIVE');


INSERT INTO `master_module` (`kode`, `nama`, `link`, `import_flag`, `created_by`, `created_on`, `modified_by`, `modified_on`, `record_status`) VALUES ('LTEPUNG', 'Tepung', '#', NULL, '1', '2019-09-25 00:00:00', NULL, NULL, 'ACTIVE');






UPDATE trans_purchase_order_header SET faktur_pajak = rate WHERE faktur_pajak is null


