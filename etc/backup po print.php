$this->pdf->SetFont('Calibri-Bold','',18);
		$x1 = 9;
		$y1 = 15;
		$w = 190;
		$h = 10; 
		$border = 1; 
		$lineBreak = 1; 
		$text = "PURCHASE ORDER";
		$align = "C"; 
		$this->pdf->SetXY ($x1,$y1);
		$this->pdf->Cell($w,$h,$text,$border,$lineBreak,$align);
		
		$name = $company_name;
		$address = " \n".
					" \n".
					" \n".
					"Telp : (061)   \n".
					"Fax  : (061) ";
		$x1 = 10;
		$y1 = 30;
		$this->pdf->SetXY ($x1,$y1);			
		$len = $this->pdf->GetStringWidth( $name );//the width
		$h = 2;			
		$this->pdf->SetFont('Calibri-Bold','',11);
		$this->pdf->Cell( $len, $h, $name);
		$this->pdf->SetXY( $x1, $y1 + 4 );			
		$length = $this->pdf->GetStringWidth( $address );
		$this->pdf->SetFont('Calibri','',11);
		$this->pdf->MultiCell($length, 4, $address);
	
		$tanggal = $this->datetime_library->indonesian_date($this->datetime_library->date_format('2018-11-12'.' 00:00:00', 'l jS F Y'), 'j F Y', '');
		$sDate = $tanggal;
		$sPO = $content_detail['no_transaksi'];
		$pg = '1';
		$x1 = 132;
		$y1 = 30;
		$this->pdf->SetXY ($x1,$y1);
		$w = 33;
		$h=5;
		$this->pdf->SetFont('Calibri','',11);
		$this->pdf->Cell ($w,$h,'P.O. Date',1,'','R');
		$this->pdf->Cell($w+1,$h,$sDate,1,'','C');
		$this->pdf->SetXY ($x1,$y1+$h);
		$this->pdf->Cell ($w,$h,'P.O No.',1,'','R');
		$this->pdf->Cell($w+1,$h,$sPO,1,'','C');
		$this->pdf->SetXY ($x1,$y1+$h+$h);
		$this->pdf->Cell ($w,$h,'Page(s)',1,'','R');
		$this->pdf->Cell($w+1,$h,$pg,1,'','C');
		
		$name = $supplier_detail['nama'];
		$address = $supplier_detail['alamat'];
		$x1 = 11;
		$y1 = 64;
		$this->pdf->SetXY ($x1,$y1);
		$w = 44;
		$h = 5;
		$this->pdf->SetFont('Calibri-Bold','',11);
		$this->pdf->SetTextColor(255,255,255); 
		$this->pdf->SetFillColor(128,128,128); 
		$this->pdf->Cell($w,$h,'Supplier','','','C',true);
		$this->pdf->SetTextColor(0,0,0);
		$x2 = 10;
		$y2 = 70;
		$this->pdf->SetXY ($x2,$y2);
		$len = $this->pdf->GetStringWidth( $name );
		$h = 2;
		$box_width = 90;
		$box_height = 30;
		$this->pdf->Rect($x2,$y2-1,$box_width,$box_height);
		$this->pdf->SetFont('Calibri-Bold','',11);
		$this->pdf->Cell( $len, $h, $name);
		$this->pdf->SetXY( $x2, $y2 + 4 );
		$length = $this->pdf->GetStringWidth( $address );
		$this->pdf->SetFont('Calibri','',11);
		$this->pdf->MultiCell($length, 4, $address);

		$supID = $content_detail['ship_to'];
		$ReffNo = $content_detail['ship_via'];
		$DateReff = $content_detail['fob'];
		$ConfNo = $content_detail['expected_date'];
		$ConfDate = $content_detail['taxable_vendor'];
		$ETA = $content_detail['rate'];
		$x1 = 132;
		$y1 = 69;
		$this->pdf->SetXY ($x1,$y1);
		$w = 33;
		$h=5;
		$this->pdf->SetFont('Calibri','',11);
		$this->pdf->Cell ($w,$h,'Ship To',1,'','R');
		$this->pdf->Cell($w+1,$h,$supID,1,'','C');
		$this->pdf->SetXY ($x1,$y1+$h);
		$this->pdf->Cell ($w,$h,'Ship Via',1,'','R');
		$this->pdf->Cell($w+1,$h,$ReffNo,1,'','C');
		$this->pdf->SetXY ($x1,$y1+$h+$h);
		$this->pdf->Cell ($w,$h,'FOB',1,'','R');
		$this->pdf->Cell($w+1,$h,$DateReff,1,'','C');
		$this->pdf->SetXY ($x1,$y1+$h+$h+$h);
		$this->pdf->Cell ($w,$h,'Expected Date',1,'','R');
		$this->pdf->Cell($w+1,$h,$ConfNo,1,'','C');
		$this->pdf->SetXY ($x1,$y1+$h+$h+$h+$h);
		$this->pdf->Cell ($w,$h,'Taxable Vendor',1,'','R');
		$this->pdf->Cell($w+1,$h,$ConfDate,1,'','C');
		$this->pdf->SetXY ($x1,$y1+$h+$h+$h+$h+$h);
		$this->pdf->Cell ($w,$h,'Rate',1,'','R');
		$this->pdf->Cell($w+1,$h,$ETA,1,'','C');
		
		$x1 = 10;
		$y1 = 110;
		$this->pdf->SetXY ($x1,$y1);
		$this->pdf->SetFont('Calibri-Bold','',11);
		$this->pdf->SetTextColor(255,255,255); 
		$this->pdf->SetFillColor(128,128,128); 
		$this->pdf->Cell(9, 5, 'No', 1, '', 'C',true);
		$this->pdf->Cell(25, 5, 'Item', 1, '', 'C', true);
		$this->pdf->Cell(45, 5, 'Description', 1, '', 'C', true);
		$this->pdf->Cell(10, 5, 'Qty', 1, '', 'C', true);
		$this->pdf->Cell(25, 5, 'Unit Price', 1, '', 'C', true);
		$this->pdf->Cell(20, 5, 'Disc %', 1, '', 'C', true);
		$this->pdf->Cell(20, 5, 'Tax %', 1, '', 'C', true);
		$this->pdf->Cell(35, 5, 'Total', 1, '', 'C', true);
		$this->pdf->Ln();
		$this->pdf->SetTextColor(0,0,0); 
		$this->pdf->SetFont('Calibri','',11);
		$num = 1;
		foreach($content_detail_list as $valueBarang){
			/*$HargaSatuanOriginal 	= 0;
			$Merk					= "";
			$Tipe 					= "";
			

			$PartNoSupp 	= "PartNoSupp";
			$PartNo 		= "PartNo";
			$PartNoOrigin	= "PartNoOrigin";
			$Qty			= 2;
			$TipeHarga		= "TipeHarga";
			$HargaSatuan	= "12.000";
			$HargaSatuanOriginal = "11.000";
			$Diskon			= "1.000";
			$NamaBarang 	= "NamaBarang";
			$Merk			= "Merk";
			$KodeMerk		= "Kode Merk";
			$Satuan 		= "Pcs";
			$Subtotal	= "100";
			$Tipe			= $Tipe;
			$Kodekelompok 	= "GroupBarang";*/
			$this->pdf->Cell(9, 5, $num, 1, '', 'C');
			$this->pdf->Cell(25, 5, substr($valueBarang['text_kode_barang'], 0, 16), 1, '', 'C');
			$this->pdf->Cell(45, 5, substr($valueBarang['text_nama_barang'], 0, 27), 1, '', 'C');
			$this->pdf->Cell(10, 5, $valueBarang['quantity'], 1, '', 'C');
			$this->pdf->Cell(25, 5, $valueBarang['harga_unit'], 1, '', 'C');
			$this->pdf->Cell(20, 5, $valueBarang['discount'], 1, '', 'C');
			$this->pdf->Cell(20, 5, $valueBarang['tax'], 1, '', 'C');
			$this->pdf->Cell(35, 5, $valueBarang['jumlah'], 1, '', 'R');
			$this->pdf->Ln();	
			$num++;
		}	
		for ($x = $num; $x <= 20; $x++) {
		$this->pdf->Cell(9, 5, $x, 1, '', 'C');
		$this->pdf->Cell(25, 5, '', 1, '', 'C');
		$this->pdf->Cell(45, 5, '', 1, '', 'C');
		$this->pdf->Cell(10, 5, '', 1, '', 'C');
		$this->pdf->Cell(25, 5, '', 1, '', 'C');
		$this->pdf->Cell(20, 5, '', 1, '', 'C');
		$this->pdf->Cell(20, 5, '', 1, '', 'C');
		$this->pdf->Cell(35, 5, '', 1, '', 'C');
		$this->pdf->Ln();	
		}
		$this->pdf->Cell(114);
		$this->pdf->Cell(40, 5, 'Sub Total', 1, '', 'C');

		$this->pdf->Cell(35, 5, (($content_detail['subtotal'])?$content_detail['subtotal']:'0'), 1, '', 'R');
		$this->pdf->Ln();
		$this->pdf->Cell(114);
		$this->pdf->Cell(40, 5, 'Discount', 1, '', 'C');
		$this->pdf->Cell(35, 5, (($content_detail['discount_value'])?$content_detail['discount_value']:'0'), 1, '', 'R');
		$this->pdf->Ln();
		$this->pdf->Cell(114);
		$this->pdf->Cell(40, 5, 'Freight', 1, '', 'C');
		$this->pdf->Cell(35, 5, (($content_detail['freight_value'])?$content_detail['freight_value']:'0'), 1, '', 'R');
		$this->pdf->Ln();
		$this->pdf->Cell(114);
		$this->pdf->Cell(40, 5, 'Grand Total', 1, '', 'C');
		$this->pdf->Cell(35, 5, (($content_detail['total'])?$content_detail['total']:'0'), 1, '', 'R');
		$this->pdf->Ln();
		$this->pdf->Ln();
		
		$x1 = 11;
		$this->pdf->SetX ($x1);
		$w = 44;
		$h = 5;
		$this->pdf->SetFont('Calibri-Bold','',11);
		$this->pdf->SetTextColor(255,255,255); 
		$this->pdf->SetFillColor(128,128,128); 
		$this->pdf->Cell($w,$h,'Remarks','','','C',true);
		$this->pdf->Ln();			
		$this->pdf->SetTextColor(0,0,0);
		$this->pdf->SetFont('Calibri','',11);
		
		$Num = 1;
		//foreach($ListTerms as $valueTerms){
			$Keterangan 	= "Note";
			$this->pdf->Cell(189, 5, $content_detail['note'], 1, 1); 
			$Num++;
		//}
		for ($x = $num; $x <= 3; $x++) {
			$this->pdf->Cell(189, 5, '', 1, 1); 
		}
		$this->pdf->Ln();
		
		$orderBy = "Teddy";
		$orderPost = "";
		$appBy = "Teddy";
		$apvPost = "Supplier";
		$this->pdf->SetFont('Calibri','',11);
		$this->pdf->Cell(9);
		$this->pdf->Cell(30, 5, 'Order By', '', '', 'C');
		$this->pdf->Cell(115); 
		$this->pdf->Cell(35, 5, 'Approve by', '', '', 'C');
		$this->pdf->Ln();
		$this->pdf->Ln();
		$this->pdf->Ln();
		$x1=19;
		$y1=285;
		$this->pdf->Cell(9); 
		$this->pdf->Cell(30, 5, $orderBy, '', '', 'C');
		$this->pdf->Line($x1,$y1,$x1+30,$y1);
		$this->pdf->Cell(115);
		$this->pdf->Cell(35, 5, $appBy, '', '', 'C');
		$this->pdf->Line($x1+30+115,$y1,$x1+30+115+35,$y1);
		$this->pdf->Ln();
		$this->pdf->Cell(9); 
		$this->pdf->Cell(30, 5, $orderPost, '', '', 'C');
		$this->pdf->Cell(115); 
		$this->pdf->Cell(35, 5, $apvPost, '', '', 'C');