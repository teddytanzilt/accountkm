CREATE TABLE `core_app_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `config_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `config_value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `config_key` (`config_key`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


LOCK TABLES `core_app_config` WRITE;
/*!40000 ALTER TABLE `core_app_config` DISABLE KEYS */;
INSERT INTO `core_app_config` VALUES (1,'WEB_NAME',''),(2,'COMPANY_NAME','');
/*!40000 ALTER TABLE `core_app_config` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE `core_app_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `forwarded_ip` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `accept_language` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `device_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `event` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `input_data` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `json_return` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `core_content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `record_type` int(10) DEFAULT NULL,
  `text_01` varchar(500) DEFAULT NULL,
  `text_02` varchar(500) DEFAULT NULL,
  `text_03` varchar(500) DEFAULT NULL,
  `text_04` varchar(500) DEFAULT NULL,
  `text_05` varchar(500) DEFAULT NULL,
  `text_06` varchar(500) DEFAULT NULL,
  `text_07` varchar(500) DEFAULT NULL,
  `text_08` varchar(500) DEFAULT NULL,
  `text_09` varchar(500) DEFAULT NULL,
  `int_01` int(10) DEFAULT NULL,
  `int_02` int(10) DEFAULT NULL,
  `int_03` int(10) DEFAULT NULL,
  `int_04` int(10) DEFAULT NULL,
  `int_05` int(10) DEFAULT NULL,
  `int_06` int(10) DEFAULT NULL,
  `int_07` int(10) DEFAULT NULL,
  `int_08` int(10) DEFAULT NULL,
  `int_09` int(10) DEFAULT NULL,
  `note_01` text,
  `note_02` text,
  `note_03` text,
  `note_04` text,
  `note_05` text,
  `note_06` text,
  `note_07` text,
  `note_08` text,
  `note_09` text,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `core_email_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mail_from` varchar(255) DEFAULT NULL,
  `mail_from_name` varchar(255) DEFAULT NULL,
  `mail_to` text,
  `mail_cc` text,
  `mail_bcc` text,
  `mail_subject` varchar(255) DEFAULT NULL,
  `mail_body` text,
  `mail_error` text,
  `mail_error_info` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `data_sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_session_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `master_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `password` char(64) NOT NULL,
  `role` int(10) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `token_cookie` varchar(64) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `user_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `user_name` (`user_name`),
  KEY `ip` (`ip`),
  KEY `role` (`role`),
  KEY `name_status` (`user_name`,`user_status`),
  CONSTRAINT `muser_role` FOREIGN KEY (`role`) REFERENCES `master_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_user`
--

LOCK TABLES `master_user` WRITE;
/*!40000 ALTER TABLE `master_user` DISABLE KEYS */;
INSERT INTO `master_user` VALUES (1,'Administrator','admin','25d55ad283aa400af464c76d713c07ad',1,'::1','',0,'2018-09-01 00:00:00',1,'2018-09-03 07:00:02','ACTIVE','ACTIVE');
/*!40000 ALTER TABLE `master_user` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE `master_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_role`
--

LOCK TABLES `master_role` WRITE;
/*!40000 ALTER TABLE `master_role` DISABLE KEYS */;
INSERT INTO `master_role` VALUES (1,'SADMIN','Super Admin',NULL,0,'2018-09-01 00:00:00',NULL,NULL,'ACTIVE'),(2,'Admin','Administrator',NULL,0,'2018-09-01 00:00:00',NULL,NULL,'ACTIVE'),(3,'ACCOUNT','Accounting',NULL,0,'2018-09-01 00:00:00',NULL,NULL,'ACTIVE');
/*!40000 ALTER TABLE `master_role` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE `master_module` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_module`
--

LOCK TABLES `master_module` WRITE;
/*!40000 ALTER TABLE `master_module` DISABLE KEYS */;
INSERT INTO `master_module` VALUES (1,'ROLE','File Role','mrole',NULL,1,'2018-09-01 00:00:00',NULL,NULL,'ACTIVE'),(2,'USER','File User','muser',NULL,1,'2018-09-01 00:00:00',NULL,NULL,'ACTIVE'),(3,'CUSTOMER','File Customer','mcustomer',NULL,1,'2018-09-01 00:00:00',NULL,NULL,'ACTIVE');
/*!40000 ALTER TABLE `master_module` ENABLE KEYS */;
UNLOCK TABLES;

CREATE TABLE `master_role_module` (
  `id_role` int(10) NOT NULL,
  `id_module` int(10) NOT NULL,
  PRIMARY KEY (`id_role`,`id_module`),
  KEY `id_role` (`id_role`),
  KEY `id_module` (`id_module`),
  KEY `role_module` (`id_role`,`id_module`),
  CONSTRAINT `mrolemodule_module` FOREIGN KEY (`id_module`) REFERENCES `master_module` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mrolemodule_role` FOREIGN KEY (`id_role`) REFERENCES `master_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `master_rekening_bank` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) NOT NULL,
  `bank` varchar(255) NOT NULL,
  `no_rekening` varchar(255) NOT NULL,
  `nama_rekening` varchar(255) NOT NULL,
  `saldo` int(20) NOT NULL DEFAULT '0',
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `bank_rekening` (`bank`,`no_rekening`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1

CREATE TABLE `master_type_cash_bank` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1

CREATE TABLE `master_customer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(50) NOT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `npwp` varchar(255) DEFAULT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1

CREATE TABLE `master_supplier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(50) NOT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `npwp` varchar(255) DEFAULT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1

CREATE TABLE `master_satuan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `kode_status` (`kode`,`record_status`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1


CREATE TABLE `master_barang` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `satuan` int(10) NOT NULL,
  `harga_modal` varchar(50) NOT NULL,
  `harga_jual` varchar(50) NOT NULL,
  `berat_per_satuan` varchar(50) NOT NULL,
  `satuan_berat` int(10) NOT NULL,
  `import_flag` varchar(10) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`),
  KEY `nama` (`nama`),
  KEY `satuan` (`satuan`),
  KEY `kode_status` (`kode`,`record_status`),
  KEY `mbarang_satuan_berat` (`satuan_berat`),
  CONSTRAINT `mbarang_satuan` FOREIGN KEY (`satuan`) REFERENCES `master_satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `mbarang_satuan_berat` FOREIGN KEY (`satuan_berat`) REFERENCES `master_satuan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1





CREATE TABLE `trans_purchase_order_header` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `supplier` int(10) NOT NULL,
  `ship_to` varchar(100) DEFAULT NULL,
  `terms` varchar(100) DEFAULT NULL,
  `fob` varchar(100) DEFAULT NULL,
  `ship_via` varchar(100) DEFAULT NULL,
  `expected_date` datetime DEFAULT NULL,
  `taxable_vendor` int(1) NOT NULL DEFAULT '0',
  `rate` varchar(100) DEFAULT NULL,
  `note` text,
  `subtotal` int(20) NOT NULL,
  `discount_value` int(20) NOT NULL,
  `ppn_rate` int(20) NOT NULL,
  `ppn_value` int(20) NOT NULL,
  `freight_value` int(20) NOT NULL,
  `total` int(20) NOT NULL,
  `prepared_by` int(10) DEFAULT NULL,
  `prepared_date` datetime NOT NULL,
  `approved_by` int(10) DEFAULT NULL,
  `approved_date` datetime NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'PENDING',
  `pembayaran_kapal_status` varchar(20) DEFAULT 'PENDING',
  PRIMARY KEY (`id`),
  KEY `no_transaksi` (`no_transaksi`),
  KEY `supplier` (`supplier`),
  CONSTRAINT `tpoheader_supplier` FOREIGN KEY (`supplier`) REFERENCES `master_supplier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1



CREATE TABLE `trans_purchase_order_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `header_id` int(10) NOT NULL,
  `barang` int(10) NOT NULL,
  `note` text,
  `quantity` int(10) NOT NULL,
  `harga_modal_unit` int(20) NOT NULL,
  `harga_unit` int(20) NOT NULL,
  `discount` int(20) NOT NULL,
  `tax` int(20) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `detail_status` varchar(20) DEFAULT 'PENDING',
  PRIMARY KEY (`id`),
  KEY `header_id` (`header_id`),
  KEY `barang` (`barang`),
  KEY `header_barang_status` (`header_id`,`barang`,`detail_status`),
  CONSTRAINT `tpodetail_barang` FOREIGN KEY (`barang`) REFERENCES `master_barang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tpodetail_header` FOREIGN KEY (`header_id`) REFERENCES `trans_purchase_order_header` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1



CREATE TABLE `trans_delivery_order_header` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `customer` int(10) NOT NULL,
  `ship_to` varchar(100) DEFAULT NULL,
  `ship_via` varchar(100) DEFAULT NULL,
  `po_no` varchar(100) DEFAULT NULL,
  `note` text,
  `prepared_by` int(10) DEFAULT NULL,
  `prepared_date` datetime NOT NULL,
  `approved_by` int(10) DEFAULT NULL,
  `approved_date` datetime NOT NULL,
  `shipped_by` int(10) DEFAULT NULL,
  `shipped_date` datetime NOT NULL,
  `received_by` int(10) DEFAULT NULL,
  `received_date` datetime NOT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `trans_status` varchar(20) DEFAULT 'PENDING',
  `pembayaran_kapal_status` varchar(20) DEFAULT 'PENDING',
  PRIMARY KEY (`id`),
  KEY `no_transaksi` (`no_transaksi`),
  KEY `customer` (`customer`),
  CONSTRAINT `tdoheader_customer` FOREIGN KEY (`customer`) REFERENCES `master_customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1



CREATE TABLE `trans_delivery_order_detail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `header_id` int(10) NOT NULL,
  `barang` int(10) NOT NULL,
  `note` text,
  `quantity` int(10) NOT NULL,
  `serial_no` varchar(100) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `record_status` varchar(20) DEFAULT 'ACTIVE',
  `detail_status` varchar(20) DEFAULT 'PENDING',
  PRIMARY KEY (`id`),
  KEY `header_id` (`header_id`),
  KEY `barang` (`barang`),
  KEY `header_barang_status` (`header_id`,`barang`,`detail_status`),
  CONSTRAINT `tdodetail_barang` FOREIGN KEY (`barang`) REFERENCES `master_barang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tdodetail_header` FOREIGN KEY (`header_id`) REFERENCES `trans_delivery_order_header` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1




INSERT INTO `master_module` (`kode`, `nama`, `link`, `created_by`, `created_on`, `record_status`) VALUES ('SUPPLIER', 'File Supplier', 'msupplier', '1', '2018-09-01 00:00:00', 'ACTIVE');
INSERT INTO `master_module` (`kode`, `nama`, `link`, `created_by`, `created_on`, `record_status`) VALUES ('SATUAN', 'File Satuan', 'msatuan', '1', '2018-09-01 00:00:00', 'ACTIVE');
INSERT INTO `master_module` (`kode`, `nama`, `link`, `created_by`, `created_on`, `record_status`) VALUES ('BARANG', 'File Barang', 'mbarang', '1', '2018-09-01 00:00:00', 'ACTIVE');
INSERT INTO `master_module` (`kode`, `nama`, `link`, `created_by`, `created_on`, `record_status`) VALUES ('BANK', 'File Bank', 'mrbank', '1', '2018-09-01 00:00:00', 'ACTIVE');
INSERT INTO `master_module` (`kode`, `nama`, `link`, `created_by`, `created_on`, `record_status`) VALUES ('TKASBON', 'File Type Kasbon', 'mtkasbon', '1', '2018-09-01 00:00:00', 'ACTIVE');

INSERT INTO `master_role_module` (`id_role`, `id_module`) VALUES ('1', '1');

INSERT INTO `accountkm`.`master_module` (`kode`, `nama`, `link`, `created_by`, `created_on`, `record_status`) VALUES ('SETTING', 'File Setting', 'msetting', '1', '2018-09-01 00:00:00', 'ACTIVE');

INSERT INTO `master_module` (`kode`, `nama`, `link`, `created_by`, `created_on`, `record_status`) VALUES ('PURCHASEORDER', 'Purchase Order', 'tpurchaseorder', '1', '2018-09-01 00:00:00', 'ACTIVE');

ALTER TABLE `master_module` CHANGE COLUMN `kode` `kode` VARCHAR(20) NOT NULL ;

UPDATE `master_module` SET `kode`='PURCHASEORDER' WHERE `id`='10';

ALTER TABLE `trans_purchase_order_header` 
CHANGE COLUMN `pembayaran_kapal_status` `pembayaran_status` VARCHAR(20) NULL DEFAULT 'PENDING' ;


ALTER TABLE `trans_purchase_order_detail` 
DROP COLUMN `harga_modal_unit`;

