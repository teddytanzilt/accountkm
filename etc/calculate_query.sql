SELECT 
    s.tanggal, s.debit, s.kredit,
    @b := @b + s.debit - s.kredit AS balance
FROM
    (SELECT @b := 0.0) AS dummy 
  CROSS JOIN
    trans_cash_bank AS s
ORDER BY
    tanggal ;


SELECT 
    s.tanggal, s.debit, s.kredit,
    @b,
    @b := @b + s.debit - s.kredit AS balance
FROM
    (SELECT @b := 0.0) AS dummy 
  CROSS JOIN
    trans_cash_bank AS s
ORDER BY
    tanggal ;


SELECT 
    s.tanggal, s.debit, s.kredit,
    @b := @b + s.debit - s.kredit AS balance
FROM
    (SELECT @b := 0.0) AS dummy 
  CROSS JOIN
    trans_cash_bank AS s
WHERE s.bank = 1
ORDER BY
    tanggal ;
    



###########################################################

SELECT 
    s.trans_date, s.jumlah_masuk, s.jumlah_keluar,
    @b,
    @b := @b + s.jumlah_masuk - s.jumlah_keluar AS balance
FROM
    (SELECT @b := 0.0) AS dummy 
  CROSS JOIN
    trans_inventory_barang AS s
ORDER BY
    trans_date ;



SELECT 
    s.id, s.id_barang, s.id_gudang, s.id_user, 
    s.comment, 
    s.trans_date, s.jumlah_masuk, s.jumlah_keluar,
    @b,
    @b := @b + s.jumlah_masuk - s.jumlah_keluar AS balance
FROM
    (SELECT @b := 0.0) AS dummy 
  CROSS JOIN
    trans_inventory_barang AS s
ORDER BY
    trans_date ;

